﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoinValues : MonoBehaviour {
	public Text CoinValue;
	// Use this for initialization
	void OnEnable () {
		if(GameConstants.currentStoryNumber == 5)
		{
            if (GameConstants.currentChapterNumber == 10 || GameConstants.currentChapterNumber == 18)
            { 
				CoinValue.text = DynamicConstants.CoinsRequiredForAccessory.ToString();
			}
            else
            { 
				CoinValue.text = DynamicConstants.CoinsRequiredForDialgoue.ToString();
			}
        }
        else
        {
            CoinValue.text = DynamicConstants.CoinsRequiredForDialgoue.ToString();
        }
    }
	

}
