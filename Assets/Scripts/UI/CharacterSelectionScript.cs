﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Fungus;
using Prime31;

public class CharacterSelectionScript : MonoBehaviour
{

    public InputField inputName;

    public Animator[] characterAnimators = new Animator[3];

    public Animator[] allCharactersAnimatorsArray;

    public GameObject posLeft;
    public GameObject posRight;
    public GameObject girl1;
    public GameObject girl2;
    public GameObject girl3;

    public GameObject[] allGirlsArray;


    // The selected character in the "Enter avatar name" screen.
    public GameObject selectedGirl1;

    public GameObject selectedGirl2;

    public GameObject selectedGirl3;

    //

    public GameObject[] selectedGirlArray;
    public GameObject centerObject;

    public Button selectButton;

    public Button characterNameSubmitButton;

    private bool hasOneKey = false;

    public GameObject[] storyCharactersObject;


    // Use this for initialization
    void Start()
    {

        //Change it to something standard- //Crap code for now
        if (GameConstants.currentStoryNumber != 1)
        {
            storyCharactersObject[GameConstants.currentStoryNumber - 2].SetActive(true);
            characterAnimators[0] = allCharactersAnimatorsArray[3 * (GameConstants.currentStoryNumber - 2)];
            characterAnimators[1] = allCharactersAnimatorsArray[(3 * (GameConstants.currentStoryNumber - 2))+1];
            characterAnimators[2] = allCharactersAnimatorsArray[(3 * (GameConstants.currentStoryNumber - 2))+2];
            selectedGirl1 = selectedGirlArray[3 * (GameConstants.currentStoryNumber - 2)];
            selectedGirl2 = selectedGirlArray[(3 * (GameConstants.currentStoryNumber - 2))+1];
            selectedGirl3 = selectedGirlArray[(3 * (GameConstants.currentStoryNumber - 2))+2];

            girl1 = allGirlsArray[3 * (GameConstants.currentStoryNumber - 2)];
            girl2 = allGirlsArray[(3 * (GameConstants.currentStoryNumber - 2))+1];
            girl3 = allGirlsArray[(3 * (GameConstants.currentStoryNumber - 2))+2];
        }

       
        //


        selectButton.interactable = false;
        characterNameSubmitButton.interactable = false;
        if(GameConstants.currentStoryNumber == 2)
        {
        characterAnimators[0].Play("Female_Idle1", 0);
        characterAnimators[1].Play("Female_Idle1", 0);
        characterAnimators[2].Play("Female_Idle1", 0);  
        }
        else
        {
            characterAnimators[0].Play("Idle1", 0);
            characterAnimators[1].Play("Idle1", 0);
            characterAnimators[2].Play("Idle1", 0);
        }
    }

    public void showAvatar(int num)
    {
        selectButton.interactable = true;
        string animName;
        if (GameConstants.currentStoryNumber == 2)
        {
            animName = "Female_Greeting";
        }
        else
        {
            animName = "Greeting";
        }
        if (num == 1)
        {
            // animateMain(girl1);
            characterAnimators[0].Play(animName, 0);
            animateAvatar(girl1, posLeft);
            animateAvatarBack(girl2, posLeft);
            animateAvatarBack(girl3, posLeft);
            //PlayerPrefs.SetInt ("SelectedCharacter", num);
        }
        if (num == 2)
        {
            // animateMain(girl1);
            characterAnimators[1].Play(animName, 0);
            animateAvatar(girl2, posLeft);
            animateAvatarBack(girl1, posLeft);
            animateAvatarBack(girl3, posLeft);
            //PlayerPrefs.SetInt ("SelectedCharacter", num);
        }
        if (num == 3)
        {
            // animateMain(girl1);
            characterAnimators[2].Play(animName, 0);
            animateAvatar(girl3, posLeft);
            animateAvatarBack(girl1, posLeft);
            animateAvatarBack(girl2, posLeft);
            //PlayerPrefs.SetInt ("SelectedCharacter", num);
        }
        GameConstants.SelectedCharacter = num;
    }

    void animateMain(GameObject girlObject)
    {
        Hashtable tweenParams = new Hashtable();

        tweenParams.Add("position", centerObject.transform.position);
        tweenParams.Add("time", 3f);
        tweenParams.Add("ignoretimescale", true);
        // tweenParams.Add("oncomplete",animationComplete)
        iTween.MoveTo(girlObject, tweenParams);
    }

    void animateAvatar(GameObject girlObject, GameObject girlPos)
    {
        Hashtable tweenParams = new Hashtable();

        //tweenParams.Add("position",girlPos.transform.position);
        tweenParams.Add("scale", new Vector3(1.2f, 1.2f, 1.2f));
        tweenParams.Add("time", 3f);
        tweenParams.Add("ignoretimescale", true);
        iTween.ScaleTo(girlObject, tweenParams);
        //iTween.MoveTo (storeArrowObject,,3f);
    }

    void animateAvatarBack(GameObject girlObject, GameObject girlPos)
    {
        Hashtable tweenParams = new Hashtable();

        //tweenParams.Add("position",girlPos.transform.position);
        tweenParams.Add("scale", new Vector3(1f, 1f, 1f));
        tweenParams.Add("time", 3f);
        tweenParams.Add("ignoretimescale", true);
        iTween.ScaleTo(girlObject, tweenParams);
        //iTween.MoveTo (storeArrowObject,,3f);
    }

    void animationComplete()
    {

    }

    public void avatarSelected(int num)
    {
        G2WGAHelper.LogEvent("Selected Character", "Story " + GameConstants.currentStoryNumber, "Character " + GameConstants.SelectedCharacter.ToString(), 1);
        string animName1,animName2,animName3;
        if (GameConstants.currentStoryNumber == 2)
        {
            animName1 = "Female_Victorious";
             animName2 = "Female_Anger";
              animName3 = "Female_Sadness";
        }
        else
        {
             animName1 = "Victorious";
             animName2 = "Disagree";
              animName3 = "Sadness";
        }
        switch (GameConstants.SelectedCharacter)
        {
            case 1:
                characterAnimators[0].Play(animName1, 0);
                characterAnimators[1].Play(animName2, 0);
                characterAnimators[2].Play(animName3, 0);

                break;
            case 2:
                characterAnimators[1].Play(animName1, 0);
                characterAnimators[0].Play(animName2, 0);
                characterAnimators[2].Play(animName3, 0);

                break;
            case 3:
                characterAnimators[2].Play(animName1, 0);
                characterAnimators[1].Play(animName2, 0);
                characterAnimators[0].Play(animName3, 0);

                break;
        }
        Invoke("loadCharacterNamePanel", 3f);

    }

    public void loadCharacterNamePanel()
    {
         if(GameConstants.currentStoryNumber == 2)
        {
        characterAnimators[0].Play("Female_Idle1", 0);
        characterAnimators[1].Play("Female_Idle1", 0);
        characterAnimators[2].Play("Female_Idle1", 0);  
        }
        else
        {
            characterAnimators[0].Play("Idle1", 0);
            characterAnimators[1].Play("Idle1", 0);
            characterAnimators[2].Play("Idle1", 0);
        }
        LevelSelectionScript._levelselection.showCharacterNamePanel();
        switch (GameConstants.SelectedCharacter)
        {
            case 1:
                selectedGirl1.SetActive(true);
                selectedGirl2.SetActive(false);
                selectedGirl3.SetActive(false);
                break;
            case 2:
                selectedGirl1.SetActive(false);
                selectedGirl2.SetActive(true);
                selectedGirl3.SetActive(false);
                break;
            case 3:
                selectedGirl1.SetActive(false);
                selectedGirl2.SetActive(false);
                selectedGirl3.SetActive(true);
                break;

        }
    }

    public void saveCharacterName()
    {
        GameConstants.PlayerName = inputName.text.ToTitleCase();
        GameConstants.isStoryNameGiven = true;
        LevelSelectionScript._levelselection.loadNextLevel();
        G2WGAHelper.LogEvent("Enter Name", "Entered");
        Debug.Log("PLAYER NAME " + GameConstants.PlayerName);
    }

    void Update()
    {

        hasOneKey = false;
        char[] array = inputName.text.ToCharArray();
        if (inputName.text != null)
        {
            characterNameSubmitButton.interactable = true;

            // Loop through array.
            for (int i = 0; i < array.Length; i++)
            {
                // Get character from array.
                char letter = array[i];

                if (letter != ' ')
                {
                    //submitButton.interactable = false;
                    hasOneKey = true;
                }
            }
        }
        else
        {
            characterNameSubmitButton.interactable = true;

        }

        if (hasOneKey)
            characterNameSubmitButton.interactable = true;
        else
            characterNameSubmitButton.interactable = false;

    }
}
