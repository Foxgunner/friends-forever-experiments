﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;
using UnityEngine.SceneManagement;

public class YoYocaching : MonoBehaviour
{
	public static YoYocaching yoyo;
	public RawImage yoyoImage;
	public static bool yoyoLoaded;
	Dictionary <int, byte[]> savedYoYos = new Dictionary<int, byte[]> ();
	public string currentURL = "";
	public string currentImageURL = "";
	public string currentGameName = "";

	void Awake ()
	{
		if (yoyo == null) {
			DontDestroyOnLoad (gameObject);
			yoyo = this;
		} else if (yoyo != this) {
			Destroy (gameObject);
		}

	}

	// Use this for initialization
	void Start ()
	{
		//PlayerPrefs.DeleteAll ();
		StartCoroutine (FetchDynamicyoyo ());
	}

	public bool GetLoadedSceneByName(string name)
	{
		for(int i = 0; i < SceneManager.sceneCount; i++)
		{
			if(SceneManager.GetSceneAt(i).name == name)
				return true;
		}

		return false;
	}

	void OnLevelWasLoaded ()
	{
	//	if (GetLoadedSceneByName("cityday") || GetLoadedSceneByName("citynight"))
			CheckForYoYo ();
	}

	public void CheckForYoYo ()
	{
		StartCoroutine (FetchDynamicyoyo ());
	}

	IEnumerator FetchDynamicyoyo ()
	{
		while (!Caching.ready)
			yield return null;

		if (Application.internetReachability != NetworkReachability.NotReachable) {   // online

			var req = new WWW (GameConstants.dynamicDataLink);
			yield return req;

			if (req.error == null) {
				//Debug.Log ("No error");

				var reqData = (Hashtable)MiniJSON.jsonDecode (req.text);

				var result = reqData ["result"] as Hashtable;
				if (result != null) {
					//Debug.Log ("No error");
					var yoyo = result ["ingame_multi_promo"] as Hashtable;
					if (yoyo != null) {
						//Debug.Log ("No error");
						//Debug.Log ("yoyo Count " + yoyo.Count);

						for (int i = 0; i < yoyo.Count; i++) {

							var yoyo_ = yoyo [(i + 1).ToString ()] as Hashtable;
							//var yoyo_click_url = yoyo [(i+1).ToString()] as Hashtable;

							if (yoyo_ != null && PlayerPrefs.GetString ("yoyo_image_url" + i.ToString ()) != yoyo_ ["image_url"].ToString ()) {
								Debug.Log ("No error " + i);

								Debug.Log ("Image URL: " + yoyo_ ["image_url"].ToString ());
								Debug.Log ("Click URL : " + yoyo_ ["click_url"].ToString ());


								var www_yoyoImage = new WWW (yoyo_ ["image_url"].ToString ());
								yield return www_yoyoImage;


								var www_yoyoClickurl = new WWW (yoyo_ ["click_url"].ToString ());
								yield return www_yoyoClickurl;

								if (!string.IsNullOrEmpty (www_yoyoImage.error)) {
									//Debug.Log ("www_yoyoImage " + www_yoyoImage.error);
									yield return null;
								} else {
									Debug.Log ("www_yoyoImage " + www_yoyoImage);

									yoyoLoaded = true;
									PlayerPrefs.SetInt ("yoyoCount", yoyo.Count);
									PlayerPrefs.SetString ("yoyo_image_url" + i.ToString (), yoyo_ ["image_url"].ToString ());
									PlayerPrefs.SetString ("yoyo_click_url" + i.ToString (), yoyo_ ["click_url"].ToString ());

									savedYoYos.Add (i, www_yoyoImage.texture.EncodeToPNG ());
									File.WriteAllBytes (Application.persistentDataPath + "/yoyoImg_" + i.ToString () + ".yoyo", savedYoYos [i]);
								}
							} else {
								yoyoLoaded = true;
								//Debug.Log ("No new YOYO available");
							}
						}
					} else {
						Debug.Log ("No Yoyo data");
					}
				}
			} else {
				Debug.Log (req.error);
			}
		}
	}

	public void showYoYo (RawImage img)
	{
		if (yoyoLoaded) {
			int currentYoYo = PlayerPrefs.GetInt ("currentyoyocount", 0);

			yoyoImage = img;

			//if (File.Exists (Application.persistentDataPath + "/yoyoImg_" + currentYoYo.ToString () + ".yoyo")) {
			if (currentYoYo > PlayerPrefs.GetInt ("yoyoCount", 0) - 1)
				currentYoYo = 0;

			byte[] yoyoData = File.ReadAllBytes (Application.persistentDataPath + "/yoyoImg_" + currentYoYo.ToString () + ".yoyo");
			currentURL = PlayerPrefs.GetString ("yoyo_click_url" + currentYoYo.ToString ());
			currentImageURL = PlayerPrefs.GetString ("yoyo_image_url" + currentYoYo.ToString ());
			Texture2D yoyoTex = new Texture2D (1, 1);
			yoyoTex.LoadImage (yoyoData);
			yoyoImage.texture = yoyoTex;
			yoyoImage.GetComponent <Animation> ().Play ();

			currentYoYo++;
			PlayerPrefs.SetInt ("currentyoyocount", currentYoYo);
		}
	}


	// Update is called once per frame
	void Update ()
	{

	}
}
