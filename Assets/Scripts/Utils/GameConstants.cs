﻿using UnityEngine;
using System.Collections;
using Prime31;
using UnityEngine.UI;

public class GameConstants
{
    public static GameStates currentGameState;
    public static bool isStoreLoaded;

    // "http://s01.appucino.com/games/init?game_id=test_template_ios";

    public static bool isShowingLocationPermission = true;

    public static bool isPNSURLOpened = false;




    #region ADS constants

    public static bool isExitGamePopupActive = false;
    public static bool isInterstitialLoaded, isInterstitialRequested = false;


#if UNITY_ANDROID
    public static string dynamicDataLink = "http://s01.appucino.com/games/init?game_id=friends_forever_android";//"http://s01.appucino.com/games/init?game_id=ff_android_test";//"http://s01.appucino.com/games/init?game_id=friends_forever_android";

    public static string mopubBannerPhoneID = "1d653bae82334d3f8d56c13c747f0fb8";
    public static string mopubInterstitialPhoneID = "79162dbab6e84885baa5a8b3baf92406";
    public static string mopubExitAdPhoneID = "601ea8690b9641d699db1efda68c5ed1";

    public static string mopubBannerTabID = "c83fd97c86074388909c81ef2317d372";
    public static string mopubInterstitialTabID = "c123b37c9664484a85d2629fc8348dde";
    public static string mopubExitAdTabID = "7a42279d161040bfaa2f3591543f4e3b";

    public static string IRONSOURCE_APPLICATION_KEY = "5b268a4d";


#elif UNITY_IOS
	public static string APPName = "Friends Forever";
	public static string APPVersion = "1.2";
	public static string PNSID = "210";

	public static string dynamicDataLink = "http://s01.appucino.com/games/init?game_id=friends_forever_ios";

	public static string mopubBannerPhoneID = "3fa389c1ce4949b298317e23dc28a9cf";
	public static string mopubInterstitialPhoneID = "ebcfd8b334504867a41ad1e222bfd527";

	public static string mopubBannerTabID = "be0f75548d84437f84f59db61b523a73";
	public static string mopubInterstitialTabID = "a782c8618df641379a548e893e4535e3";

	public static string IRONSOURCE_APPLICATION_KEY = "5c25d5cd";

#endif

    public static int InterstitialTimer
    {
        get
        {
            return PlayerPrefs.GetInt("InterstitialTimer", 60);
        }
        set
        {
            PlayerPrefs.SetInt("InterstitialTimer", value);
            PlayerPrefs.Save();
        }
    }

    #endregion

    #region IAP constants


    public static string IAP_COINPACK_1 = "com.games2win.ff.coinpack_1";
    public static string IAP_COINPACK_2 = "com.games2win.ff.coinpack_2";
    public static string IAP_COINPACK_3 = "com.games2win.ff.coinpack_3";
    public static string IAP_MOVEPACK_1 = "com.games2win.ff.movepack_1";
    public static string IAP_MOVEPACK_2 = "com.games2win.ff.movepack_2";
    public static string IAP_MOVEPACK_3 = "com.games2win.ff.movepack_3";


    #endregion

    #region RewardedVideo constants


    public const string RVP_StoreCoins = "coins_store";
    public const string RVP_StoreMoves = "moves_store";
    public const string RVP_InGameCoins = "coins_ingame";
    public const string RVP_InGameMoves = "moves_ingame";
    public const string RVP_StoryModeCoins = "storymode_coins";


    #endregion

    //public static string currentSceneName = "Story" + GameConstants.currentStoryNumber + "Scene";


    #region int constants

    public static int hintsUsedInLevel = 0;

    public static int timerUsedInLevel = 0;

    public static int coinsUsedForHints = 0;

    public static int coinsUsedForTimer = 0;

    #endregion

    /// <summary>
    /// Gets or sets the current story number.
    /// </summary>
    /// <value>The current story number.</value>
    public static int currentStoryNumber
    {
        get
        {
            return P31Prefs.getInt("currentStoryNumber", 1);
        }
        set
        {
            P31Prefs.setInt("currentStoryNumber", value);
            PlayerPrefs.Save();
        }
    }

    /// <summary>
    /// Gets or sets the current chapter number.
    /// </summary>
    /// <value>The current level number.</value>
    public static int currentChapterNumber
    {
        get
        {
            Debug.Log("GameConstants.currentStoryNumber " + GameConstants.currentStoryNumber);
            if (GameConstants.currentStoryNumber == 1)
            {

                return P31Prefs.getInt("currentChapterNumber", 1);

            }
            else
            {

                return P31Prefs.getInt("Story" + GameConstants.currentStoryNumber.ToString() + "currentChapterNumber", 1);

            }

        }
        set
        {
            if (GameConstants.currentStoryNumber == 1)
            {
                P31Prefs.setInt("currentChapterNumber", value);
            }
            else
            {
                P31Prefs.setInt("Story" + GameConstants.currentStoryNumber + "currentChapterNumber", value);

            }
            PlayerPrefs.Save();
        }
    }

    /// <summary>
    /// Gets or sets the current hidden object level number.
    /// </summary>
    /// <value>The current hiddenobject level number.</value>
    public static int currentHiddenObjectLevelNumber
    {
        get
        {

            return P31Prefs.getInt("currentHiddenObjectLevelNumber", 1);
        }
        set
        {
            P31Prefs.setInt("currentHiddenObjectLevelNumber", value);
            PlayerPrefs.Save();
        }
    }


    /// <summary>
    /// Gets or Sets the total number of video watched.
    /// </summary>
    /// <value>The total videos watched.</value>
    public static int totalVideosWatched
    {
        get
        {
            return PlayerPrefs.GetInt("totalVideosWatched", 0);
        }
        set
        {
            PlayerPrefs.SetInt("totalVideosWatched", value);
            PlayerPrefs.Save();
        }
    }

    /// <summary>
    /// Gets or Sets the total gameplays.
    /// </summary>
    /// <value>The total videos watched.</value>
    public static int totalGameplays
    {
        get
        {
            return PlayerPrefs.GetInt("totalGameplays", 0);
        }
        set
        {
            PlayerPrefs.SetInt("totalGameplays", value);
            PlayerPrefs.Save();
        }
    }


    /// <summary>
    /// Return true if the current story level has hidden object.
    /// </summary>
    /// <returns><c>true</c>Level has hidden object <c>false</c> Level doesn't has hidden object</returns>
    public static bool isHiddenObject()
    {
        if (GameConstants.currentStoryNumber == 1)
        {
            if (currentChapterNumber == 5 || currentChapterNumber == 8 || currentChapterNumber == 11 || currentChapterNumber == 16 || currentChapterNumber == 21 || currentChapterNumber == 26)
            {
                return true;
            }
        }
        else if (GameConstants.currentStoryNumber == 2)
        {
            if (currentChapterNumber == 6 || currentChapterNumber == 10 || currentChapterNumber == 12 || currentChapterNumber == 18 || currentChapterNumber == 23 || currentChapterNumber == 29)
            {
                return true;
            }
        }
        else if (GameConstants.currentStoryNumber == 3)
        {
            if (currentChapterNumber == 2 || currentChapterNumber == 8 || currentChapterNumber == 11 || currentChapterNumber == 17 || currentChapterNumber == 21 || currentChapterNumber == 28)
            {
                return true;
            }
        }
        else if (GameConstants.currentStoryNumber == 4)
        {
            if (currentChapterNumber == 5 || currentChapterNumber == 11 || currentChapterNumber == 16 || currentChapterNumber == 18 || currentChapterNumber == 25 || currentChapterNumber == 28)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Returns true if the internet is connected otherwise shows alert message.
    /// </summary>

    public static bool isNetConnected()
    {
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            return true;
        }
        else
        {
#if UNITY_IOS
			var buttons = new string[] { "OK" };
			EtceteraBinding.showAlertWithTitleMessageAndButtons ("NO INTERNET CONNECTION.", "YOU ARE NOT CONNECTED TO INTERNET. TRY AGAIN LATER.", buttons);
#elif UNITY_ANDROID
            EtceteraAndroid.showAlert("NO INTERNET CONNECTION", "YOU ARE NOT CONNECTED TO INTERNET. TRY AGAIN LATER.", "OK");
#endif
            return false;
        }

    }

    /// <summary>
    /// For Native Location permission.
    /// </summary>
    /// <value><c>true</c> if ask location permission; otherwise, <c>false</c>.</value>

    public static bool askLocationPermission
    {
        get
        {
            return PlayerPrefsX.GetBool("askLocationPermission", false);
        }
        set
        {
            PlayerPrefsX.SetBool("askLocationPermission", value);
            PlayerPrefs.Save();
        }
    }

    /// <summary>
    /// Checks if the user has completed the whole story once or not.
    /// </summary>
    /// <value><c>true</c> if is story completed; otherwise, <c>false</c>.</value>
    public static bool isStoryCompleted
    {
        get
        {
            if (GameConstants.currentStoryNumber == 1)
            {

                return P31Prefs.getBool("isStoryCompleted");
            }
            else
            {
                return P31Prefs.getBool("isStory" + GameConstants.currentStoryNumber + "Completed");
            }
        }
        set
        {
            if (GameConstants.currentStoryNumber == 1)
            {
                P31Prefs.setBool("isStoryCompleted", value);
            }
            else
            {
                P31Prefs.setBool("isStory" + GameConstants.currentStoryNumber + "Completed", value);
            }
            PlayerPrefs.Save();
        }
    }


    public static bool isStoryNameGiven
    {
        get
        {
            string s = "isStory" + GameConstants.currentStoryNumber.ToString() + "NameGiven";
            //return P31Prefs.getBool ("isStory2NameGiven");
            return P31Prefs.getBool(s);
        }
        set
        {
            string s = "isStory" + GameConstants.currentStoryNumber.ToString() + "NameGiven";
            P31Prefs.setBool(s, value);
            //P31Prefs.setBool ("isStory2NameGiven", value);

            PlayerPrefs.Save();
        }
    }

    ///Retuns the current Story name based on the story number
    public static string getStoryName()
    {
        string storyname = "";

        switch (currentStoryNumber)
        {
            case 1:
                storyname = "CTL";
                break;
            case 2:
                storyname = "LLL";
                break;
            case 3:
                storyname = "TG";
                break;
            case 4:
                storyname = "SSC";
                break;
            case 5:
                storyname = "TW";
                break;
            case 6:
                storyname = "OHN";
                break;
        }
        return storyname;
    }

    public static string getSceneName()
    {
        string scenename = "";
        scenename = "Story" + GameConstants.currentStoryNumber + "Scene";
        return scenename;
    }

    public static bool checkIfStoryUnlocked(int story)
    {
        return P31Prefs.getBool("Story" + story + "Unlocked");

    }

    public static bool canCollectMove
    {
        get
        {

            return P31Prefs.getBool("canCollectMove");
        }
        set
        {
            P31Prefs.setBool("canCollectMove", value);
            PlayerPrefs.Save();
        }
    }

    public static bool hasOverriddenLanguage
    {
        get
        {

            return P31Prefs.getBool("hasOverriddenLanguage");
        }
        set
        {
            P31Prefs.setBool("hasOverriddenLanguage", value);
            PlayerPrefs.Save();
        }
    }

    /// Stores the character selected by player for each story.
    public static int SelectedCharacter
    {
        get
        {
            if (GameConstants.currentStoryNumber == 2)
            {

                return P31Prefs.getInt("SelectedCharacter");
            }
            else
            {
                return P31Prefs.getInt("SelectedCharacter" + GameConstants.currentStoryNumber.ToString());
            }
        }
        set
        {
            if (GameConstants.currentStoryNumber == 2)
            {
                P31Prefs.setInt("SelectedCharacter", value);
            }
            else
            {
                P31Prefs.setInt("SelectedCharacter" + GameConstants.currentStoryNumber.ToString(), value);
            }
            PlayerPrefs.Save();
        }
    }

    ///Stores the user given character name for each story.
    public static string PlayerName
    {
        get
        {
            if (GameConstants.currentStoryNumber == 2)
            {

                return P31Prefs.getString("PlayerName");
            }
            else
            {
                return P31Prefs.getString("PlayerName" + GameConstants.currentStoryNumber.ToString());
            }
        }
        set
        {
            if (GameConstants.currentStoryNumber == 2)
            {
                P31Prefs.setString("PlayerName", value);
            }
            else
            {
                P31Prefs.setString("PlayerName" + GameConstants.currentStoryNumber.ToString(), value);
            }
            PlayerPrefs.Save();
        }
    }

/// 0 - No rating has been asked yet, 1 - Rating has been asked once after 5th Chapter, 2 - Rating has been asked twice after 10th Chapter and, also for not showing the popup again if the user rated the first time.
    public static int askedRateUsTranslationNumber
    { 
        get
        {
            return PlayerPrefs.GetInt("askedRateUsTranslationNumberfor" + currentStoryNumber, 0);
        }
        set
        {
            PlayerPrefs.SetInt("askedRateUsTranslationNumberfor" + currentStoryNumber, value);
            PlayerPrefs.Save();
        }
    }


    /// 0 - No rating has been asked yet, 1 -  Rating has been done.
    public static int askedStoryFeedbackRateUsNumber
    { 
        get
        {
            return PlayerPrefs.GetInt("askedStoryFeedbackRateUsNumber" + currentStoryNumber, 0);
        }
        set
        {
            PlayerPrefs.SetInt("askedStoryFeedbackRateUsNumber" + currentStoryNumber, value);
            PlayerPrefs.Save();
        }
    }

    public static string currentLanguage
    {
        get
        {

                return P31Prefs.getString("currentLanguage");

        }
        set
        {

            P31Prefs.setString("currentLanguage", value);
            PlayerPrefs.Save();
        }
    }

    public static int sessionCount
    {
        get
        {
            return P31Prefs.getInt("sessionCount");
        }
        set
        {
            P31Prefs.setInt("sessionCount", value);
            PlayerPrefs.Save();
        }
    }

    public static int offlinePlays {
		get {
			return PlayerPrefs.GetInt ("offlinePlays", 0);
		}
		set {
			PlayerPrefs.SetInt ("offlinePlays", value);
			PlayerPrefs.Save ();
		}
	}


	public static bool turnedOnInternet {
		get {

			return P31Prefs.getBool ("turnedOnInternet");
		}
		set {
			P31Prefs.setBool ("turnedOnInternet", value);
			PlayerPrefs.Save ();
		}
	}





}
