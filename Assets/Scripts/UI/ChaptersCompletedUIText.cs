﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Prime31;

public class ChaptersCompletedUIText : MonoBehaviour
{
	public Text chapterText;

	public int currentStoryNumber;

	void OnEnable ()
	{
		Debug.Log ("currentStoryNumber>>>>>> " + currentStoryNumber);
		if (currentStoryNumber == 1) {
			int cChapter = P31Prefs.getInt ("currentChapterNumber", 1);
			Debug.Log ("currentChapterNumber>>>>>> " + cChapter);

			if (P31Prefs.getBool ("isStoryCompleted")) {
				if (cChapter == 1) {
					chapterText.text = "30/30";
				} else {
					chapterText.text = (cChapter - 1) + "/30";
				}
			} else {
				chapterText.text = (cChapter - 1) + "/30";
				if (chapterText.text == "-1/30") {
					chapterText.text = "0/30";
				}
			}
			Debug.Log ("chapterText>>>>>> " + chapterText.text);
		} else  { 
			int cChapter = P31Prefs.getInt ("Story" + currentStoryNumber + "currentChapterNumber", 1);
			if (P31Prefs.getBool ("isStory" + currentStoryNumber + "Completed")) {
				if (cChapter == 1) {
					chapterText.text = "30/30";
				} else {
					chapterText.text = (cChapter - 1) + "/30";
				}
			} else {
				chapterText.text = (cChapter - 1) + "/30";
				if (chapterText.text == "-1/30") {
					chapterText.text = "0/30";
				}
			}
            
		}
	}
}
