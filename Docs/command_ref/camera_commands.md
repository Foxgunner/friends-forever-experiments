# Camera commands # {#camera_commands}

[TOC]
# Fade Screen # {#FadeScreen}
Draws a fullscreen texture over the scene to give a fade effect. Setting Target Alpha to 1 will obscure the screen, alpha 0 will reveal the screen. If no Fade Texture is provided then a default flat color texture is used.

Defined in Fungus.FadeScreen

Property | Type | Description
 --- | --- | ---
Duration | System.Single | 
Target Alpha | System.Single | 
Wait Until Finished | System.Boolean | 
Fade Color | UnityEngine.Color | 
Fade Texture | UnityEngine.Texture2D | 

# Fade To View # {#FadeToView}
Fades the camera out and in again at a position specified by a View object.

Defined in Fungus.FadeToView

Property | Type | Description
 --- | --- | ---
Duration | System.Single | 
Fade Out | System.Boolean | 
Target View | Fungus.View | 
Wait Until Finished | System.Boolean | 
Fade Color | UnityEngine.Color | 
Fade Texture | UnityEngine.Texture2D | 
Target Camera | UnityEngine.Camera | 

# Fullscreen # {#Fullscreen}
Sets the application to fullscreen, windowed or toggles the current state.

Defined in Fungus.Fullscreen
# Move To View # {#MoveToView}
Moves the camera to a location specified by a View object.

Defined in Fungus.MoveToView

Property | Type | Description
 --- | --- | ---
Duration | System.Single | 
Target View | Fungus.View | 
Wait Until Finished | System.Boolean | 
Target Camera | UnityEngine.Camera | 

# Set Camera Size # {#SetCameraSize}
Modify Camera Size

Defined in ChangeCameraSize

Property | Type | Description
 --- | --- | ---
Camera Size | System.Single | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Shake Camera # {#ShakeCamera}
Applies a camera shake effect to the main camera.

Defined in Fungus.ShakeCamera

Property | Type | Description
 --- | --- | ---
Duration | System.Single | 
Amount | UnityEngine.Vector2 | 
Wait Until Finished | System.Boolean | 

# Start Swipe # {#StartSwipe}
Activates swipe panning mode where the player can pan the camera within the area between viewA & viewB.

Defined in Fungus.StartSwipe

Property | Type | Description
 --- | --- | ---
View A | Fungus.View | 
View B | Fungus.View | 
Duration | System.Single | 
Speed Multiplier | System.Single | 
Target Camera | UnityEngine.Camera | 

# Stop Swipe # {#StopSwipe}
Deactivates swipe panning mode.

Defined in Fungus.StopSwipe
