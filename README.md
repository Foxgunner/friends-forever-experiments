### First Steps ###

Before checking out the repository, ensure your Android SDK is uptodate.

<ANDROID_SDK_PATH>/tools/android 

Download any SDKs that are dated, especially in Extras
Android Support Repository
Android Support Library
Google Play services
Google Repository


Plugins That Need to be Installed Manually
1. All AAR files generated by Google Resolver
1. All Supersonic related mediation plugins




## Platform Conversion ##
Convert the Unity Project to Android / iOS 
 
 <UNITY_PATH>/Unity -buildTarget android -projectPath <PATH_TO_PROJECT>

### Android ###
## Import Google Play Services ##
Get this done first, since it is the common dependency across other SDKs.

1. Download the Prime31 plugin.
e.g. URL is http://www.prime31.com/unity/pluginDownload.php?id=40f879b6-aafe-11e6-af38-0025901e53b0

2. Install the plugin
Either Import the Asset or run 

 <UNITY_PATH>/Unity -importPackage <PATH_TO_PRIME31_PLUGIN> -projectPath <PATH_TO_PROJECT>

 3. Follow instructions on Prime31 website

 as on 13th December 2016 :
Add the app ID that you get when setting up your game in the Developer Console in the Plugins/Prime31/PlayGameServicesAndroid/Manifest.xml file. 
Important: you should enter the app ID in the com.google.android.gms.games.APP_ID field and you must include the preceding "\ " (slash and space)! If you do not include the slash and space your app will not work. An example of what should be entered is:

<meta-data android:name="com.google.android.gms.games.APP_ID" android:value="\ 111111"/>


4. With new AAR file system, the JARResolver can be used to download the right AARs and JARs.
These are fecthed from the local maven repository in your android sdk.

PlayGamesServicesResolver.cs contains commands to fetch the right versions of AARs.  

To ensure caches are cleared, ensure you quit Unity, edit the PlayServicesResolver.cs files, and delete ProjectSettings\
GoogleAarExplodeCache.xml
GoogleDependencyGooglePlayGames.xml

Google.VersionHandler.InvokeInstanceMethod( svcSupport, "DependOn",
				new object[] {
					"com.google.android.gms",
					"play-services-games",
					"9.8.0"
				},
				namedArgs: new Dictionary<string, object>() {
					{ "packageIds", new string[] { "extra-google-m2repository" } }
				} );
				
			Google.VersionHandler.InvokeInstanceMethod( svcSupport, "DependOn",
				new object[] {
					"com.google.android.gms",
					"play-services-plus",
					"9.8.0"
				},
				namedArgs: new Dictionary<string, object>() {
					{ "packageIds", new string[] { "extra-google-m2repository" } }
				} );

			Google.VersionHandler.InvokeInstanceMethod( svcSupport, "DependOn",
				new object[] {
					"com.google.android.gms",
					"play-services-ads",
					"9.8.0"
				},
				namedArgs: new Dictionary<string, object>() {
					{ "packageIds", new string[] { "extra-google-m2repository" } }
				} );


5. Importantly, after all Prime31 plugins have been imported, remember to run Tools -> Prime31 -> Generate AndroidManifestFile.xml


MAKE A BUILD TO CONFIRM EVERYTHING WORKS


## MoPub 

1. Import MoPub plugin 

<UNITY_PATH>/Unity -importPackage <MOPUB_PACKAGE> -projectPath <PATH_TO_PROJECT>

2. Edit the default permissions requested by MoPub, by editing Assets/Plugins/Android/mopub/AndroidManifest.xml
Remove :
 <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>


3. Import the required Ad SDKs (provided by AdOps team, and usually listed in the game's Game Information Sheet)
 
 e.g. to include Vungle's SDK :-
-/Applications/Unity/Unity.app/Contents/MacOS/Unity -importPackage ~/Downloads/mopub-unity-sdk-v4.11.1/VungleSupport.unitypackage -projectPath ~/Projects/friends_forever/ &


4. To avoid Manifest merging conflicts with AdsActivty ACtivity, comment out 

        <!-- activity android:name="com.google.android.gms.ads.AdActivity"
                  android:configChanges="keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize"/ -->

from MoPub's manifest. (Plugins/Android/mopub/AndroidManifest.xml)


### Specific Requirements for MoPub - Facebook ###
1. Install the MoPub Facebook Unity Adapter 
1. DOwnload the Facebook SDK. We have a copy at https://drive.google.com/open?id=0B-GrcLfIe3rYelZhWG9uWVJ3VkU.
2. We need the AudienceNetwork.aar file.  You find it in facebook-sdk/AudienceNetwork/bin/AudienceNetwork.aar
3. Place it in Plugins/Android folder and do a Refresh, so that you can configure the AAR to work on Android.
Subsequently place the .aar in Plugins/Android/mopub-support/libs/Facebook.
4. Ensure target SDK is 11 and above.

### Specific Requirements for MoPub - Vungle ###
1. Install the MoPub Vungle adapter.
1. The following files from Vungle need to be placed in Assets/Plugins/Android/libs [placed elsewhere and we get a missing classes error]
nineoldandroids-2.4.0.jar
dagger-2.7.jar
javax.inject-1.jar
and in Assets/Plugins/Android, place :
vungle-publisher-adaptive-4.0.3.jar
1. Create a folder assets in Assets/Plugins/Android, and place the following resources :
vg_mute_off
vg_mute_on
vg_close
vg_cta
vg_cta_disabled
vg_timer
vg_privacy


Supersonic Integration
1. Install the Supersonic Plugin. (http://developers.ironsrc.com/ironsource-mobile-unity/plugin-integration/unity-plugin/)
1. IGNORE their instructions in their documentation to "Remove Supersonic ➣ Plugins ➣ Android ➣ Supersonic ➣ AndroidManifest.xml". Import this manifest file.
1. Mediated ad networks can run in either MoPub or Supersonic, not both. Remove other ad network Activities that you may have added to MoPub. e.g. comment out AdColony Activities.


Supersonic - Adapters
1. Install the Adapter files from Supersonic website. Straightforward integration.
AdColony : https://bintray.com/supersonic/unity-adapters/download_file?file_path=SupersonicAdColonyAdapter_v2.1.5.unitypackage
AppLovin : https://bintray.com/supersonic/unity-adapters/download_file?file_path=SupersonicApplovinAdapter_v2.1.4.unitypackage
ChartBoost : https://bintray.com/supersonic/unity-adapters/download_file?file_path=SupersonicChartboostAdapter_v2.1.6.unitypackage
Unity :https://bintray.com/supersonic/unity-adapters/download_file?file_path=SupersonicUnityAdsAdapter_v2.1.3.unitypackage
HyperMx : https://bintray.com/supersonic/unity-adapters/download_file?file_path=SupersonicHyprMXAdapter_v2.1.2.unitypackage


PushWoosh
1. DOwnload SDK from https://github.com/Pushwoosh/pushwoosh-unity
1. When importing Package, dont include Google Play JARs nor android-support-v4.jar
1. Edit PlayServices resolver to include 
gcm
iid
location

### IOS ###

## Supersonic 
1) Download and import the Supersonic Unity plugin from the following link.
http://developers.ironsrc.com/ironsource-mobile-unity/plugin-integration/unity-plugin/#step-1

2) Download and import the required third party networks from the following link: 
http://developers.ironsrc.com/ironsource-mobile/unity/mediation-networks-unity/#step-1

3)  Mediated ad networks can run in either MoPub or Supersonic, not both. Remove other ad network Activities that you may have added to MoPub.