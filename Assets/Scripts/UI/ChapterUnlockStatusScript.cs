﻿using UnityEngine;
using UnityEngine.UI;

public class ChapterUnlockStatusScript : MonoBehaviour
{

	public GameObject chapterStatusObject;

	public GameObject unlockCoinObject;

	public int storyNumber;

	public Text unlockCoinValue;

	// Use this for initialization
	void OnEnable ()
	{
		if (GameConstants.checkIfStoryUnlocked (storyNumber)) {
			chapterStatusObject.SetActive (true);
			unlockCoinObject.SetActive (false);
		} else {
            unlockCoinValue.text = DynamicConstants.CoinsForUnlockingStory(storyNumber).ToString();
			unlockCoinObject.SetActive (true);
			chapterStatusObject.SetActive (false);
		}
    
	}
	
	
}
