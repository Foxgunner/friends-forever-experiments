﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Prime31;

public class PromoImageHead : MonoBehaviour
{

	public GameObject yoyoObject;
	RawImage yoyoImage;

	public bool isPromo1 = false;
	public bool isPromo2 = false;
	public bool isPromo3 = false;
	public bool isBanner = false;
	private bool bHasNewTexture = false;

	public string clickUrl = "";

	private bool imageChanged = false;

	void Start ()
	{
		yoyoImage = gameObject.GetComponent<RawImage> ();

		//print ("my name is " + YoyoImageCache.promoURL1.ToString());
		/*if (YoyoImageCache.bUrlLoad)
			{
				//yoyoImage.texture = YoyoImageCache.wwwUrl.texture;
				//yoyoObject.renderer.material.mainTexture = yoyoImage;
				
				if (YoyoImageCache.bUrlLoad && isPromo1)
					yoyoImage.texture = YoyoImageCache.promoURL1.texture;
			else if(YoyoImageCache.bUrlLoad && isPromo2)
				yoyoImage.texture = YoyoImageCache.promoURL2.texture;
				else yoyoObject.gameObject.SetActive (false);

			if(yoyoImage.texture == null)
				yoyoObject.SetActive(false);
			}
			//else yoyoObject.transform.GetComponentInChildren<Transform>().gameObject.SetActive (false);
*/

		if (isBanner)
			transform.parent.parent.gameObject.SetActive (false);

		//if (YoyoImageCache.bUrlLoad)
		{
			//print ("my name is " + YoyoImageCache.promoURL1.ToString());
			//StartCoroutine("LoadImages");
			
			if (G2WDynamic.promoTex1 != null && isPromo1) {
				yoyoImage.texture = G2WDynamic.promoTex1;
				//YoyoImageCache.promoTex1 = yoyoImage.texture;
			} else if (G2WDynamic.promoTex2 != null && isPromo2) {
				yoyoImage.texture = G2WDynamic.promoTex2;
				//YoyoImageCache.promoTex2 = yoyoImage.texture;
			} else if (G2WDynamic.promoTex3 != null && isPromo3) {
				yoyoImage.texture = G2WDynamic.promoTex3;
				//YoyoImageCache.promoTex2 = yoyoImage.texture;
			}
//			else if(G2WDynamic.bannerTex != null && isBanner)
//			{
//				yoyoImage.texture = YoyoImageCache.bannerTex;
//
//				if(GameConstants.ComboPurchased)
//					gameObject.SetActive(false);
//				//YoyoImageCache.promoTex2 = yoyoImage.texture;
//			}
//			else yoyoObject.gameObject.SetActive (false);
			
			if (yoyoImage.texture == null)
				yoyoObject.SetActive (false);

		}
	}

	void Update ()
	{
		

		if (G2WDynamic.bUrlLoad) {
			//yoyoImage.texture = YoyoImageCache.wwwUrl.texture;
			//yoyoObject.renderer.material.mainTexture = yoyoImage;
			
			bHasNewTexture = true;
		}
		
		if (bHasNewTexture && imageChanged == false) {
			//print ("my name is " + YoyoImageCache.promoURL1.ToString());
			//StartCoroutine("LoadImages");
			
			if (G2WDynamic.bUrlLoad && isPromo1) {
				yoyoImage.texture = G2WDynamic.promoURL1.texture;
				G2WDynamic.promoTex1 = yoyoImage.texture;
			} else if (G2WDynamic.bUrlLoad && isPromo2) {
				yoyoImage.texture = G2WDynamic.promoURL2.texture;
				G2WDynamic.promoTex2 = yoyoImage.texture;
			} else if (G2WDynamic.bUrlLoad && isPromo3) {
				yoyoImage.texture = G2WDynamic.promoURL3.texture;
				G2WDynamic.promoTex3 = yoyoImage.texture;
			} else
				yoyoObject.gameObject.SetActive (false);
			
			if (yoyoImage.texture == null)
				yoyoObject.SetActive (false);
			
			imageChanged = true;
			
		}

		//else yoyoObject.transform.GetComponentInChildren<Transform>().gameObject.SetActive (false);

	}

	IEnumerator LoadImages ()
	{
		print ("here");
		if (G2WDynamic.bUrlLoad && isPromo1)
			yoyoImage.texture = G2WDynamic.promoURL1.texture;
		else if (G2WDynamic.bUrlLoad && isPromo2)
			yoyoImage.texture = G2WDynamic.promoURL2.texture;

		yield return G2WDynamic.promoURL1;

		yield return G2WDynamic.promoURL2;
		print ("there");
	}

	public void OpenURL ()
	{
		print (G2WDynamic.bUrlLoad);

		if (Application.internetReachability != NetworkReachability.NotReachable) {

			if (G2WDynamic.bUrlLoad) {
//			var dict = new Dictionary<string,string> ();
//			if(isPromo1)
//			{
//				dict.Add ("Properties", YoyoImageCache.promoCampaign1);
//				FlurryHelper.LogEvent ("Cross promotion - 1", dict);
//			}
//			else if (isPromo2)
//			{
//				dict.Add("Properties", YoyoImageCache.promoCampaign2);
//				FlurryHelper.LogEvent ("Cross promotion - 2", dict);
//			}
//			else if (isPromo3)
//			{
//				dict.Add("Properties", YoyoImageCache.promoCampaign3);
//				FlurryHelper.LogEvent ("Cross promotion - 3", dict);
//			}
//				else if (isBanner)
//				{
//					dict.Add("Properties", YoyoImageCache.bannerPromoCampaign);
//					FlurryHelper.LogEvent ("Cross promotion - 4", dict);
//				}
//
//
			

				if (isPromo1) {
					print (G2WDynamic.promoURL1.ToString ());
					Application.OpenURL (G2WDynamic.clickURL1.ToString ());
					G2WGAHelper.LogEvent ("Cross promotion - 1", "Image URL", G2WDynamic.promoURL1.url, 1);
					G2WGAHelper.LogEvent ("Cross promotion - 1", "Session", GameConstants.sessionCount.ToString(), 1);
					G2WGAHelper.LogEvent ("Cross promotion - 1", "Day", DateMaster.instance.returnDays().ToString(), 1);
					G2WGAHelper.LogEvent ("Cross promotion - 1", "Last Unlocked Level", GameConstants.getStoryName() + ": Chapter" + GameConstants.currentChapterNumber, 1);
					 G2WGAHelper.LogRichEvent("Also Play 1");
				} else if (isPromo2) {
					Application.OpenURL (G2WDynamic.clickURL2.ToString ());
					G2WGAHelper.LogEvent ("Cross promotion - 2", "Image URL", G2WDynamic.promoURL2.url, 1);
					G2WGAHelper.LogEvent ("Cross promotion - 2", "Session", GameConstants.sessionCount.ToString(), 1);
					G2WGAHelper.LogEvent ("Cross promotion - 2", "Day", DateMaster.instance.returnDays().ToString(), 1);
					G2WGAHelper.LogEvent ("Cross promotion - 2", "Last Unlocked Level", GameConstants.getStoryName() + ": Chapter" + GameConstants.currentChapterNumber, 1);
					G2WGAHelper.LogRichEvent("Also Play 2");
				} else if (isPromo3) {
					Application.OpenURL (G2WDynamic.clickURL3.ToString ());
					G2WGAHelper.LogEvent ("Cross promotion - 3", "Image URL", G2WDynamic.promoURL3.url, 1);
					G2WGAHelper.LogEvent ("Cross promotion - 3", "Session", GameConstants.sessionCount.ToString(), 1);
					G2WGAHelper.LogEvent ("Cross promotion - 3", "Day", DateMaster.instance.returnDays().ToString(), 1);
					G2WGAHelper.LogEvent ("Cross promotion - 3", "Last Unlocked Level", GameConstants.getStoryName() + ": Chapter" + GameConstants.currentChapterNumber, 1);
					G2WGAHelper.LogRichEvent("Also Play 3");
				} else if (isBanner) {
					if (Application.internetReachability != NetworkReachability.NotReachable) {

						//if(AdsCustom.ads.banner != null)
						{
							//	if(!AdsCustom.ads.banner.visible)
							//		Application.OpenURL (YoyoImageCache.bannerPromoClickURL.ToString());
						}
						//Application.OpenURL (YoyoImageCache.bannerPromoClickURL.ToString());

					}
				}
			} else {


//				var dict = new Dictionary<string,string> ();
//				if(isPromo1)
//				{
//					dict.Add ("Properties", "PF");
//					FlurryHelper.LogEvent ("Cross promotion - 1", dict);
//				}
//				else if (isPromo2)
//				{
//					dict.Add("Properties", "HSBP");
//					FlurryHelper.LogEvent ("Cross promotion - 2", dict);
//				}
//				else if(isPromo3)
//				{
//					dict.Add("Properties", "ACP_IOS");
//					FlurryHelper.LogEvent ("Cross promotion - 3", dict);
//				}
				//if (!externalLinkclicked) {
				//FlurryHelper.LogEvent ("Promo Clicked", dict);
				//externalLinkclicked = true;
				


				if (isPromo1) {
//					print(YoyoImageCache.promoURL1.ToString());
					#if UNITY_ANDROID
					Application.OpenURL ("https://play.google.com/store/apps/details?id=com.games2win.drivetodate&referrer=utm_source%3Dyoyo%26utm_campaign%3Ddtd_android_also");
					#elif UNITY_IOS
					Application.OpenURL ("https://itunes.apple.com/app/apple-store/id1104911262?pt=273704&ct=dtd_also_ios&mt=8");
					#endif
				} else if (isPromo2) {
					#if UNITY_ANDROID
					Application.OpenURL ("https://play.google.com/store/apps/details?id=air.com.games2win.highschoolromance&referrer=utm_source%3Dalso%26utm_campaign%3Dhsr_android_also");
					#elif UNITY_IOS
					Application.OpenURL ("http://itunes.apple.com/app/apple-store/id975482811?pt=273704&ct=hsr_also_ios&mt=8");
					#endif

				} else if (isPromo3) {
					#if UNITY_ANDROID
					Application.OpenURL ("https://play.google.com/store/apps/details?id=air.com.games2win.highschoolpranks&referrer=utm_source%3Dalso%26utm_campaign%3Dhsp_android_also");
					#elif UNITY_IOS
					Application.OpenURL ("https://itunes.apple.com/app/apple-store/id1124330263?pt=273704&ct=hsp_also_ios&mt=8");
					#endif

				} else if (isBanner) {
					
					#if UNITY_ANDROID
					//need to check.....
					#elif UNITY_IOS
					Application.OpenURL ("http://itunes.apple.com/app/apple-store/id516558326?pt=273704&ct=pf_also_ios&mt=8");
					#endif
				}
			
			}
		
		} else {
			#if UNITY_IOS  //pragma
			var buttons = new string[] { "OK" };
			EtceteraBinding.showAlertWithTitleMessageAndButtons ("No Internet", "Please connect to internet and try again later.", buttons);
			#elif UNITY_ANDROID
			EtceteraAndroid.showAlert("NO INTERNET CONNECTION.", "YOU ARE NOT CONNECTED TO INTERNET. TRY AGAIN LATER.", "OK");
			#endif
			//Application.OpenURL(clickUrl);
		}

	}


	bool externalLinkclicked = false;

	void OnEnable ()
	{
		externalLinkclicked = false;
	}

	void OnApplicationFocus (bool focus)
	{
		if (!focus) {
			externalLinkclicked = true;
		} else {
			externalLinkclicked = false;
		}
	}


}