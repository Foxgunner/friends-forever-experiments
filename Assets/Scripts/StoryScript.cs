﻿using UnityEngine;
using System.Collections;
using System.Linq;

using Unity.Linq;
using UnityEngine.SceneManagement;

using Fungus;
using Prime31;
using System;

public class StoryScript : MonoBehaviour
{

	public GameObject pauseObject;

	public Flowchart[] flowchart;

	private bool hasCoins;

	static StoryScript _storyscript;

	public DateTime levelStartTime;

    public DateTime levelOverTime;



    public static StoryScript  storyScript {
		get { return _storyscript; }
	}

	void OnEnable ()
	{
		Messenger.AddListener<string> (MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY, showPause);
		Messenger.MarkAsPermanent (MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY);

		Messenger.AddListener<string> (MessengerConstants.SOUND_CHANGED, soundStatus);
		Messenger.MarkAsPermanent (MessengerConstants.SOUND_CHANGED);
	}

	void OnDisable ()
	{
		Messenger.RemoveListener<string> (MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY, showPause);
		Messenger.RemoveListener<string> (MessengerConstants.SOUND_CHANGED, soundStatus);
	}

	void Start ()
	{
		_storyscript = this;
		levelStartTime = System.DateTime.Now;
		G2WBackButtonHelper.isPauseOn = false;
		G2WGAHelper.LogEvent ("Start Screen", "Seen");
		G2WGAHelper.LogRichEvent("Start Screen");
		G2WGAHelper.LogEvent ("Start Screen - " + GameConstants.getStoryName (), "Seen");
		if (GameConstants.currentStoryNumber != 1) {
			assignVariables ();
		}
		GameConstants.totalGameplays++;
		loadCurrentChapter ();
		if (PlayerPrefs.GetInt ("SoundStatus") == 1) {
			AudioListener.volume = 1;
		} else if (PlayerPrefs.GetInt ("SoundStatus") == 0) {
			AudioListener.volume = 0;
		}
        
    }

	public void assignVariables ()
	{ 
		flowchart = GameObject.FindObjectsOfType<Flowchart> ();
		if (GameConstants.currentStoryNumber == 5) {
			if (GameConstants.currentChapterNumber == 10 || GameConstants.currentChapterNumber == 18) { 
				if (StoreConstants.totalCoins >= DynamicConstants.CoinsRequiredForAccessory) {
					hasCoins = true;
				}
			} else { 
				if (StoreConstants.totalCoins >= DynamicConstants.CoinsRequiredForDialgoue) {
					hasCoins = true;
				}
			}
		} else {
			if (StoreConstants.totalCoins >= DynamicConstants.CoinsRequiredForDialgoue) {
				hasCoins = true;
			}
		}
        

		foreach (Flowchart f in flowchart) {
			if (GameConstants.PlayerName=="") {
				f.SetStringVariable ("name", "Vegeta");
			} else {
				f.SetStringVariable ("name", GameConstants.PlayerName);
			}
			f.SetBooleanVariable ("HasCoins", hasCoins);
			f.SetIntegerVariable ("Character", GameConstants.SelectedCharacter);
		}
		Debug.Log ("SELECTED CHARACTER" + GameConstants.SelectedCharacter + StoreConstants.totalCoins + DynamicConstants.CoinsRequiredForDialgoue);
	}


	void loadCurrentChapter ()
	{
#if UNITY_EDITOR
		// GameConstants.currentChapterNumber = 25;
#endif
		var levels1 = GameObject.Find ("Levels");
		for (int i = 1; i <= 30; i++) {
			var child1 = levels1.Child ("Level" + i);
			child1.gameObject.SetActive (false);
		}
		var child = levels1.Child ("Level" + GameConstants.currentChapterNumber);
		child.gameObject.SetActive (true);
		Debug.Log ("CHILD 1" + child);
	}

	public void showPause ()
	{
		//Time.timeScale = 0;
		G2WBackButtonHelper.isPauseOn = true;
		pauseObject.SetActive (true);
	}

	public void resumeGame ()
	{
		//Time.timeScale = 1;
		G2WBackButtonHelper.isPauseOn = false;
		pauseObject.SetActive (false);
	}

	void OnApplicationPause (bool pauseStatus)
	{

		if (pauseStatus == true) {
			showPause ();
		}
	}

	public void showPause (string s)
	{
		if (s == "Pause") {
			showPause ();
		} else if (s == "Resume") {
			resumeGame ();
		}
	}


	public void soundStatus (string s)
	{
		if (s == "Off") {
			AudioListener.volume = 0;
		} else if (s == "On") {
			AudioListener.volume = 1;	
		}
	}

//Checks if user has already purchased the dialogue option
	public void checkIfAlreadyUnlocked ()
	{ 

		flowchart = GameObject.FindObjectsOfType<Flowchart> ();

		foreach (Flowchart f in flowchart) {
			f.SetBooleanVariable ("isDialogueUnlocked", PlayerPrefsX.GetBool ("Story"+GameConstants.currentStoryNumber.ToString()+"Chapter" + GameConstants.currentChapterNumber.ToString () + "DialogueUnlock"));
		}
	}



}
