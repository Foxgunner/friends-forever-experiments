# Variable commands # {#variable_commands}

[TOC]
# Delete Save Key # {#DeleteSaveKey}
Deletes a saved value from permanent storage.

Defined in Fungus.DeleteSaveKey

Property | Type | Description
 --- | --- | ---
Key | System.String | 

# Load Variable # {#LoadVariable}
Loads a saved value and stores it in a Boolean, Integer, Float or String variable. If the key is not found then the variable is not modified.

Defined in Fungus.LoadVariable

Property | Type | Description
 --- | --- | ---
Key | System.String | 
Variable | Fungus.Variable | 

# Random Float # {#RandomFloat}
Sets an float variable to a random value in the defined range.

Defined in Fungus.RandomFloat

Property | Type | Description
 --- | --- | ---
Variable | Fungus.FloatVariable | 
Min Value | Fungus.FloatData | 
Max Value | Fungus.FloatData | 

# Random Integer # {#RandomInteger}
Sets an integer variable to a random value in the defined range.

Defined in Fungus.RandomInteger

Property | Type | Description
 --- | --- | ---
Variable | Fungus.IntegerVariable | 
Min Value | Fungus.IntegerData | 
Max Value | Fungus.IntegerData | 

# Read Text File # {#ReadTextFile}
Reads in a text file and stores the contents in a string variable

Defined in Fungus.ReadTextFile

Property | Type | Description
 --- | --- | ---
Text File | UnityEngine.TextAsset | 
String Variable | Fungus.StringVariable | 

# Reset # {#Reset}
Resets the state of all commands and variables in the Flowchart.

Defined in Fungus.Reset

Property | Type | Description
 --- | --- | ---
Reset Commands | System.Boolean | 
Reset Variables | System.Boolean | 

# Save Variable # {#SaveVariable}
Save an Boolean, Integer, Float or String variable to persistent storage using a string key. The value can be loaded again later using the Load Variable command. You can also use the Set Save Profile command to manage separate save profiles for multiple players.

Defined in Fungus.SaveVariable

Property | Type | Description
 --- | --- | ---
Key | System.String | 
Variable | Fungus.Variable | 

# Set Save Profile # {#SetSaveProfile}
Sets the active profile that the Save Variable and Load Variable commands will use. This is useful to crete multiple player save games. Once set, the profile applies across all Flowcharts and will also persist across scene loads.

Defined in Fungus.SetSaveProfile

Property | Type | Description
 --- | --- | ---
Save Profile Name | System.String | 

# Set Variable # {#SetVariable}
Sets a Boolean, Integer, Float or String variable to a new value using a simple arithmetic operation. The value can be a constant or reference another variable of the same type.

Defined in Fungus.SetVariable

Property | Type | Description
 --- | --- | ---
Variable | Fungus.Variable | 
Set Operator | Fungus.SetOperator | 
Boolean Data | Fungus.BooleanData | 
Integer Data | Fungus.IntegerData | 
Float Data | Fungus.FloatData | 
String Data | Fungus.StringDataMulti | 

