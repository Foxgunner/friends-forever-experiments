﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class MiscHelper : MonoBehaviour {

	/// <summary>
	/// Cast a ray to test if Input.mousePosition is over any UI object in EventSystem.current. This is a replacement
	/// for IsPointerOverGameObject() which does not work on Android in 4.6.0f3
	/// </summary>
	public static bool IsPointerOverUIObject() {
		// Referencing this code for GraphicRaycaster https://gist.github.com/stramit/ead7ca1f432f3c0f181f
		// the ray cast appears to require only eventData.position.
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
		return results.Count > 0;
	}



    public static bool isPointerOverPauseItems()
    {
        Debug.Log("IS PAUSE ON" + G2WBackButtonHelper.isPauseOn);
        if (EventSystem.current.currentSelectedGameObject != null)
		{
            if (EventSystem.current.currentSelectedGameObject.gameObject.transform.parent.name == "Canvas" || EventSystem.current.currentSelectedGameObject.gameObject.transform.parent.name == "ButtonGroup" || EventSystem.current.currentSelectedGameObject.gameObject.transform.parent.name == "TopPanel" || EventSystem.current.currentSelectedGameObject.gameObject.transform.parent.name == "PauseObject")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

return false;

    }
}
