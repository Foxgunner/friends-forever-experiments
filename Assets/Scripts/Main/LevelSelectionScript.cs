﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Fungus;
using Prime31;

public class LevelSelectionScript : MonoBehaviour
{
	public GameObject startReqPanel;
	public GameObject startChapPanel;

	public GameObject levelObjectPanel;
	public GameObject loadingObjectPanel;

	public GameObject characterSelectionPanel;

	public GameObject characterNamePanel;

	public GameObject outofMovesPopup;

	public GameObject rewardPopup;

	public Slider progressSlider;

	public GameObject panel;

	public GameObject tempLevelSelection;

	public static LevelSelectionScript _levelselection;


	public GameObject offlinePopup;

	public static LevelSelectionScript LevelSelection {
		get { return _levelselection; }
	}

	void OnEnable ()
	{
		Messenger.AddListener<string> (MessengerConstants.DISABLE_BACK_PANEL, hidePanel);
		Messenger.MarkAsPermanent (MessengerConstants.DISABLE_BACK_PANEL);
	}

	void OnDisable ()
	{
		Messenger.RemoveListener<string> (MessengerConstants.DISABLE_BACK_PANEL, hidePanel);
	}

	void Start ()
	{
		_levelselection = this;


		if (!P31Prefs.hasKey ("Story" + GameConstants.currentStoryNumber + "Chapter1")) {
			Debug.Log ("LOGG");
			GameConstants.currentChapterNumber = 1;
		} else { 
			//   GameConstants.currentChapterNumber = PlayerPrefs.GetInt("Story"+GameConstants.currentStoryNumber+"Chapter")
		}

		if (GameConstants.currentStoryNumber == 1) {
			if (GameConstants.currentChapterNumber > 30) {
				GameConstants.currentChapterNumber = 1;
			}
			chapterMoveRequirementStatus ();
		} else  {
			if (GameConstants.isStoryNameGiven) {
				//tempLevelSelection.SetActive(true);
			}
			if (GameConstants.currentChapterNumber > 30) {
				GameConstants.currentChapterNumber = 1;
			}
			chapterMoveRequirementStatus ();
			// if(GameConstants.isStory2NameGiven)
			// {
			// 		// If the chapter has been played already then don't show the "Move requirement" text
			// 		if (PlayerPrefs.GetInt ("Story"+GameConstants.currentStoryNumber+"Chapter" + GameConstants.currentChapterNumber) == 0) {
			// 			startReqPanel.SetActive (true);
			// 			startChapPanel.SetActive (false);
			// 		} else {
			// 			startChapPanel.SetActive (true);
			// 			startReqPanel.SetActive (false);
			// 		}
			// }
			// else
			// {
			// 	levelObjectPanel.SetActive(false);
			// 	characterSelectionPanel.SetActive(true);
			// }
		}
	}

	public void checkLevelStatus ()
	{
		   if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            GameConstants.offlinePlays++;
        }
		  if (GameConstants.offlinePlays <= DynamicConstants.offlinePlaysAllowed || Application.internetReachability != NetworkReachability.NotReachable)
        {
            if (GameConstants.turnedOnInternet)
            {
                G2WGAHelper.LogEvent("Online Post Popup", "Session", GameConstants.sessionCount.ToString(), 1);
                G2WGAHelper.LogEvent("Online Post Popup", "Day", DateMaster.instance.returnDays().ToString(), 1);
                G2WGAHelper.LogEvent("Online Post Popup", "Last Unlocked Level", GameConstants.getStoryName() + ": Chapter" + GameConstants.currentChapterNumber, 1);
                GameConstants.turnedOnInternet = false;
            }
            if (GameConstants.isStoryNameGiven)
            {
                loadNextLevel();
                //tempLevelSelection.SetActive(true);
            }
            else
            {
                levelObjectPanel.SetActive(false);
                characterSelectionPanel.SetActive(true);
            }
        }
        else
        {
            showOfflinePopup();
        }
	}

	private void showOfflinePopup()
    {
        G2WGAHelper.LogEvent("Offline Popup - Shown", "Session", GameConstants.sessionCount.ToString(), 1);
        G2WGAHelper.LogEvent("Offline Popup - Shown", "Day", DateMaster.instance.returnDays().ToString(), 1);
        G2WGAHelper.LogEvent("Offline Popup - Shown", "Last Unlocked Level", GameConstants.getStoryName() + ": Chapter" + GameConstants.currentChapterNumber, 1);

        offlinePopup.SetActive(true);
        GameConstants.turnedOnInternet = true;
    }

	public void loadNextLevel ()
	{
		Debug.Log ("CHAPTER VALUE" + P31Prefs.getInt ("Story" + GameConstants.currentStoryNumber + "Chapter" + GameConstants.currentChapterNumber));
		if (P31Prefs.getInt ("Story" + GameConstants.currentStoryNumber + "Chapter" + GameConstants.currentChapterNumber) == 0) {
			if (StoreConstants.totalMoves > 0) {
				G2WGAHelper.LogEvent (GameConstants.getStoryName () + ": " + "Start Chapter " + GameConstants.currentChapterNumber.ToString (), "Coins Balance", StoreConstants.totalCoins.ToString (), 1);
				G2WGAHelper.LogEvent (GameConstants.getStoryName () + ": " + "Start Chapter " + GameConstants.currentChapterNumber.ToString (), "Moves Balance", StoreConstants.totalMoves.ToString (), 1);
				if(GameConstants.currentChapterNumber == 1 || (GameConstants.currentChapterNumber % 5 == 0))
				{
                    G2WGAHelper.LogRichEvent("Story " + GameConstants.currentStoryNumber + " - Start Chapter "+ GameConstants.currentChapterNumber.ToString());
                }
				StoreConstants.totalMoves--;
				P31Prefs.setInt ("Story" + GameConstants.currentStoryNumber + "Chapter" + GameConstants.currentChapterNumber, 1);
				setHiddenObjectLevel ();
				startLevel (false);
			} else {
				levelObjectPanel.SetActive (false);
				outofMovesPopup.SetActive (true);
				Messenger.Broadcast<string> (MessengerConstants.START_TIMER, "");
				G2WGAHelper.LogEvent ("Out of Moves Popup - Shown", GameConstants.getStoryName (), "Chapter " + (GameConstants.currentChapterNumber - 1).ToString (), 1);
			}
		} else {
			G2WGAHelper.LogEvent (GameConstants.getStoryName () + ": " + "Start Chapter " + GameConstants.currentChapterNumber.ToString (), "Coins Balance", StoreConstants.totalCoins.ToString (), 1);
			G2WGAHelper.LogEvent (GameConstants.getStoryName () + ": " + "Start Chapter " + GameConstants.currentChapterNumber.ToString (), "Moves Balance", StoreConstants.totalMoves.ToString (), 1);
			if(GameConstants.currentChapterNumber == 1 || (GameConstants.currentChapterNumber % 5 == 0))
				{
                    G2WGAHelper.LogRichEvent("Story " + GameConstants.currentStoryNumber + " - Start Chapter "+ GameConstants.currentChapterNumber.ToString());
                }
			setHiddenObjectLevel ();
			startLevel (false);
		}
		characterNamePanel.SetActive (false);
	}

	public void loadRequiredLevel (int levelNumber)
	{
		GameConstants.currentChapterNumber = levelNumber;
		setHiddenObjectLevel ();
		startLevel (false);
	}

	public void loadRequiredHiddenObjectLevel (int levelNumber)
	{
		if (GameConstants.currentStoryNumber == 1) {
			GameConstants.currentHiddenObjectLevelNumber = levelNumber;
            
		} else if(GameConstants.currentStoryNumber == 2) {
			GameConstants.currentHiddenObjectLevelNumber = levelNumber + 6;
		}
		else if(GameConstants.currentStoryNumber == 3) {
			GameConstants.currentHiddenObjectLevelNumber = levelNumber + 12;
		}
		else if(GameConstants.currentStoryNumber == 4) {
			GameConstants.currentHiddenObjectLevelNumber = levelNumber + 18;
		}
		startLevel (true);
	}

	public void startLevel (bool isHiddenObject)
	{

		if (isHiddenObject) {
			GameConstants.currentGameState = GameStates.HIDDEN_OBJECT_MODE;
			SceneManager.LoadScene ("HiddenObjectScene");
		} else {
			GameConstants.currentGameState = GameStates.STORY_MODE;
			//SceneManager.LoadSceneAsync (GameConstants.currentSceneName);

			levelObjectPanel.SetActive (false);
			loadingObjectPanel.SetActive (true);
			animateProgressBar ();
			SceneManager.LoadScene (GameConstants.getSceneName ());
		}
	}

	public void setHiddenObjectLevel ()
	{
		if (GameConstants.currentStoryNumber == 1) {
			switch (GameConstants.currentChapterNumber) {
			case 5:
				GameConstants.currentHiddenObjectLevelNumber = 1;
				break;
			case 8:
				GameConstants.currentHiddenObjectLevelNumber = 2;
				break;
			case 11:
				GameConstants.currentHiddenObjectLevelNumber = 3;
				break;
			case 16:
				GameConstants.currentHiddenObjectLevelNumber = 4;
				break;
			case 21:
				GameConstants.currentHiddenObjectLevelNumber = 5;
				break;
			case 26:
				GameConstants.currentHiddenObjectLevelNumber = 6;
				break;
			}
		} else if (GameConstants.currentStoryNumber == 2) { 
			switch (GameConstants.currentChapterNumber) {
			case 6:
				GameConstants.currentHiddenObjectLevelNumber = 7;
				break;
			case 10:
				GameConstants.currentHiddenObjectLevelNumber = 8;
				break;
			case 12:
				GameConstants.currentHiddenObjectLevelNumber = 9;
				break;
			case 18:
				GameConstants.currentHiddenObjectLevelNumber = 10;
				break;
			case 23:
				GameConstants.currentHiddenObjectLevelNumber = 11;
				break;
			case 29:
				GameConstants.currentHiddenObjectLevelNumber = 12;
				break;
			}
		}
		else if (GameConstants.currentStoryNumber == 3) { 
			switch (GameConstants.currentChapterNumber) {
			case 2:
				GameConstants.currentHiddenObjectLevelNumber = 13;
				break;
			case 8:
				GameConstants.currentHiddenObjectLevelNumber = 14;
				break;
			case 11:
				GameConstants.currentHiddenObjectLevelNumber = 15;
				break;
			case 17:
				GameConstants.currentHiddenObjectLevelNumber = 16;
				break;
			case 22:
				GameConstants.currentHiddenObjectLevelNumber = 17;
				break;
			case 28:
				GameConstants.currentHiddenObjectLevelNumber = 18;
				break;
			}
		}
		else if (GameConstants.currentStoryNumber == 4) { 
			switch (GameConstants.currentChapterNumber) {
			case 5:
				GameConstants.currentHiddenObjectLevelNumber = 19;
				break;
			case 11:
				GameConstants.currentHiddenObjectLevelNumber = 20;
				break;
			case 16:
				GameConstants.currentHiddenObjectLevelNumber = 21;
				break;
			case 18:
				GameConstants.currentHiddenObjectLevelNumber = 22;
				break;
			case 25:
				GameConstants.currentHiddenObjectLevelNumber = 23;
				break;
			case 28:
				GameConstants.currentHiddenObjectLevelNumber = 24;
				break;
			}
		}
	}

	public void animateProgressBar ()
	{
		Hashtable param = new Hashtable ();
		param.Add ("easetype", Fungus.iTween.EaseType.easeInOutQuad);
		param.Add ("from", progressSlider.value);
		param.Add ("to", 1f);
		param.Add ("time", 5f);
		param.Add ("onupdate", "TweenValorSlider");
		param.Add ("ignoretimescale", true);
		iTween.ValueTo (progressSlider.gameObject, param);
	}

	void TweenValorSlider (float val)
	{

		progressSlider.value = val;

	}

	public void loadStore (string type)
	{
		G2WGAHelper.LogEvent ("Open Store", "Source", "Out of Moves", 1);
		G2WGAHelper.LogEvent ("Out of Moves Popup - Buy Moves", "Tapped");
		if (type == "coins") {
			StoreConstants.showCoinScreen = true;
		} else {
			StoreConstants.showCoinScreen = false;
		}
		panel.SetActive (true);
		outofMovesPopup.SetActive (false);
		levelObjectPanel.SetActive (true);
		SceneManager.LoadScene ("Store", LoadSceneMode.Additive);

	}

	public void hidePanel (string s)
	{
		panel.SetActive (false);
	}

	public void closeOutOfMoves ()
	{
		G2WGAHelper.LogEvent ("Out of Moves Popup - Close");
		levelObjectPanel.SetActive (true);
		outofMovesPopup.SetActive (false);
	}

	public void backButtonHandler ()
	{
		if (characterSelectionPanel.activeSelf) {
			//Messenger.Broadcast<string>(MessengerConstants.BACK_BUTTON_TAPPED, "LevelSelectionCharSelect");
		} else if (characterNamePanel.activeSelf) { 
			Messenger.Broadcast<string> (MessengerConstants.BACK_BUTTON_TAPPED, "LevelSelectionNameSubmit");
		} else {
			Messenger.Broadcast<string> (MessengerConstants.BACK_BUTTON_TAPPED, "LevelSelection");
		}
	}


	public void watchAVideoOutOfMoves ()
	{
		//if (GameConstants.isNetConnected ()) {
			G2WAdHelper.ShowRewardedVideo (GameConstants.RVP_InGameMoves,"Story");
			GameServicesHelper.UnlockAchievements(AchievementConstants.AVID_OBSERVER);
		//}
	}

	public void showCharacterNamePanel ()
	{
		characterSelectionPanel.SetActive (false);
		characterNamePanel.SetActive (true);
	}

	public void chapterMoveRequirementStatus ()
	{
		// If the chapter has been played already then don't show the "Move requirement" text
		if (P31Prefs.getInt ("Story" + GameConstants.currentStoryNumber + "Chapter" + GameConstants.currentChapterNumber) == 0) {
			startReqPanel.SetActive (true);
			startChapPanel.SetActive (false);
		} else {
			startChapPanel.SetActive (true);
			startReqPanel.SetActive (false);
		}

	}


}
