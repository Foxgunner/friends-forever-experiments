﻿using UnityEngine;
using System.Collections;

public class AccessoryScript : MonoBehaviour {

    public bool isNeckLace = true;

    public bool isBruise = false;

    public void OnEnable()
	{
        Debug.Log("ACCESSORY SCRIPT" + PlayerPrefsX.GetBool ("necklaceUnlocked") + GameConstants.currentChapterNumber);
        //for necklace
        if(isNeckLace)
        {
        if(GameConstants.currentChapterNumber == 18)
		{
            if(PlayerPrefsX.GetBool ("necklaceUnlocked")&&PlayerPrefsX.GetBool ("necklaceWear"))
            {
					GetComponent<SpriteRenderer> ().enabled = true;
                // gameObject.SetActive(true);
            }
            else
            {
					GetComponent<SpriteRenderer> ().enabled = false;
               //  gameObject.SetActive(false);
            }
        }
         else
        {
				GetComponent<SpriteRenderer> ().enabled = false;
           //  gameObject.SetActive(false);
        }
        }
        else
        {
            //for headgear
            if (isBruise)
            { 
                if (GameConstants.currentChapterNumber == 1)  // || GameConstants.currentChapterNumber == 25)
                {
                   
                        GetComponent<SpriteRenderer>().enabled = true;
                        // gameObject.SetActive(true);
            
                }
                else
                {
                    GetComponent<SpriteRenderer>().enabled = false;
                    // gameObject.SetActive(false);
                }
            }
            else
            {
                if (GameConstants.currentChapterNumber == 10)  // || GameConstants.currentChapterNumber == 25)
                {
                    Debug.Log("ACCESSORY SCRIPT" + PlayerPrefsX.GetBool("bagUnlocked") + PlayerPrefsX.GetBool("bagWear"));
                    if (PlayerPrefsX.GetBool("bagUnlocked") && PlayerPrefsX.GetBool("bagWear"))
                    {
                        GetComponent<SpriteRenderer>().enabled = true;
                        // gameObject.SetActive(true);
                    }
                    else
                    {
                        GetComponent<SpriteRenderer>().enabled = false;
                        //  gameObject.SetActive(false);
                    }
                }
                else
                {
                    GetComponent<SpriteRenderer>().enabled = false;
                    // gameObject.SetActive(false);
                }
            }
        }
    }
}
