﻿using UnityEngine;
using System.Collections;

public class PZAndroid : MonoBehaviour {    
	public static void SetEnabled(bool enabled) {
#if UNITY_ANDROID
		new AndroidJavaClass ("com.packetzoom.speed.PacketZoomClient")
			.CallStatic("setEnabled", enabled);
#endif
		}

	public static void Init(string appId, string apiKey) {
#if UNITY_ANDROID
		new AndroidJavaClass ("com.packetzoom.speed.PacketZoomClient")
			.CallStatic("init", new AndroidJavaClass("com.unity3d.player.UnityPlayer")
			            .GetStatic<AndroidJavaObject>("currentActivity"), appId, apiKey);
#endif
	}

	public static void EnableDebugLogs() {
#if UNITY_ANDROID
		AndroidJavaObject logValue = new AndroidJavaClass("com.packetzoom.speed.PZLogLevel").GetStatic<AndroidJavaObject>("pzlogDEBUG1");
		new AndroidJavaClass ("com.packetzoom.speed.PacketZoomClient")
			.CallStatic ("setLogLevel", logValue);
#endif
	}
}
