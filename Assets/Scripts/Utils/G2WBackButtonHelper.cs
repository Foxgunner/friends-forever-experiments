﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class G2WBackButtonHelper : MonoBehaviour
{

    private static G2WBackButtonHelper instance = null;

    public static bool isSettingsOn = false;

    public static bool isFeedBackOn = false;

    public static bool isPauseOn = false;

    public static bool isTutorialOn = false;

    public static bool ispopupOn = false;

    void Awake()
    {
        if (instance)
            Destroy(gameObject);

        instance = this;
        DontDestroyOnLoad(gameObject);

    }

    // Use this for initialization
    void Start()
    {

    }

    void OnEnable()
    {
        Messenger.AddListener<string>(MessengerConstants.BACK_BUTTON_TAPPED, backButtonhandler);
        Messenger.MarkAsPermanent(MessengerConstants.BACK_BUTTON_TAPPED);
    }

    void OnDisable()
    {
        Messenger.RemoveListener<string>(MessengerConstants.BACK_BUTTON_TAPPED, backButtonhandler);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !ispopupOn)

            {
                backButtonhandler("");
            }
    }

    public void backButtonhandler(string s)
    {
        if (GameConstants.isStoreLoaded)
        {
            GameConstants.isStoreLoaded = false;
            SceneManager.UnloadScene("Store");
            if (HOUIScript.isTimerNoCoinsPopOn)
            {
                Messenger.Broadcast<string>(MessengerConstants.BACK_FROM_STORE, "NoCoins");
            }
        }
        else if (GameConstants.currentGameState == GameStates.CHAPTER_SELECT && !GameConstants.isStoreLoaded)
        {
            GameConstants.currentGameState = GameStates.INTRO;
            SceneManager.LoadScene("IntroScene");
        }
        else if (GameConstants.currentGameState == GameStates.LEVEL_SELECT && !GameConstants.isStoreLoaded)
        {
            
         if (LevelSelectionScript._levelselection.characterSelectionPanel.activeSelf)
            {
                //LevelSelectionScript._levelselection.characterSelectionPanel.SetActive(false);
                //LevelSelectionScript._levelselection.levelObjectPanel.SetActive(true);
            }
            else if (LevelSelectionScript._levelselection.characterNamePanel.activeSelf)
            {
                LevelSelectionScript._levelselection.characterSelectionPanel.SetActive(true);
                LevelSelectionScript._levelselection.characterNamePanel.SetActive(false);
            }
            else 
            {
                GameConstants.currentGameState = GameStates.CHAPTER_SELECT;
                SceneManager.LoadScene("ChapterSelectionScreen");
            }
        }
        else if (GameConstants.currentGameState == GameStates.STORY_MODE || GameConstants.currentGameState == GameStates.HIDDEN_OBJECT_MODE)
        {


            if (!isTutorialOn && !isSettingsOn)
            {
                Debug.Log("BACK FOR PAUSE" + isPauseOn);
                if (isPauseOn)
                {
                    Messenger.Broadcast<string>(MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY, "Resume");
                }
                //else if (isPauseOn) {
                else if (!isPauseOn)
                {
                    Messenger.Broadcast<string>(MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY, "Pause");
                }
                //}
            }

            if (isSettingsOn)
            {
                Messenger.Broadcast<string>(MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY, "Settings");
            }




        }
        else if (GameConstants.currentGameState == GameStates.GAME_END && !GameConstants.isStoreLoaded)
        {
            if (isFeedBackOn)
            {
                Messenger.Broadcast<string>(MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY, "Feedback");
            }
        }

        if (isSettingsOn)
        {
            Messenger.Broadcast<string>(MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY, "Settings");
        }

    }

    void OnDestroy()
    {

    }
}
