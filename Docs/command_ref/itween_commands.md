# iTween commands # {#itween_commands}

[TOC]
# Look From # {#LookFrom}
Instantly rotates a GameObject to look at the supplied Vector3 then returns it to it's starting rotation over time.

Defined in Fungus.LookFrom

Property | Type | Description
 --- | --- | ---
_from Transform | Fungus.TransformData | 
_from Position | Fungus.Vector3Data | 
Axis | Fungus.iTweenAxis | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Look To # {#LookTo}
Rotates a GameObject to look at a supplied Transform or Vector3 over time.

Defined in Fungus.LookTo

Property | Type | Description
 --- | --- | ---
_to Transform | Fungus.TransformData | 
_to Position | Fungus.Vector3Data | 
Axis | Fungus.iTweenAxis | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Move Add # {#MoveAdd}
Moves a game object by a specified offset over time.

Defined in Fungus.MoveAdd

Property | Type | Description
 --- | --- | ---
_offset | Fungus.Vector3Data | 
Space | UnityEngine.Space | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Move From # {#MoveFrom}
Moves a game object from a specified position back to its starting position over time. The position can be defined by a transform in another object (using To Transform) or by setting an absolute position (using To Position, if To Transform is set to None).

Defined in Fungus.MoveFrom

Property | Type | Description
 --- | --- | ---
_from Transform | Fungus.TransformData | 
_from Position | Fungus.Vector3Data | 
Is Local | System.Boolean | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Move To # {#MoveTo}
Moves a game object to a specified position over time. The position can be defined by a transform in another object (using To Transform) or by setting an absolute position (using To Position, if To Transform is set to None).

Defined in Fungus.MoveTo

Property | Type | Description
 --- | --- | ---
_to Transform | Fungus.TransformData | 
_to Position | Fungus.Vector3Data | 
Is Local | System.Boolean | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Punch Position # {#PunchPosition}
Applies a jolt of force to a GameObject's position and wobbles it back to its initial position.

Defined in Fungus.PunchPosition

Property | Type | Description
 --- | --- | ---
_amount | Fungus.Vector3Data | 
Space | UnityEngine.Space | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Punch Rotation # {#PunchRotation}
Applies a jolt of force to a GameObject's rotation and wobbles it back to its initial rotation.

Defined in Fungus.PunchRotation

Property | Type | Description
 --- | --- | ---
_amount | Fungus.Vector3Data | 
Space | UnityEngine.Space | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Punch Scale # {#PunchScale}
Applies a jolt of force to a GameObject's scale and wobbles it back to its initial scale.

Defined in Fungus.PunchScale

Property | Type | Description
 --- | --- | ---
_amount | Fungus.Vector3Data | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Rotate Add # {#RotateAdd}
Rotates a game object by the specified angles over time.

Defined in Fungus.RotateAdd

Property | Type | Description
 --- | --- | ---
_offset | Fungus.Vector3Data | 
Space | UnityEngine.Space | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Rotate From # {#RotateFrom}
Rotates a game object from the specified angles back to its starting orientation over time.

Defined in Fungus.RotateFrom

Property | Type | Description
 --- | --- | ---
_from Transform | Fungus.TransformData | 
_from Rotation | Fungus.Vector3Data | 
Is Local | System.Boolean | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Rotate To # {#RotateTo}
Rotates a game object to the specified angles over time.

Defined in Fungus.RotateTo

Property | Type | Description
 --- | --- | ---
_to Transform | Fungus.TransformData | 
_to Rotation | Fungus.Vector3Data | 
Is Local | System.Boolean | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Scale Add # {#ScaleAdd}
Changes a game object's scale by a specified offset over time.

Defined in Fungus.ScaleAdd

Property | Type | Description
 --- | --- | ---
_offset | Fungus.Vector3Data | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Scale From # {#ScaleFrom}
Changes a game object's scale to the specified value and back to its original scale over time.

Defined in Fungus.ScaleFrom

Property | Type | Description
 --- | --- | ---
_from Transform | Fungus.TransformData | 
_from Scale | Fungus.Vector3Data | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Scale To # {#ScaleTo}
Changes a game object's scale to a specified value over time.

Defined in Fungus.ScaleTo

Property | Type | Description
 --- | --- | ---
_to Transform | Fungus.TransformData | 
_to Scale | Fungus.Vector3Data | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Shake Position # {#ShakePosition}
Randomly shakes a GameObject's position by a diminishing amount over time.

Defined in Fungus.ShakePosition

Property | Type | Description
 --- | --- | ---
_amount | Fungus.Vector3Data | 
Is Local | System.Boolean | 
Axis | Fungus.iTweenAxis | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Shake Rotation # {#ShakeRotation}
Randomly shakes a GameObject's rotation by a diminishing amount over time.

Defined in Fungus.ShakeRotation

Property | Type | Description
 --- | --- | ---
_amount | Fungus.Vector3Data | 
Space | UnityEngine.Space | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Shake Scale # {#ShakeScale}
Randomly shakes a GameObject's rotation by a diminishing amount over time.

Defined in Fungus.ShakeScale

Property | Type | Description
 --- | --- | ---
_amount | Fungus.Vector3Data | 
_target Object | Fungus.GameObjectData | 
_tween Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Ease Type | Fungus.iTween+EaseType | 
Loop Type | Fungus.iTween+LoopType | 
Stop Previous Tweens | System.Boolean | 
Wait Until Finished | System.Boolean | 

# Stop Tween # {#StopTween}
Stops an active iTween by name.

Defined in Fungus.StopTween

Property | Type | Description
 --- | --- | ---
_tween Name | Fungus.StringData | 

# Stop Tweens # {#StopTweens}
Stop all active iTweens in the current scene.

Defined in Fungus.StopTweens
