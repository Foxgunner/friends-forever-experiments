﻿using UnityEngine;
using System.Collections;

public class WrongClick : MonoBehaviour {
	
	public Transform wrongSprite;
	public Animator anim;
	float shake = 0.3f;
	float shakeAmount = 0.06f;
	float decreaseFactor = 1.0f;
	//bool isShake = false;
	// Use this for initialization
	void Start () {
		anim.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		Shake();
	}
	
	void Shake() {
		if (shake > 0){
			wrongSprite.localPosition = Vector3.zero + Random.insideUnitSphere * shakeAmount;
			shake -= Time.deltaTime * decreaseFactor;
		}
		else {
			shake = 0f;
			wrongSprite.localPosition = Vector3.zero;
		}
	}
}
