﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using Prime31;

public class G2WPNSIOS : MonoBehaviour
{
	private static G2WPNSIOS instance = null;
	#if UNITY_IOS
	string devicePushToken = "";
	string pns_appName = GameConstants.APPName;

	private string regURL;
	private const string host = "pns.games2win.com/apns/devices/register?task=register";

	//PNS ID provided by the publishing team
	private string pnsID = GameConstants.PNSID;

	//TODO PNS Environment = should be 'production' in the release build
	private const string pnsEnvironment = "production";
	//"sandbox";

	private string pnsAppVersion = GameConstants.APPVersion;

	//public GameObject badgeObject;

	void Awake ()
	{
		if (instance)
			Destroy (gameObject);

		instance = this;
		DontDestroyOnLoad (gameObject);

	}

	void Start ()
	{
		EtceteraManager.remoteRegistrationSucceededEvent += HandleEtceteraManagerremoteRegistrationSucceeded;
		EtceteraBinding.registerForRemoteNotifications (P31RemoteNotificationType.Alert | P31RemoteNotificationType.Badge | P31RemoteNotificationType.Sound);
	}


	void HandleEtceteraManagerremoteRegistrationSucceeded (string obj)
	{
		EtceteraManager.remoteRegistrationSucceededEvent -= HandleEtceteraManagerremoteRegistrationSucceeded;
	
		devicePushToken = obj;
		
		string pushBadge = "disabled";
		string pushAlert = "disabled";
		string pushSound = "disabled";
		
		if (UnityEngine.iOS.NotificationServices.enabledNotificationTypes == UnityEngine.iOS.NotificationType.Badge) {
			pushBadge = "enabled";
		} else if (UnityEngine.iOS.NotificationServices.enabledNotificationTypes == UnityEngine.iOS.NotificationType.Alert) {
			pushAlert = "enabled";
		} else if (UnityEngine.iOS.NotificationServices.enabledNotificationTypes == UnityEngine.iOS.NotificationType.Sound) {
			pushSound = "enabled";
		} else if (UnityEngine.iOS.NotificationServices.enabledNotificationTypes == (UnityEngine.iOS.NotificationType.Badge | UnityEngine.iOS.NotificationType.Alert)) {
			pushBadge = "enabled";
			pushAlert = "enabled";
		} else if (UnityEngine.iOS.NotificationServices.enabledNotificationTypes == (UnityEngine.iOS.NotificationType.Badge | UnityEngine.iOS.NotificationType.Sound)) {
			pushBadge = "enabled";
			pushSound = "enabled";
		} else if (UnityEngine.iOS.NotificationServices.enabledNotificationTypes == (UnityEngine.iOS.NotificationType.Alert | UnityEngine.iOS.NotificationType.Sound)) {
			pushAlert = "enabled";
			pushSound = "enabled";
		} else if (UnityEngine.iOS.NotificationServices.enabledNotificationTypes == (UnityEngine.iOS.NotificationType.Alert | UnityEngine.iOS.NotificationType.Sound | UnityEngine.iOS.NotificationType.Badge)) {
			pushBadge = "enabled";
			pushAlert = "enabled";
			pushSound = "enabled";
		}
			
		StringBuilder urlString = new StringBuilder ();
		
		urlString.AppendFormat ("&appName={0}", WWW.EscapeURL (pns_appName)); // provided by G2W
		urlString.AppendFormat ("&app_id={0}", WWW.EscapeURL (pnsID)); // provided by G2W
		urlString.AppendFormat ("&appversion={0}", WWW.EscapeURL (pnsAppVersion)); // provided by G2W
		
		urlString.AppendFormat ("&deviceuid={0}", WWW.EscapeURL ("00"));
		urlString.AppendFormat ("&devicetoken={0}", WWW.EscapeURL (devicePushToken));
		urlString.AppendFormat ("&devicename={0}", WWW.EscapeURL (SystemInfo.deviceName));
		urlString.AppendFormat ("&devicemodel={0}", WWW.EscapeURL (SystemInfo.deviceModel));
		urlString.AppendFormat ("&deviceversion={0}", WWW.EscapeURL (SystemInfo.graphicsDeviceVersion));
		
		urlString.AppendFormat ("&pushbadge={0}", WWW.EscapeURL (pushBadge));
		urlString.AppendFormat ("&pushalert={0}", WWW.EscapeURL (pushAlert));
		urlString.AppendFormat ("&pushsound={0}", WWW.EscapeURL (pushSound));
		
		urlString.AppendFormat ("&apns_env={0}", WWW.EscapeURL (pnsEnvironment));
		
		Debug.Log ("http://" + (host + urlString));
		regURL = urlString.ToString ();
		
		StartCoroutine ("RegDeviceForPush");
		ProcessBadges ();
	}

	IEnumerator RegDeviceForPush ()
	{
		WWW w = new WWW ("http://" + (host + regURL), UTF8Encoding.UTF8.GetBytes (regURL));
		yield return w;
		
		Debug.Log ("EEEEE: " + w.error);
	}

	public void ProcessBadges ()
	{
		int badges = 0;

		if (UnityEngine.iOS.NotificationServices.remoteNotificationCount > 0) {
			//badges = NotificationServices.GetRemoteNotification (NotificationServices.remoteNotificationCount - 1).applicationIconBadgeNumber;
			
			for (int i = 0; i < UnityEngine.iOS.NotificationServices.remoteNotificationCount; i++) {
				var not = UnityEngine.iOS.NotificationServices.GetRemoteNotification (i);
				badges += not.applicationIconBadgeNumber;
				
				if (!string.IsNullOrEmpty (((string)not.userInfo ["open_url"]))) {
					Application.OpenURL (((string)not.userInfo ["open_url"]));
				}
			}
			
			UnityEngine.iOS.NotificationServices.ClearRemoteNotifications ();
			
			EtceteraBinding.setBadgeCount (badges);
		} else {
			badges = EtceteraBinding.getBadgeCount ();
		}
		
//		if (badges == 0 || GameConstants.isBadgeClcked) {
//			badgeObject.SetActive (false);
//		} else if (badges > 0 && !GameConstants.isBadgeClcked) {
//			badgeObject.SetActive (true);
//			badgeID.text = badges.ToString ();
//		}
	}
	#endif
}
