﻿// <copyright file="SampleDependencies.cs" company="Google Inc.">
// Copyright (C) 2015 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//    limitations under the License.
// </copyright>

using System;
using System.Collections.Generic;
using UnityEditor;


namespace Prime31
{
	[InitializeOnLoad]
	public class PlayGameServicesDependencies : AssetPostprocessor
	{
		public static object svcSupport;


		static PlayGameServicesDependencies()
		{
			RegisterDependencies();
		}


		/// <summary>
		/// Registers the dependencies needed by this plugin.
		/// </summary>
		public static void RegisterDependencies()
		{
#if UNITY_ANDROID
			RegisterAndroidDependencies();
#endif
		}


		public static void RegisterAndroidDependencies()
		{
			var playServicesSupport = Google.VersionHandler.FindClass( "Google.JarResolver", "Google.JarResolver.PlayServicesSupport" );
			if( playServicesSupport == null )
				return;

			svcSupport = svcSupport ?? Google.VersionHandler.InvokeStaticMethod( playServicesSupport, "CreateInstance",
				new object[] {
					"GooglePlayGames",
					EditorPrefs.GetString( "AndroidSdkRoot" ),
					"ProjectSettings"
				} );

			Google.VersionHandler.InvokeInstanceMethod( svcSupport, "DependOn",
				new object[] {
					"com.google.android.gms",
					"play-services-games",
					"9.8.0"
				},
				namedArgs: new Dictionary<string, object>() {
					{ "packageIds", new string[] { "extra-google-m2repository" } }
				} );
				
			Google.VersionHandler.InvokeInstanceMethod( svcSupport, "DependOn",
				new object[] {
					"com.google.android.gms",
					"play-services-plus",
					"9.8.0"
				},
				namedArgs: new Dictionary<string, object>() {
					{ "packageIds", new string[] { "extra-google-m2repository" } }
				} );

			Google.VersionHandler.InvokeInstanceMethod( svcSupport, "DependOn",
				new object[] {
					"com.google.android.gms",
					"play-services-ads",
					"9.8.0"
				},
				namedArgs: new Dictionary<string, object>() {
					{ "packageIds", new string[] { "extra-google-m2repository" } }
				} );

			Google.VersionHandler.InvokeInstanceMethod( svcSupport, "DependOn",
				new object[] {
					"com.google.android.gms",
					"play-services-gcm",
					"9.8.0"
				},
				namedArgs: new Dictionary<string, object>() {
					{ "packageIds", new string[] { "extra-google-m2repository" } }
				} );

			Google.VersionHandler.InvokeInstanceMethod( svcSupport, "DependOn",
				new object[] {
					"com.google.android.gms",
					"play-services-iid",
					"9.8.0"
				},
				namedArgs: new Dictionary<string, object>() {
					{ "packageIds", new string[] { "extra-google-m2repository" } }
				} );

			Google.VersionHandler.InvokeInstanceMethod( svcSupport, "DependOn",
				new object[] {
					"com.google.android.gms",
					"play-services-location",
					"9.8.0"
				},
				namedArgs: new Dictionary<string, object>() {
					{ "packageIds", new string[] { "extra-google-m2repository" } }
				} );
		}

	}
}