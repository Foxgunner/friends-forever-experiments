﻿using UnityEngine;
using System.Collections;

public class AchievementConstants  {

#region STORY1 ACHIEVEMENTS
#if UNITY_ANDROID
public static string EAGER_READER = "CgkIgPHnqbsYEAIQAQ";
#elif UNITY_IOS
public static string EAGER_READER = "ff_eager_reader";
#endif

#if UNITY_ANDROID
public static string KEEN_EYE = "CgkIgPHnqbsYEAIQAg";
#elif UNITY_IOS
public static string KEEN_EYE = "ff_keen_eye";
#endif

#if UNITY_ANDROID
public static string THE_EARLY_READER = "CgkIgPHnqbsYEAIQAw";
#elif UNITY_IOS
public static string THE_EARLY_READER = "ff_the_early_reader";
#endif

#if UNITY_ANDROID
public static string CLUE_CATCHER = "CgkIgPHnqbsYEAIQBA";
#elif UNITY_IOS
public static string CLUE_CATCHER = "ff_clue_catcher";
#endif

#if UNITY_ANDROID
public static string AVID_OBSERVER = "CgkIgPHnqbsYEAIQBQ";
#elif UNITY_IOS
public static string AVID_OBSERVER = "ff_avid_observer";
#endif

#endregion

#region STORY2 ACHIEVEMENTS
#if UNITY_ANDROID
public static string DILIGENT_READER = "CgkIgPHnqbsYEAIQBg";
#elif UNITY_IOS
public static string DILIGENT_READER = "ff_diligent_reader";
#endif

#if UNITY_ANDROID
public static string FASTEST_FIVE = "CgkIgPHnqbsYEAIQBw";
#elif UNITY_IOS
public static string FASTEST_FIVE = "ff_fastest_five";
#endif

#if UNITY_ANDROID
public static string INQUISITIVE_READER = "CgkIgPHnqbsYEAIQCA";
#elif UNITY_IOS
public static string INQUISITIVE_READER = "ff_inquisitive_reader";
#endif

#if UNITY_ANDROID
public static string DETECTIVE_EXTRAORDINAIRE = "CgkIgPHnqbsYEAIQCQ";
#elif UNITY_IOS
public static string DETECTIVE_EXTRAORDINAIRE = "ff_detective_extraordinaire";
#endif

#if UNITY_ANDROID
public static string BLOSSOMING_ROMANCE = "CgkIgPHnqbsYEAIQCg";
#elif UNITY_IOS
public static string BLOSSOMING_ROMANCE = "ff_blossoming_romance";
#endif

#endregion

#region STORY3 ACHIEVEMENTS
#if UNITY_ANDROID
public static string HERO_OF_THE_DAY = "CgkIgPHnqbsYEAIQDw";
#elif UNITY_IOS
public static string HERO_OF_THE_DAY = "ff_diligent_reader";
#endif

#if UNITY_ANDROID
public static string LOYAL_READER = "CgkIgPHnqbsYEAIQCw";
#elif UNITY_IOS
public static string LOYAL_READER = "ff_fastest_five";
#endif

#if UNITY_ANDROID
public static string TANGO_EXPERT = "CgkIgPHnqbsYEAIQDA";
#elif UNITY_IOS
public static string TANGO_EXPERT = "ff_inquisitive_reader";
#endif

#if UNITY_ANDROID
public static string DETAIL_ORIENTED = "CgkIgPHnqbsYEAIQDQ";
#elif UNITY_IOS
public static string DETAIL_ORIENTED = "ff_detective_extraordinaire";
#endif

#if UNITY_ANDROID
public static string AGENT_TO_THE_RESCUE = "CgkIgPHnqbsYEAIQDg";
#elif UNITY_IOS
public static string AGENT_TO_THE_RESCUE = "ff_blossoming_romance";
#endif

#endregion

#region STORY4 ACHIEVEMENTS
#if UNITY_ANDROID
public static string ARDENT_READER = "CgkIgPHnqbsYEAIQEA";
#elif UNITY_IOS
public static string ARDENT_READER = "ff_diligent_reader";
#endif

#if UNITY_ANDROID
public static string LAB_ROMANCE = "CgkIgPHnqbsYEAIQEQ";
#elif UNITY_IOS
public static string LAB_ROMANCE = "ff_fastest_five";
#endif

#if UNITY_ANDROID
public static string SWEET_ENCOUNTER = "CgkIgPHnqbsYEAIQEg";
#elif UNITY_IOS
public static string SWEET_ENCOUNTER = "ff_inquisitive_reader";
#endif

#if UNITY_ANDROID
public static string LIVING_IT_UP = "CgkIgPHnqbsYEAIQEw";
#elif UNITY_IOS
public static string LIVING_IT_UP = "ff_detective_extraordinaire";
#endif

#if UNITY_ANDROID
public static string A_FRIEND_IN_NEED = "CgkIgPHnqbsYEAIQFA";
#elif UNITY_IOS
public static string A_FRIEND_IN_NEED = "ff_blossoming_romance";
#endif

#if UNITY_ANDROID
public static string THE_HEIST_PRO = "CgkIgPHnqbsYEAIQFQ";
#elif UNITY_IOS
public static string THE_HEIST_PRO = "ff_blossoming_romance";
#endif

#endregion

#region STORY5 ACHIEVEMENTS
#if UNITY_ANDROID
public static string SIGNS_SPECIALIST = "CgkIgPHnqbsYEAIQFw";
#elif UNITY_IOS
public static string SIGNS_SPECIALIST = "ff_diligent_reader";
#endif

#if UNITY_ANDROID
public static string BACKSTORY_PRO = "CgkIgPHnqbsYEAIQGA";
#elif UNITY_IOS
public static string BACKSTORY_PRO = "ff_fastest_five";
#endif

#if UNITY_ANDROID
public static string REAL_ENCOUNTER = "CgkIgPHnqbsYEAIQGQ";
#elif UNITY_IOS
public static string REAL_ENCOUNTER = "ff_inquisitive_reader";
#endif

#if UNITY_ANDROID
public static string RECALL_EXPERT = "CgkIgPHnqbsYEAIQGg";
#elif UNITY_IOS
public static string RECALL_EXPERT = "ff_detective_extraordinaire";
#endif

#if UNITY_ANDROID
public static string CURIOUS_READER = "CgkIgPHnqbsYEAIQGw";
#elif UNITY_IOS
public static string CURIOUS_READER = "ff_blossoming_romance";
#endif

#if UNITY_ANDROID
public static string DEDICATED_READER = "CgkIgPHnqbsYEAIQFg";
#elif UNITY_IOS
public static string DEDICATED_READER = "ff_blossoming_romance";
#endif

#endregion

#region STORY6 ACHIEVEMENTS
#if UNITY_ANDROID
public static string A_BLAST_FROM_THE_PAST = "CgkIgPHnqbsYEAIQHA";
#elif UNITY_IOS
public static string A_BLAST_FROM_THE_PAST = "CgkIgPHnqbsYEAIQHA";
#endif

#if UNITY_ANDROID
public static string TAKES_TWO_TO_TANGO = "CgkIgPHnqbsYEAIQHQ";
#elif UNITY_IOS
public static string TAKES_TWO_TO_TANGO = "CgkIgPHnqbsYEAIQHQ";
#endif

#if UNITY_ANDROID
public static string CURIOUS_CHOICES = "CgkIgPHnqbsYEAIQHg";
#elif UNITY_IOS
public static string CURIOUS_CHOICES = "CgkIgPHnqbsYEAIQHg";
#endif

#if UNITY_ANDROID
public static string ON_THE_FENCE = "CgkIgPHnqbsYEAIQHw";
#elif UNITY_IOS
public static string ON_THE_FENCE = "CgkIgPHnqbsYEAIQHw";
#endif

#if UNITY_ANDROID
public static string MARCUS_ANGELS = "CgkIgPHnqbsYEAIQIA";
#elif UNITY_IOS
public static string MARCUS_ANGELS = "CgkIgPHnqbsYEAIQIA";
#endif

#if UNITY_ANDROID
public static string CASE_CRACKED = "CgkIgPHnqbsYEAIQIQ";
#elif UNITY_IOS
public static string CASE_CRACKED = "CgkIgPHnqbsYEAIQIQ";
#endif

#endregion
}

