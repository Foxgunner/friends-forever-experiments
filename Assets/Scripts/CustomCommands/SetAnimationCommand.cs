﻿using UnityEngine;
using System.Collections;
using Fungus;
using System.Collections.Generic;
using System.Linq;

[CommandInfo("G2W",
    "Set Animation",
    "Sets a trigger parameter on an Animator component to control a Unity animation edited for using multiple animations at once")]
[AddComponentMenu("")]
[ExecuteInEditMode]
public class SetAnimationCommand : Command
{

    [Tooltip("Character to display")]
    [SerializeField]
	public Character character;

    private AnimatorData _animator;

    private StringData _parameterNamePrimary;

    private StringData _parameterNameSecondary;

    [Tooltip("Check if using multiple animations")]
    [SerializeField]
	public bool useMultipleAnimation = true;

    public virtual bool UseMultipleAnimation { get { return useMultipleAnimation; } }

    [Tooltip("The shape of the easing curve applied to the animation")]
    [SerializeField]
    public PrimaryAnimTypes primaryAnimation = PrimaryAnimTypes.MouthSpeaking;

    public virtual PrimaryAnimTypes PrimaryAnimation { get { return primaryAnimation; } }

    [Tooltip("The shape of the easing curve applied to the animation")]
    [SerializeField]
	public SecondaryAnimTypes secondaryAnimation = SecondaryAnimTypes.Idle1;

    [Tooltip("Flip character.")]
    [SerializeField]
    protected bool isFlipped = false;

    [Tooltip("Duration to wait for")]
    [SerializeField]
	public FloatData _duration = new FloatData(0);


    public virtual SecondaryAnimTypes SecondaryAnimation { get { return secondaryAnimation; } }

    /// <summary>
    /// Character to display.
    /// </summary>
    public virtual Character _Character { get { return character; } set { character = value; } }



    #region Public members

    public enum PrimaryAnimTypes
    {
        MouthAnger,
        MouthBored,
        MouthDisgust,
        MouthHappiness,
        MouthNone,
        MouthSpeaking,
        MouthSpeakingShort,
        MouthExtraShortSpeaking
    }

    public enum SecondaryAnimTypes
    {

        Anger,
        Arrogance,
        Blushing,
        Bored,
        Confusion,
        Disagree,
        Disgust,
        Embarrassment,
        EyeWink,
        FearWorry,
        Greeting,
        Happiness,
        Idle1,
        Idle2,
        Irritated,
        Laughter,
        Listening,
        Mischief,
        Questioning,
        RaisedEyebrow,
        Sadness,
        Smiling,
        Surprise,
        Talking,
        Victorious,
        Walking,
		PhoneTalking,
        Surprise2,
		Fainting,
		Dancing,
		GiveSomething,
		TakeSomething,
		GiveKiss,
		TakeKiss,
        GiveCoffee,
        GiveHD,
		GiveMap,
		GivePendulum,
		GivePendant,
		GiveAmulet,
		GiveHealthBar,
		GiveVoodooDoll,
 		GiveChips,
		GiveRingBox,
		GiveSwitch,
 		TakeCoffee,
 		TakeHD,
		TakeChips,
		TakeAmulet,
		TakePendant,
		TakePendulum,
		TakeMap,
		TakeHealthBar,
		TakeRingBox


    }

    #endregion

    void GetSecondaryAnimFunction()
    {
        switch (secondaryAnimation)
        {
            case SecondaryAnimTypes.Anger:
                _parameterNameSecondary.Value = "Anger";
                break;
            case SecondaryAnimTypes.Arrogance:
                _parameterNameSecondary.Value = "Arrogance";
                break;
            case SecondaryAnimTypes.Blushing:
                _parameterNameSecondary.Value = "Blushing";
                break;
            case SecondaryAnimTypes.Bored:
                _parameterNameSecondary.Value = "Bored";
                break;
            case SecondaryAnimTypes.Confusion:
                _parameterNameSecondary.Value = "Confusion";
                break;
            case SecondaryAnimTypes.Disagree:
                _parameterNameSecondary.Value = "Disagree";
                break;
            case SecondaryAnimTypes.Disgust:
                _parameterNameSecondary.Value = "Disgust";
                break;
            case SecondaryAnimTypes.Embarrassment:
                _parameterNameSecondary.Value = "Embarrassment";
                break;
            case SecondaryAnimTypes.EyeWink:
                _parameterNameSecondary.Value = "EyeWink";
                break;
            case SecondaryAnimTypes.FearWorry:
                _parameterNameSecondary.Value = "FearWorry";
                break;
            case SecondaryAnimTypes.Greeting:
                _parameterNameSecondary.Value = "Greeting";
                break;
            case SecondaryAnimTypes.Happiness:
                _parameterNameSecondary.Value = "Happiness";
                break;
            case SecondaryAnimTypes.Idle1:
                _parameterNameSecondary.Value = "Idle1";
                break;
            case SecondaryAnimTypes.Idle2:
                _parameterNameSecondary.Value = "Idle2";
                break;
            case SecondaryAnimTypes.Irritated:
                _parameterNameSecondary.Value = "Irritated";
                break;
            case SecondaryAnimTypes.Laughter:
                _parameterNameSecondary.Value = "Laughter";
                break;
            case SecondaryAnimTypes.Listening:
                _parameterNameSecondary.Value = "Listening";
                break;
            case SecondaryAnimTypes.Mischief:
                _parameterNameSecondary.Value = "Mischief";
                break;
            case SecondaryAnimTypes.Questioning:
                _parameterNameSecondary.Value = "Questioning";
                break;
            case SecondaryAnimTypes.RaisedEyebrow:
                _parameterNameSecondary.Value = "RaisedEyebrow";
                break;
            case SecondaryAnimTypes.Sadness:
                _parameterNameSecondary.Value = "Sadness";
                break;
            case SecondaryAnimTypes.Smiling:
                _parameterNameSecondary.Value = "Smiling";
                break;
            case SecondaryAnimTypes.Surprise:
                _parameterNameSecondary.Value = "Surprise";
                break;
            case SecondaryAnimTypes.Talking:
                _parameterNameSecondary.Value = "Talking";
                break;
            case SecondaryAnimTypes.Victorious:
                _parameterNameSecondary.Value = "Victorious";
                break;
			case SecondaryAnimTypes.Walking:
                _parameterNameSecondary.Value = "Walking";
                break;
			case SecondaryAnimTypes.PhoneTalking:
			_parameterNameSecondary.Value = "PhoneTalking";
			break;
            case SecondaryAnimTypes.Fainting:
			_parameterNameSecondary.Value = "Fainting";
			break;
			case SecondaryAnimTypes.Dancing:
			_parameterNameSecondary.Value = "Dancing";
			break;
			case SecondaryAnimTypes.Surprise2:
			_parameterNameSecondary.Value = "Surprise2";
			break;
			case SecondaryAnimTypes.GiveSomething:
			_parameterNameSecondary.Value = "GiveSomething";
			break;
			case SecondaryAnimTypes.TakeSomething:
			_parameterNameSecondary.Value = "TakeSomething";
			break;
			case SecondaryAnimTypes.GiveKiss:
			_parameterNameSecondary.Value = "GiveKiss";
			break;
			case SecondaryAnimTypes.TakeKiss:
			_parameterNameSecondary.Value = "TakeKiss";
			break;
       case SecondaryAnimTypes.GiveChips:
                _parameterNameSecondary.Value = "GiveSomethingChips";
                break;
                 case SecondaryAnimTypes.GiveCoffee:
                _parameterNameSecondary.Value = "GiveSomethingCoffee";
                break;
                 case SecondaryAnimTypes.GiveHD:
                _parameterNameSecondary.Value = "GiveSomethingHD";
                break;
                case SecondaryAnimTypes.TakeChips:
                _parameterNameSecondary.Value = "TakeSomethingChips";
                break;
                case SecondaryAnimTypes.TakeCoffee:
                _parameterNameSecondary.Value = "TakeSomethingCoffee";
                break;
                case SecondaryAnimTypes.TakeHD:
                _parameterNameSecondary.Value = "TakeSomethingHD";
                break;
		case SecondaryAnimTypes.GiveMap:
			_parameterNameSecondary.Value = "GiveMap";
			break;
		case SecondaryAnimTypes.GivePendant:
			_parameterNameSecondary.Value = "GivePendant";
			break;
		case SecondaryAnimTypes.GivePendulum:
			_parameterNameSecondary.Value = "GivePendulum";
			break;
		case SecondaryAnimTypes.GiveAmulet:
			_parameterNameSecondary.Value = "GiveAmulet";
			break;
		case SecondaryAnimTypes.GiveVoodooDoll:
			_parameterNameSecondary.Value = "GiveVoodooDoll";
			break;
		case SecondaryAnimTypes.GiveHealthBar:
			_parameterNameSecondary.Value = "GiveHealthBar";
			break;
		case SecondaryAnimTypes.TakeAmulet:
			_parameterNameSecondary.Value = "TakeAmulet";
			break;
		case SecondaryAnimTypes.TakeHealthBar:
			_parameterNameSecondary.Value = "TakeHealthBar";
			break;
		case SecondaryAnimTypes.TakePendulum:
			_parameterNameSecondary.Value = "TakePendulum";
			break;
		case SecondaryAnimTypes.TakePendant:
			_parameterNameSecondary.Value = "TakePendant";
			break;
		case SecondaryAnimTypes.TakeMap:
			_parameterNameSecondary.Value = "TakeMap";
			break;
		case SecondaryAnimTypes.TakeRingBox:
			_parameterNameSecondary.Value = "Take RingBox";
			break;
		case SecondaryAnimTypes.GiveRingBox:
			_parameterNameSecondary.Value = "Give RingBox";
			break;
		case SecondaryAnimTypes.GiveSwitch:
			_parameterNameSecondary.Value = "Give switch";
			break;

        }
    }

    void GetPrimaryAnimFunction()
    {
        switch (primaryAnimation)
        {
            case PrimaryAnimTypes.MouthAnger:
                _parameterNamePrimary.Value = "MouthAnger";
                break;
            case PrimaryAnimTypes.MouthBored:
                _parameterNamePrimary.Value = "MouthBored";
                break;
            case PrimaryAnimTypes.MouthDisgust:
                _parameterNamePrimary.Value = "MouthDisgust";
                break;
            case PrimaryAnimTypes.MouthHappiness:
                _parameterNamePrimary.Value = "MouthHappiness";
                break;
            case PrimaryAnimTypes.MouthNone:
                _parameterNamePrimary.Value = "MouthNone";
                break;
            case PrimaryAnimTypes.MouthSpeaking:
                _parameterNamePrimary.Value = "MouthSpeaking";
                break;
            case PrimaryAnimTypes.MouthSpeakingShort:
                _parameterNamePrimary.Value = "MouthSpeakingShort";
                break;
                case PrimaryAnimTypes.MouthExtraShortSpeaking:
			_parameterNamePrimary.Value = "MouthExtraShortSpeaking";
                break;
        }
    }

    protected virtual void OnEnable()
    {
        if (character != null)
        {
            _animator.Value = character.gameObject.GetComponentInChildren<Animator>();
        }

    }

    public override void OnEnter()
    {
        if (character != null)
        {
            _animator.Value = character.gameObject.GetComponentInChildren<Animator>();
        }
        if (isFlipped)
        {
            character.gameObject.transform.localScale = new Vector3(-(character.gameObject.transform.localScale.x), character.gameObject.transform.localScale.y, character.gameObject.transform.localScale.z);
        }
        Invoke("OnWaitComplete", _duration.Value);

        Continue();
    }

    protected virtual void OnWaitComplete()
    {
        if (_animator.Value != null)
        {
            if (useMultipleAnimation)
            {
                GetPrimaryAnimFunction ();
				Debug.Log ("PRIMARY ANIMATION" + _parameterNamePrimary.Value + GameConstants.currentStoryNumber);
				if(GameConstants.currentStoryNumber == 1 || GameConstants.currentStoryNumber == 2)
				{
				_animator.Value.SetTrigger (_parameterNamePrimary.Value);
				}
				else 
				{
				_animator.Value.Play(_parameterNamePrimary.Value, -1, 0);
				}
            }

            GetSecondaryAnimFunction ();
			Debug.Log ("SECONDARY ANIMATION" + _parameterNameSecondary.Value);
			if(GameConstants.currentStoryNumber == 1 || GameConstants.currentStoryNumber == 2)
				{
			_animator.Value.SetTrigger(_parameterNameSecondary.Value);
			}
				else 
				{
				_animator.Value.Play(_parameterNameSecondary.Value, -1, 0);
				}
        }


    }

    public override string GetSummary()
    {
        //		if (_animator.Value == null)
        //		{
        //			return "Error: No animator selected";
        //}
        string namePrefix = "";
        if (character != null)
        {
            namePrefix = character.NameText + ": ";

            if (character.NameText == "")
            {
                namePrefix = character.gameObject.name + ": ";
            }
        }

        string prim_animname = "";
        if (useMultipleAnimation)
        {
            prim_animname = primaryAnimation.ToString();
        }

        return namePrefix + " (" + prim_animname + " " + secondaryAnimation.ToString() + ")";

    }

    public override Color GetButtonColor()
    {
        Color32 c = new Color32();

        if (useMultipleAnimation)
        {
            c = new Color32(155, 255, 212, 255);
        }
        else
        {
            c = new Color32(235, 244, 88, 255);
        }

        return c;
    }



}
