﻿using UnityEngine;
using System.Collections;
using Unity.Linq;

public class SortingScript : MonoBehaviour {

	//public GameObject itemObject;
	void Start()
	{
		//sortObjects ();
	}

	public void sortObjects(GameObject itemObject,string layername)
	{
		foreach (var item in itemObject.Descendants())
		{
			if (item.GetComponent<SpriteRenderer> () != null) {
				item.GetComponent<SpriteRenderer> ().sortingLayerName = layername;
			}
		}
	}
}
