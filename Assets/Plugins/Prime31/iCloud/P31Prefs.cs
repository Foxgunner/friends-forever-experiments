using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Prime31;



#pragma warning disable 0162, 0649

namespace Prime31
{
	public class P31Prefs
	{
		public static bool iCloudDocumentStoreAvailable { get { return _iCloudDocumentStoreAvailable; } }

		private static bool _iCloudDocumentStoreAvailable;


		#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR
		static P31Prefs()
		{
		_iCloudDocumentStoreAvailable = iCloudBinding.documentStoreAvailable();
		}
		#endif


		public static bool synchronize ()
		{
			#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR
			return iCloudBinding.synchronize();
			#endif
			PlayerPrefs.Save ();
			return true;
		}


		public static bool hasKey (string key)
		{
			#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR

			if (iCloudBinding.getUbiquityIdentityToken() != null){
				return iCloudBinding.hasKey( key );
			}
			return PlayerPrefs.HasKey (key);
			#else
			return PlayerPrefs.HasKey (key);
			#endif
		}


		public static List<object> allKeys ()
		{
			#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR
			return iCloudBinding.allKeys();
			#else
			return new List<object> ();
			#endif
		}


		public static void removeObjectForKey (string key)
		{
			#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR
			iCloudBinding.removeObjectForKey( key );
			#endif
			PlayerPrefs.DeleteKey (key);
		}


		public static void removeAll ()
		{
			#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR
			iCloudBinding.removeAll();
			#endif
			PlayerPrefs.DeleteAll ();
		}


		public static void setInt (string key, int val)
		{
			Debug.Log ("setInt >> key: " + key + " value: " + val);
			#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR
			PlayerPrefs.SetInt( key, val );
			iCloudBinding.setInt( val, key );
			#endif
			PlayerPrefs.SetInt (key, val);
		}


		public static int getInt (string key, int def = 0)
		{
			
			#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR
			if (iCloudBinding.getUbiquityIdentityToken() != null){
				if (hasKey(key)){
				Debug.Log ("getInt cloud >> key: " + key + " value: " + iCloudBinding.intForKey( key ));
				return iCloudBinding.intForKey( key );
				}
			}
			Debug.Log ("getInt PlayerPrefs >> key:  " + key + " value: " + def);
			return PlayerPrefs.GetInt( key, def );

			#else
			Debug.Log ("getInt PlayerPrefs2 >> key:  " + key + " value: " + def);
			return PlayerPrefs.GetInt (key, def);
			#endif
		}


		public static void setFloat (string key, float val)
		{

			#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR
			iCloudBinding.setDouble( val, key );
			#endif
			PlayerPrefs.SetFloat (key, val);
		}


		public static float getFloat (string key, float def = 0.0f)
		{
			Debug.Log ("getFloat");
			#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR
			if (iCloudBinding.getUbiquityIdentityToken() != null){
			if (hasKey(key)){
			return iCloudBinding.doubleForKey( key );
			}
			}
			return PlayerPrefs.GetFloat( key, def  );

			#else
			return PlayerPrefs.GetFloat (key, def);
			#endif
		}


		public static void setString (string key, string val)
		{
			#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR
			iCloudBinding.setString( val, key );
			#endif
			PlayerPrefs.SetString (key, val);
		}


		public static string getString (string key)
		{
			#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR
			if (iCloudBinding.getUbiquityIdentityToken() != null){
			if (hasKey(key)){
			return iCloudBinding.stringForKey( key );
			}
			}
			return PlayerPrefs.GetString( key);

			#else
			return PlayerPrefs.GetString (key);
			#endif
		}


		public static void setBool (string key, bool val)
		{
			#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR
			PlayerPrefs.SetInt( key, val ? 1 : 0 );
			iCloudBinding.setBool( val, key );
			#endif
			PlayerPrefs.SetInt (key, val ? 1 : 0);
		}


		public static bool getBool (string key)
		{
			#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR
			if (iCloudBinding.getUbiquityIdentityToken() != null){
			if (hasKey(key)){
			return iCloudBinding.boolForKey( key );
			}
			}
			return PlayerPrefs.GetInt( key, 0 ) == 1;

			#else
			return PlayerPrefs.GetInt (key, 0) == 1;
			#endif
		}


		public static void setDictionary (string key, Dictionary<string,object> val)
		{
			var json = Prime31.Json.encode (val);

			#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR
			iCloudBinding.setDictionary( json, key );
			#endif
			PlayerPrefs.SetString (key, json);
		}


		public static IDictionary getDictionary (string key)
		{
			#if ( UNITY_IOS || UNITY_TVOS ) && !UNITY_EDITOR
			return iCloudBinding.dictionaryForKey( key );
			#else
			var str = PlayerPrefs.GetString (key);
			return str.dictionaryFromJson ();
			#endif
		}

	}

}
#pragma warning restore 0162, 0649
