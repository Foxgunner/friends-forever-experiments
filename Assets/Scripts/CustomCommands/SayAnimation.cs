// This code is part of the Fungus library (http://fungusgames.com) maintained by Chris Gregan (http://twitter.com/gofungus).
// It is released for free under the MIT open source license (https://github.com/snozbot/fungus/blob/master/LICENSE)

using UnityEngine;
using Fungus;

/// <summary>
/// Writes text in a dialog box.
/// </summary>
[CommandInfo("G2W",
                 "Dialogue Animation",
                 "Writes text in a dialog box and sets an animation.")]
[AddComponentMenu("")]
public class SayAnimation : Command, ILocalizable
{
    // Removed this tooltip as users's reported it obscures the text box
    [TextArea(5, 10)]
    [SerializeField]
	public string storyText = "";

    [Tooltip("Notes about this story text for other authors, localization, etc.")]
    [SerializeField]
    protected string description = "";

    [Tooltip("Character that is speaking")]
    [SerializeField]
	public Character character;

    [Tooltip("Portrait that represents speaking character")]
    [SerializeField]
    protected Sprite portrait;

    [Tooltip("Voiceover audio to play when writing the text")]
    [SerializeField]
    protected AudioClip voiceOverClip;

    [Tooltip("Always show this Say text when the command is executed multiple times")]
    [SerializeField]
    protected bool showAlways = true;

    [Tooltip("Number of times to show this Say text when the command is executed multiple times")]
    [SerializeField]
    protected int showCount = 1;

    [Tooltip("Type this text in the previous dialog box.")]
    [SerializeField]
    protected bool extendPrevious = false;

    [Tooltip("Fade out the dialog box when writing has finished and not waiting for input.")]
    [SerializeField]
    protected bool fadeWhenDone = true;

    [Tooltip("Wait for player to click before continuing.")]
    [SerializeField]
    protected bool waitForClick = true;

    [Tooltip("Stop playing voiceover when text finishes writing.")]
    [SerializeField]
    protected bool stopVoiceover = true;

    [Tooltip("Sets the active Say dialog with a reference to a Say Dialog object in the scene. All story text will now display using this Say Dialog.")]
    [SerializeField]
    protected SayDialog setSayDialog;

    protected int executionCount;

    private AnimatorData _animator;

    private StringData _parameterNamePrimary;

    private StringData _parameterNameSecondary;

    [Tooltip("The shape of the easing curve applied to the animation")]
    [SerializeField]
	public PrimaryAnimTypes primaryAnimation = PrimaryAnimTypes.MouthSpeaking;

    public virtual PrimaryAnimTypes PrimaryAnimation { get { return primaryAnimation; } }

    [Tooltip("The shape of the easing curve applied to the animation")]
    [SerializeField]
	public SecondaryAnimTypes secondaryAnimation = SecondaryAnimTypes.Idle1;

    [Tooltip("Flip character.")]
    [SerializeField]
    protected bool isFlipped = false;

    #region Public members

    #region Public members

    public enum PrimaryAnimTypes
    {
        MouthAnger,
        MouthBored,
        MouthDisgust,
        MouthHappiness,
        MouthNone,
        MouthSpeaking,
        MouthSpeakingShort,
        MouthExtraShortSpeaking
    }

    public enum SecondaryAnimTypes
    {

        Anger,
        Arrogance,
        Blushing,
        Bored,
        Confusion,
        Disagree,
        Disgust,
        Embarrassment,
        EyeWink,
        FearWorry,
        Greeting,
        Happiness,
        Idle1,
        Idle2,
        Irritated,
        Laughter,
        Listening,
        Mischief,
        Questioning,
        RaisedEyebrow,
        Sadness,
        Smiling,
        Surprise,
        Talking,
        Victorious,
        Walking,
        PhoneTalking,
        Surprise2,
        Fainting,
        Dancing,
        GiveSomething,
        TakeSomething,
        GiveKiss,
        TakeKiss,
        GiveCoffee,
        GiveHD,
 GiveChips,
		GiveSwitch,
 TakeCoffee,
 TakeHD,
TakeChips,
		GiveMap,
		GivePendant,
		GivePendulum,
		GiveVoodooDoll,
		GiveAmulet,
		GiveHealthBar,
		GiveRingBox,
		TakeMap,
		TakePendant,
		TakePendulum,
		TakeAmulet,
		TakeHealthBar,
		TakeRingBox





    }

    #endregion

    void GetSecondaryAnimFunction()
    {
        switch (secondaryAnimation)
        {
            case SecondaryAnimTypes.Anger:
                _parameterNameSecondary.Value = "Anger";
                break;
            case SecondaryAnimTypes.Arrogance:
                _parameterNameSecondary.Value = "Arrogance";
                break;
            case SecondaryAnimTypes.Blushing:
                _parameterNameSecondary.Value = "Blushing";
                break;
            case SecondaryAnimTypes.Bored:
                _parameterNameSecondary.Value = "Bored";
                break;
            case SecondaryAnimTypes.Confusion:
                _parameterNameSecondary.Value = "Confusion";
                break;
            case SecondaryAnimTypes.Disagree:
                _parameterNameSecondary.Value = "Disagree";
                break;
            case SecondaryAnimTypes.Disgust:
                _parameterNameSecondary.Value = "Disgust";
                break;
            case SecondaryAnimTypes.Embarrassment:
                _parameterNameSecondary.Value = "Embarrassment";
                break;
            case SecondaryAnimTypes.EyeWink:
                _parameterNameSecondary.Value = "EyeWink";
                break;
            case SecondaryAnimTypes.FearWorry:
                if (GameConstants.currentStoryNumber > 2)
                {
                    _parameterNameSecondary.Value = "FearWorry2";
                }
                else
                {
                    _parameterNameSecondary.Value = "FearWorry";
                }
                break;
            case SecondaryAnimTypes.Greeting:
                _parameterNameSecondary.Value = "Greeting";
                break;
            case SecondaryAnimTypes.Happiness:
                _parameterNameSecondary.Value = "Happiness";
                break;
            case SecondaryAnimTypes.Idle1:
                _parameterNameSecondary.Value = "Idle1";
                break;
            case SecondaryAnimTypes.Idle2:
                _parameterNameSecondary.Value = "Idle2";
                break;
            case SecondaryAnimTypes.Irritated:
                _parameterNameSecondary.Value = "Irritated";
                break;
            case SecondaryAnimTypes.Laughter:
                _parameterNameSecondary.Value = "Laughter";
                break;
            case SecondaryAnimTypes.Listening:
                _parameterNameSecondary.Value = "Listening";
                break;
            case SecondaryAnimTypes.Mischief:
                _parameterNameSecondary.Value = "Mischief";
                break;
            case SecondaryAnimTypes.Questioning:
                _parameterNameSecondary.Value = "Questioning";
                break;
            case SecondaryAnimTypes.RaisedEyebrow:
                _parameterNameSecondary.Value = "RaisedEyebrow";
                break;
            case SecondaryAnimTypes.Sadness:
                _parameterNameSecondary.Value = "Sadness";
                break;
            case SecondaryAnimTypes.Smiling:
                _parameterNameSecondary.Value = "Smiling";
                break;
            case SecondaryAnimTypes.Surprise:
                _parameterNameSecondary.Value = "Surprise";
                break;
            case SecondaryAnimTypes.Talking:
                _parameterNameSecondary.Value = "Talking";
                break;
            case SecondaryAnimTypes.Victorious:
                _parameterNameSecondary.Value = "Victorious";
                break;
            case SecondaryAnimTypes.Walking:
                _parameterNameSecondary.Value = "Walking";
                break;
            case SecondaryAnimTypes.PhoneTalking:
                _parameterNameSecondary.Value = "PhoneTalking";
                break;
            case SecondaryAnimTypes.Fainting:
                _parameterNameSecondary.Value = "Fainting";
                break;
            case SecondaryAnimTypes.Dancing:
                _parameterNameSecondary.Value = "Dancing";
                break;
            case SecondaryAnimTypes.Surprise2:
                _parameterNameSecondary.Value = "Surprise2";
                break;
            case SecondaryAnimTypes.GiveSomething:
                _parameterNameSecondary.Value = "GiveSomething";
                break;
                 case SecondaryAnimTypes.GiveChips:
                _parameterNameSecondary.Value = "GiveSomethingChips";
                break;
                 case SecondaryAnimTypes.GiveCoffee:
                _parameterNameSecondary.Value = "GiveSomethingCoffee";
                break;
                 case SecondaryAnimTypes.GiveHD:
                _parameterNameSecondary.Value = "GiveSomethingHD";
                break;
		            case SecondaryAnimTypes.TakeSomething:
                _parameterNameSecondary.Value = "TakeSomething";
                break;
                case SecondaryAnimTypes.TakeChips:
                _parameterNameSecondary.Value = "TakeSomethingChips";
                break;
                case SecondaryAnimTypes.TakeCoffee:
                _parameterNameSecondary.Value = "TakeSomethingCoffee";
                break;
                case SecondaryAnimTypes.TakeHD:
                _parameterNameSecondary.Value = "TakeSomethingHD";
                break;
            case SecondaryAnimTypes.GiveKiss:
                _parameterNameSecondary.Value = "GiveKiss";
                break;
            case SecondaryAnimTypes.TakeKiss:
                _parameterNameSecondary.Value = "TakeKiss";
                break;
		case SecondaryAnimTypes.GiveMap:
			_parameterNameSecondary.Value = "GiveMap";
			break;
		case SecondaryAnimTypes.GivePendant:
			_parameterNameSecondary.Value = "GivePendant";
			break;
		case SecondaryAnimTypes.GivePendulum:
			_parameterNameSecondary.Value = "GivePendulum";
			break;
		case SecondaryAnimTypes.GiveAmulet:
			_parameterNameSecondary.Value = "GiveAmulet";
			break;
		case SecondaryAnimTypes.GiveVoodooDoll:
			_parameterNameSecondary.Value = "GiveVoodooDoll";
			break;
		case SecondaryAnimTypes.GiveHealthBar:
			_parameterNameSecondary.Value = "GiveHealthBar";
			break;
		case SecondaryAnimTypes.TakeAmulet:
			_parameterNameSecondary.Value = "TakeAmulet";
			break;
		case SecondaryAnimTypes.TakeHealthBar:
			_parameterNameSecondary.Value = "TakeHealthBar";
			break;
		case SecondaryAnimTypes.TakePendulum:
			_parameterNameSecondary.Value = "TakePendulum";
			break;
		case SecondaryAnimTypes.TakePendant:
			_parameterNameSecondary.Value = "TakePendant";
			break;
		case SecondaryAnimTypes.TakeMap:
			_parameterNameSecondary.Value = "TakeMap";
			break;
		case SecondaryAnimTypes.TakeRingBox:
			_parameterNameSecondary.Value = "Take RingBox";
			break;
		case SecondaryAnimTypes.GiveRingBox:
			_parameterNameSecondary.Value = "Give RingBox";
			break;
		case SecondaryAnimTypes.GiveSwitch:
			_parameterNameSecondary.Value = "Give switch";
			break;
			
        }
    }

    void GetPrimaryAnimFunction()
    {
        switch (primaryAnimation)
        {
            case PrimaryAnimTypes.MouthAnger:
                _parameterNamePrimary.Value = "MouthAnger";
                break;
            case PrimaryAnimTypes.MouthBored:
                _parameterNamePrimary.Value = "MouthBored";
                break;
            case PrimaryAnimTypes.MouthDisgust:
                _parameterNamePrimary.Value = "MouthDisgust";
                break;
            case PrimaryAnimTypes.MouthHappiness:
                _parameterNamePrimary.Value = "MouthHappiness";
                break;
            case PrimaryAnimTypes.MouthNone:
                _parameterNamePrimary.Value = "MouthNone";
                break;
            case PrimaryAnimTypes.MouthSpeaking:
                _parameterNamePrimary.Value = "MouthSpeaking";
                break;
            case PrimaryAnimTypes.MouthSpeakingShort:
                _parameterNamePrimary.Value = "MouthSpeakingShort";
                break;
            case PrimaryAnimTypes.MouthExtraShortSpeaking:
                _parameterNamePrimary.Value = "MouthExtraShortSpeaking";
                break;
        }
    }

    /// <summary>
    /// Character that is speaking.
    /// </summary>
    public virtual Character _Character { get { return character; } }

    /// <summary>
    /// Portrait that represents speaking character.
    /// </summary>
    public virtual Sprite Portrait { get { return portrait; } set { portrait = value; } }

    /// <summary>
    /// Type this text in the previous dialog box.
    /// </summary>
    public virtual bool ExtendPrevious { get { return extendPrevious; } }

    public override void OnEnter()
    {
        if (!showAlways && executionCount >= showCount)
        {
            Continue();
            return;
        }

        executionCount++;

        // Override the active say dialog if needed
        if (character != null && character.SetSayDialog != null)
        {
            SayDialog.ActiveSayDialog = character.SetSayDialog;
        }

        if (setSayDialog != null)
        {
            SayDialog.ActiveSayDialog = setSayDialog;
        }

        var sayDialog = SayDialog.GetSayDialog();
        if (sayDialog == null)
        {
            Continue();
            return;
        }

        var flowchart = GetFlowchart();

        sayDialog.SetActive(true);

        sayDialog.SetCharacter(character);
        sayDialog.SetCharacterImage(portrait);

        string displayText = storyText;

        var activeCustomTags = CustomTag.activeCustomTags;
        for (int i = 0; i < activeCustomTags.Count; i++)
        {
            var ct = activeCustomTags[i];
            displayText = displayText.Replace(ct.TagStartSymbol, ct.ReplaceTagStartWith);
            if (ct.TagEndSymbol != "" && ct.ReplaceTagEndWith != "")
            {
                displayText = displayText.Replace(ct.TagEndSymbol, ct.ReplaceTagEndWith);
            }
        }

        string subbedText = flowchart.SubstituteVariables(displayText);

        sayDialog.Say(subbedText, !extendPrevious, waitForClick, fadeWhenDone, stopVoiceover, voiceOverClip, delegate
        {
            Continue();
        });
        if (character != null)
        {
            _animator.Value = character.gameObject.GetComponentInChildren<Animator>();
        }
        if (isFlipped)
        {
            character.gameObject.transform.localScale = new Vector3(-(character.gameObject.transform.localScale.x), character.gameObject.transform.localScale.y, character.gameObject.transform.localScale.z);
        }

        if (_animator.Value != null)
        {
            //	if (useMultipleAnimation) {
            GetPrimaryAnimFunction();
            Debug.Log("PRIMARY ANIMATION" + _parameterNamePrimary.Value + GameConstants.currentStoryNumber);
            if (GameConstants.currentStoryNumber == 1 || GameConstants.currentStoryNumber == 2)
            {
                _animator.Value.SetTrigger(_parameterNamePrimary.Value);
            }
            else
            {
                _animator.Value.Play(_parameterNamePrimary.Value, -1, 0);
            }
            //	}
            GetSecondaryAnimFunction();
            Debug.Log("SECONDARY ANIMATION" + _parameterNameSecondary.Value);
            if (GameConstants.currentStoryNumber == 1 || GameConstants.currentStoryNumber == 2)
            {
                _animator.Value.SetTrigger(_parameterNameSecondary.Value);
            }
            else
            {
                _animator.Value.Play(_parameterNameSecondary.Value, -1, 0);
            }

        }
    }

    public override string GetSummary()
    {
        string namePrefix = "";
        if (character != null)
        {
            namePrefix = character.NameText + ": ";
            if (character.NameText == "")
            {
                namePrefix = character.name + ": ";
            }
        }
        if (extendPrevious)
        {
            namePrefix = "EXTEND" + ": ";
        }
        return namePrefix + primaryAnimation.ToString() + "-" + secondaryAnimation.ToString() + " \"" + storyText + "\"";
    }

    public override Color GetButtonColor()
    {
        return new Color32(184, 210, 235, 255);
    }

    public override void OnReset()
    {
        executionCount = 0;
    }

    public override void OnStopExecuting()
    {
        var sayDialog = SayDialog.GetSayDialog();
        if (sayDialog == null)
        {
            return;
        }

        sayDialog.Stop();
    }

    #endregion

    #region ILocalizable implementation

    public virtual string GetStandardText()
    {
        return storyText;
    }

    public virtual void SetStandardText(string standardText)
    {
        storyText = standardText;
    }

    public virtual string GetDescription()
    {
        return description;
    }

    public virtual string GetStringId()
    {
        // String id for Say commands is SAY.<Localization Id>.<Command id>.[Character Name]
        string stringId = "SAY." + GetFlowchartLocalizationId() + "." + itemId + ".";

            // if (character != null)
            // {
            //     stringId += character.NameText;
            // }
        

        return stringId;
    }

    #endregion

}