# UI commands # {#ui_commands}

[TOC]
# Fade UI # {#FadeUI}
Fades a UI object

Defined in Fungus.FadeUI

Property | Type | Description
 --- | --- | ---
Target Objects | System.Collections.Generic.List`1[UnityEngine.GameObject] | 
Tween Type | LeanTweenType | 
Wait Until Finished | Fungus.BooleanData | 
Duration | Fungus.FloatData | 

# Get Text # {#GetText}
Gets the text property from a UI Text object and stores it in a string variable.

Defined in Fungus.GetText

Property | Type | Description
 --- | --- | ---
Target Text Object | UnityEngine.GameObject | 
String Variable | Fungus.StringVariable | 

# Get Toggle State # {#GetToggleState}
Gets the state of a toggle UI object and stores it in a boolean variable.

Defined in Fungus.GetToggleState

Property | Type | Description
 --- | --- | ---
Toggle | UnityEngine.UI.Toggle | 
Toggle State | Fungus.BooleanVariable | 

# Set Interactable # {#SetInteractable}
Set the interactable sate of selectable objects.

Defined in Fungus.SetInteractable

Property | Type | Description
 --- | --- | ---
Target Objects | System.Collections.Generic.List`1[UnityEngine.GameObject] | 
Interactable State | Fungus.BooleanData | 

# Set Slider Value # {#SetSliderValue}
Sets the value property of a slider object

Defined in Fungus.SetSliderValue

Property | Type | Description
 --- | --- | ---
Slider | UnityEngine.UI.Slider | 
Value | Fungus.FloatData | 

# Set Text # {#SetText}
Sets the text property on a UI Text object and/or an Input Field object.

Defined in Fungus.SetText

Property | Type | Description
 --- | --- | ---
Target Text Object | UnityEngine.GameObject | 
Text | Fungus.StringDataMulti | 
Description | System.String | 

# Set Toggle State # {#SetToggleState}
Sets the state of a toggle UI object

Defined in Fungus.SetToggleState

Property | Type | Description
 --- | --- | ---
Toggle | UnityEngine.UI.Toggle | 
Value | Fungus.BooleanData | 

# Write # {#Write}
Writes content to a UI Text or Text Mesh object.

Defined in Fungus.Write

Property | Type | Description
 --- | --- | ---
Text Object | UnityEngine.GameObject | 
Text | Fungus.StringDataMulti | 
Description | System.String | 
Clear Text | System.Boolean | 
Wait Until Finished | System.Boolean | 
Text Color | Fungus.TextColor | 
Set Alpha | Fungus.FloatData | 
Set Color | Fungus.ColorData | 

