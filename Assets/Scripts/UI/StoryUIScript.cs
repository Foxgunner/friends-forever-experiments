﻿using UnityEngine;
using System.Collections;
using Fungus;

public class StoryUIScript : MonoBehaviour
{

	public GameObject outofCoinsPopup;

	public GameObject rewardPopup;

	static StoryUIScript _storyuiScript;

	public Flowchart[] flowchart;

	public static StoryUIScript storyuiscript {
		get { return _storyuiScript; }
	}


	void OnEnable ()
	{
		//GameConstants.currentStoryNumber = 4;
		//Messenger.AddListener<string>(MessengerConstants.BACK_FROM_STORE, checkPopupStatus);
		//Messenger.MarkAsPermanent(MessengerConstants.BACK_FROM_STORE);
	}


	void OnDisable ()
	{
		//Messenger.AddListener<string>(MessengerConstants.BACK_FROM_STORE, checkPopupStatus);
	}


	void Start ()
	{
		_storyuiScript = this;
	}

	//Gets called in the flowchart using the Invoke Method command.
	public void showOutofCoinsPopup ()
	{
		StoryScript.storyScript.showPause ("Pause");
		outofCoinsPopup.SetActive (true);
		//G2WGAHelper.LogEvent("Out of Coins Popup - Shown",GameConstants.getStoryName()+"- Chapter ",GameConstants.currentChapterNumber.ToString(),1);
	}


	public void closeCoinsPopup ()
	{
		G2WGAHelper.LogEvent ("Out of Coins Popup - Close");
		outofCoinsPopup.SetActive (false);
	}

	public void useCoinsForDialogue ()
	{
		StoreConstants.totalCoins -= DynamicConstants.CoinsRequiredForDialgoue;
		G2WGAHelper.LogEvent (GameConstants.getStoryName()+": Unlock Response "+GameConstants.currentChapterNumber.ToString(), "ResponseCost", DynamicConstants.CoinsRequiredForDialgoue.ToString (), 1);
		PlayerPrefsX.SetBool ("Story"+GameConstants.currentStoryNumber.ToString()+"Chapter"+GameConstants.currentChapterNumber.ToString()+"DialogueUnlock", true);
        UnlockAchievements();
    }

	public void watchAVideoCoinsPopup ()
	{
		//if (GameConstants.isNetConnected ()) {
			G2WAdHelper.ShowRewardedVideo (GameConstants.RVP_InGameCoins,"Story");
				GameServicesHelper.UnlockAchievements(AchievementConstants.AVID_OBSERVER);
		//}
	}

    public void UnlockAchievements()
    {
        if (GameConstants.currentStoryNumber == 2)
        {
            GameServicesHelper.UnlockAchievements(AchievementConstants.INQUISITIVE_READER);
        }
		else if (GameConstants.currentStoryNumber == 3)
        { 
			GameServicesHelper.UnlockAchievements(AchievementConstants.DETAIL_ORIENTED);
		}

		else if (GameConstants.currentStoryNumber == 5)
        { 
			if(GameConstants.currentChapterNumber == 12)
			{
				GameServicesHelper.UnlockAchievements(AchievementConstants.BACKSTORY_PRO);
			}
			else if(GameConstants.currentChapterNumber == 27)
			{
				GameServicesHelper.UnlockAchievements(AchievementConstants.RECALL_EXPERT);
			}
			
		}
		else if (GameConstants.currentStoryNumber == 6)
        { 
			if(GameConstants.currentChapterNumber == 1)
			{
				GameServicesHelper.UnlockAchievements(AchievementConstants.A_BLAST_FROM_THE_PAST);
			}
			else if(GameConstants.currentChapterNumber == 21)
			{
				GameServicesHelper.UnlockAchievements(AchievementConstants.MARCUS_ANGELS);
			}
			
		}
    }

    public void branchAchievements(int value)
    {
		if(value == 1)
        PlayerPrefs.SetInt(GameConstants.currentStoryNumber.ToString ()+ GameConstants.currentChapterNumber.ToString() + value.ToString(), 1);
		else
		PlayerPrefs.SetInt(GameConstants.currentStoryNumber.ToString ()+ GameConstants.currentChapterNumber.ToString() + value.ToString(), 1);

        if (PlayerPrefs.GetInt(GameConstants.currentStoryNumber.ToString() + GameConstants.currentChapterNumber.ToString() + "1") == 1 && PlayerPrefs.GetInt(GameConstants.currentStoryNumber.ToString() + GameConstants.currentChapterNumber.ToString() + "2") == 1)
        { 
			  GameServicesHelper.UnlockAchievements(AchievementConstants.CURIOUS_READER);
		}
    }


    public void selectedBranch (int value)
	{
		Debug.Log ("SELECTED BRANCH" + value + " " + "Story"+GameConstants.currentStoryNumber.ToString()+"Chapter" + GameConstants.currentChapterNumber.ToString () + "storyBranch");
		G2WGAHelper.LogEvent ("Parallel Tracks",GameConstants.getStoryName() + ": " + GameConstants.currentChapterNumber.ToString(),"Option " + value.ToString(), 1);
		//G2WGAHelper.LogEvent (GameConstants.getStoryName()+": Unlock Response "+GameConstants.currentChapterNumber.ToString(), "ResponseCost", DynamicConstants.CoinsRequiredForDialgoue.ToString (), 1);
		PlayerPrefs.SetInt ("Story"+GameConstants.currentStoryNumber.ToString()+"Chapter" + GameConstants.currentChapterNumber.ToString () + "storyBranch", value);
        branchAchievements(value);
    }

	public void checkWhichBranchToTake(int level)
	{
		flowchart = GameObject.FindObjectsOfType<Flowchart> ();
		
		foreach (Flowchart f in flowchart) {
			f.SetIntegerVariable ("selectedStoryBranch", PlayerPrefs.GetInt ("Story"+GameConstants.currentStoryNumber.ToString()+"Chapter" + level.ToString () + "storyBranch"));
		}
	}

	public void useCoinsForAccessory (string name)
	{
		StoreConstants.totalCoins -= DynamicConstants.CoinsRequiredForAccessory;
		 G2WGAHelper.LogEvent (GameConstants.getStoryName()+": Accessory "+GameConstants.currentChapterNumber.ToString(), "Accessory Cost", DynamicConstants.CoinsRequiredForAccessory.ToString (), 1);
		PlayerPrefsX.SetBool (name+"Unlocked", true);
		//GameServicesHelper.UnlockAchievements(AchievementConstants.SENSATIONAL_STYLE);
	}
	//Checks if user has already purchased the dialogue option
	public void checkIfAccessoryAlreadyUnlocked (string name)
	{ 

		flowchart = GameObject.FindObjectsOfType<Flowchart> ();

		foreach (Flowchart f in flowchart) {
			f.SetBooleanVariable ("isAccessoryUnlocked", PlayerPrefsX.GetBool (name+"Unlocked"));
		}
	}

	public void useAccessoryOrNot(bool wear, string name)
	{
		PlayerPrefsX.SetBool (name+"Wear", wear);
	}
		

}
