﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Prime31;
using UnityEngine.UI;

/// <summary>
/// Script for handling the functions in Pause Screen. It is attached to the PauseScript GameObject in individual Story Scenes.
/// </summary>
public class PauseScript : MonoBehaviour
{

	// Used for blocking the buttons in ChapterSelectionScreen when Store is added additively as a hack because buttons in the ChapterSelectionScreen are getting clicked in Store (Unity bug).
	public GameObject panel;


	public GameObject settingsObject;

	public Text iCloudMsg;


	
	void OnEnable ()
	{
		#if UNITY_EDITOR
		//StoreConstants.totalCoins = 0;
		#endif
		Messenger.AddListener<string> (MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY, closeSettings);
		Messenger.MarkAsPermanent (MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY);
	}

	void OnDisable ()
	{
		Messenger.RemoveListener<string> (MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY, closeSettings);
	}


	/// <summary>
	/// Replay the current chapter from start.
	/// </summary>
	public void replayChapter ()
	{
		Debug.Log("REPLAY CHAPTERRRR");
		G2WBackButtonHelper.isPauseOn = false;
		SceneManager.LoadScene (GameConstants.getSceneName ());
	}

	public void replayHiddenObject ()
	{
		G2WBackButtonHelper.isPauseOn = false;
		SceneManager.LoadScene ("HiddenObjectScene");
	}

	public void homeButton ()
	{
		G2WBackButtonHelper.isPauseOn = false;
       // G2WAdHelper.CheckForInterstitialSuperSonic();
        G2WAdHelper.CheckForInterstitial ();
        SceneManager.LoadScene ("ChapterSelectionScreen");
	//	SceneManager.LoadScene ("TempLevelSelection");
	}

	/// <summary>
	/// Loads the store.
	/// </summary>
	/// <param name="type">Coins or Moves</param>
	public void loadStore (string type)
	{
		G2WGAHelper.LogEvent ("Out of Coins Popup - Buy Moves");
		if (type == "coins") {
			StoreConstants.showCoinScreen = true;
		} else {
			StoreConstants.showCoinScreen = false;
		}
		//panel.SetActive (true);
		SceneManager.LoadScene ("Store", LoadSceneMode.Additive);

	}

	public void hidePanel (string s)
	{
		panel.SetActive (false);
	}

	public void openSettings ()
	{
		settingsObject.SetActive (true);
        G2WGAHelper.LogEvent("Open Settings");
#if UNITY_IOS
		if (iCloudBinding.getUbiquityIdentityToken () == null) {
			iCloudMsg.gameObject.SetActive (true);
		} else {
			iCloudMsg.gameObject.SetActive (false);
		}
#else
		if(iCloudMsg !=null)
        iCloudMsg.gameObject.SetActive (false);
		#endif
		G2WBackButtonHelper.isSettingsOn = true;
	}

	public void closeSettings (string s)
	{
		settingsObject.SetActive (false);
		G2WBackButtonHelper.isSettingsOn = false;
	}


}
