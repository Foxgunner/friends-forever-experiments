﻿using UnityEngine;
using System.Collections;

public class LoadCharacters : MonoBehaviour {

	public GameObject characterPrefab1;

	void Start()
	{
		Instantiate (characterPrefab1, Vector3.one, Quaternion.identity);
	}
}
