﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SimpleJSON;
using Fungus;
using System;

public class s : EditorWindow {



	private static Flowchart currentFlowChart;
	private static Block currentBlock;
	private static JSONNode jsonData;
	private static TextAsset textData;
	private static string storyNumber;
	private static Block nextBlock;
	private static int blockCount;
	private static int flowChartCount;
	private static int chapterCounter;
	private static int sceneCounter;
	private static Block block_a;
	private static Block block_b;
	private static int block_x;
	private static int block_y;
	private static GameObject levels;
	private static GameObject level;


	[MenuItem("G2W/ImportFlowChart")]
	static void Create()
	{

		blockCount = 0;
		flowChartCount=0;
		chapterCounter = 0;
		sceneCounter = 0;
		levels = new GameObject ("Levels");
		string path = EditorUtility.OpenFilePanel ("Please target the json file", "", "json");
		WWW www = new WWW ("file:///" + path);
		while (www.isDone == false){}
		if(path.Split("_".ToCharArray (0, 1)).Length>2||path.Split(".".ToCharArray (0, 1)).Length>2){
			Debug.Log ("The Path of the file contains multiple underscores/dots in it. Please change the file location or path name");
			return;
		}
		storyNumber = path.Split(".".ToCharArray (0, 1))[0].Split ("_".ToCharArray (0, 1)) [1];
		Debug.Log ("storyNumber > "+ storyNumber);
		jsonData = JSON.Parse(www.text.Replace(((char)173).ToString(), String.Empty));
		for (int i = 0; i < jsonData.Count; i++) {
			Debug.Log ("storytext > "+ jsonData [i].ToString());

			if (jsonData [i]["chapter"] != null) {
				Debug.Log ("new Chapater.....");
				addChapter (jsonData [i]["chapter"]);
			}else if (jsonData [i]["scene"] != null) {
				addScene (false,jsonData [i]["scene"]);

			}
			if(jsonData [i]["content"] ==null &&jsonData [i]["expressions"] ==null ){
				continue;
			}
			if (jsonData [i] ["shortContentForChoices"] != null) {

				string charString = jsonData [i] ["characterTalking"];
				string content =jsonData [i] ["content"];

				if(block_a!=null&&block_b!=null){
					endMenuBlocks ();
				}

				Fungus.Menu menu = addMenuCommand (currentBlock); 
				menu.text = jsonData [i] ["shortContentForChoices"];

				if (content.StartsWith("a#.")) {
					block_a = addBlock (currentFlowChart, false);
					menu.targetBlock = block_a;
				} else if(content.StartsWith("b#.")) {
					block_b = addBlock (currentFlowChart, false);
					menu.targetBlock = block_b;
				}
				
				if (charString!=null&&charString.ToLower().Contains ("text")) {
					if (block_b == null) {
						addSayCommand (jsonData [i], block_a);
					} else {
						addSayCommand (jsonData [i], block_b);
					}
				} else {
					if (block_b == null) {
						addSayAnimCommand (jsonData [i], block_a);
					} else {
						addSayAnimCommand (jsonData [i], block_b);
					}

				}
				if(nextBlock==null){
					
					nextBlock = addBlock (currentFlowChart,false);
					block_y += 100;
					block_x = 100;
				}


			} else {
				
				string charString = jsonData [i] ["characterTalking"];
				if (charString!=null&&(charString.ToLower().Contains("phone")||charString.ToLower().Contains("text"))) {
					addSayCommand (jsonData [i],currentBlock);
				} else {
					
					string storytext = jsonData [i] ["content"];

					if(storytext!=null&&storytext.StartsWith("a#.")&&block_a!=null){
						addSayAnimCommand (jsonData [i],block_a);
					}else if(storytext!=null&&storytext.StartsWith("b#.")&&block_b!=null){
						addSayAnimCommand (jsonData [i],block_b);
					}else{
						if(nextBlock!=null){
							endMenuBlocks ();
						}
						addSayAnimCommand (jsonData [i],currentBlock);
					}

				}

			}
		}
	}
	private static void addChapter (string name){
		chapterCounter++;
		sceneCounter = 0;
		level = new GameObject ("Level"+chapterCounter);
		level.transform.parent = levels.transform;
		addScene (true,"scene1");
	}
	private static void addScene(bool isNewChapter,string name){
		if(block_a!=null&&block_b!=null){
			endMenuBlocks ();
		}

		if (isNewChapter) {
			if(currentBlock){
				addLoadSceneCommand (currentBlock,"EndScene");
			}

		} else {
			addSendMessageCommand (currentBlock,chapterCounter,sceneCounter+1);
		}
		sceneCounter++;
		block_x = 100;
		block_y=100;
		if (name.Contains ("Unlock")) {
			createFlowChart ("_coins");
		} else {
			createFlowChart ("");
		}


	}
	private static void addLoadSceneCommand(Block block,string sceneName){
		LoadScene loadCommand=block.gameObject.AddComponent<LoadScene> () as LoadScene;
		loadCommand.ParentBlock = block;
		loadCommand.ItemId = currentFlowChart.NextItemId();
		loadCommand.OnCommandAdded(block);
		block.CommandList.Add(loadCommand);
		loadCommand._sceneName=new StringData(sceneName);
	}
	private static void createFlowChart(string unlock){

		GameObject go = new GameObject ("L"+chapterCounter+"_S"+ sceneCounter.ToString("D2")+unlock);
		go.transform.parent = level.transform;
		currentFlowChart = go.AddComponent <Flowchart>() as Flowchart;;
		currentBlock = addBlock (currentFlowChart,true);
		block_y += 100;
		block_x = 100;
		flowChartCount++;

	}
	private static void addSendMessageCommand(Block block,int chapter,int scene)
	{
		SendMessage sayCommand=block.gameObject.AddComponent<SendMessage> () as SendMessage;
		sayCommand.ParentBlock = block;
		sayCommand.ItemId = currentFlowChart.NextItemId();
		sayCommand.OnCommandAdded(block);
		block.CommandList.Add(sayCommand);
		sayCommand._message=new StringData("L"+chapter+"_S"+ scene.ToString("D2"));

	}
	private static void endMenuBlocks()
	{
		Call call_a = addCall (block_a);
		call_a.targetBlock = nextBlock;

		Call call_b = addCall (block_b);
		call_b.targetBlock = nextBlock;

		currentBlock = nextBlock;
		nextBlock = null;
		block_a = null;
		block_b = null;
	}
	private static Call addCall(Block block){
		Call call = block.gameObject.AddComponent<Call> () as Call;
		call.ParentBlock = block;
		call.ItemId = currentFlowChart.NextItemId();
		call.OnCommandAdded(block);
		block.CommandList.Add(call);
		return call;

	}

	private static Fungus.Menu addMenuCommand(Block block)
	{
		
		Fungus.Menu menu = block.gameObject.AddComponent<Fungus.Menu> () as Fungus.Menu;
		menu.ParentBlock = block;
		menu.ItemId = currentFlowChart.NextItemId();
		menu.OnCommandAdded(block);
		block.CommandList.Add(menu);
		return menu;

	}


	private static Block addBlock(Flowchart fc,bool onEnable){
		Block block = fc.CreateBlock (new Vector2Data(new Vector2(block_x,block_y)));
		block_x += 150;
		if (onEnable) {
			if (sceneCounter == 1) {
				FlowchartEnabled newHandler = block.gameObject.AddComponent<FlowchartEnabled> () as FlowchartEnabled;
				newHandler.ParentBlock = block;
				block._EventHandler = newHandler;
			} else {
				MessageReceived newHandler = block.gameObject.AddComponent<MessageReceived> () as MessageReceived;
				newHandler.ParentBlock = block;
				block._EventHandler = newHandler;
				newHandler.message = "L" + chapterCounter + "_S" + (sceneCounter).ToString ("D2");
			}

		}

		blockCount++;
		return block;
	}
	private static void addSayAnimCommand(JSONNode commandData,Block block){

		if(commandData ["expressions"]!=null){
			addExpressionsToChars (commandData ["expressions"],block);
		}

		SayAnimation sayCommand=block.gameObject.AddComponent<SayAnimation> () as SayAnimation;
		sayCommand.ParentBlock = block;
		sayCommand.ItemId = currentFlowChart.NextItemId();
		sayCommand.OnCommandAdded(block);
		block.CommandList.Add(sayCommand);


		string storyText = commandData ["content"];	
		if(storyText!=null){
			storyText = storyText.Replace("a#.","");
			storyText = storyText.Replace("b#.","");
			sayCommand.storyText = storyText;
		}


		if(commandData ["characterTalking"]!=null){
			addMainChar (commandData ["characterTalking"], sayCommand, storyText);
		}




	}
	private static void addSayCommand(JSONNode commandData,Block block){
		Say sayCommand=block.gameObject.AddComponent<Say> () as Say;
		sayCommand.ParentBlock = block;
		sayCommand.ItemId = currentFlowChart.NextItemId();
		sayCommand.OnCommandAdded(block);
		block.CommandList.Add(sayCommand);

		string txt = commandData ["content"];
		txt = txt.Replace("a#.","") ;
		txt = txt.Replace("b#.","") ;
		sayCommand.storyText =txt;
		string charString = commandData ["characterTalking"];
		if(charString.Contains("Text message")){
			
			string[] charTxt=charString.Split(" ".ToCharArray(0,1));
			if(charTxt[0]=="You"){
				sayCommand.character = GameObject.Find ("You_S"+storyNumber+"_proxy" ).GetComponent<ProxyCharacter>();
			}else{
				sayCommand.character = GameObject.Find (charTxt[0]+"_proxy" ).GetComponent<ProxyCharacter>();
			}
		}
	}
	private static void addMainChar(string charString,SayAnimation sayCommand,string storyText){
		charString = charString.Replace (" ", "");
		//Debug.Log ("charString "+ charString);
		string[] charTxt = charString.Split("-".ToCharArray(0,1));
		

		//Debug.Log("::"+charTxt[0]+"_proxy"+"::");
		if(charTxt[0]=="You"){
			sayCommand.character = GameObject.Find ("You_S"+storyNumber+"_proxy" ).GetComponent<ProxyCharacter>();
		}else{
			sayCommand.character = GameObject.Find (charTxt[0]+"_proxy" ).GetComponent<ProxyCharacter>();
		}
		if(charTxt.Length==1){
			setTalkingLength(sayCommand,storyText);
			sayCommand.secondaryAnimation = SayAnimation.SecondaryAnimTypes.Idle1;
		}
		if(charTxt.Length>1){
			string[] extTxt=charTxt [1].Split ("+".ToCharArray(0,1));
			
			if(extTxt.Length==1){
				if(extTxt[0]=="Talking"){
					setTalkingLength(sayCommand,storyText);
					sayCommand.secondaryAnimation = SayAnimation.SecondaryAnimTypes.Idle1;
				}else if(extTxt[0]=="Thinking"){
					sayCommand.primaryAnimation = SayAnimation.PrimaryAnimTypes.MouthNone;
					sayCommand.secondaryAnimation = SayAnimation.SecondaryAnimTypes.Idle1;
				}else{
					setTalkingLength(sayCommand,storyText);
					SayAnimation.SecondaryAnimTypes sayAnimType = (SayAnimation.SecondaryAnimTypes) Enum.Parse(typeof(SayAnimation.SecondaryAnimTypes), extTxt[0]); 
				}

			}else{
				setTalkingLength(sayCommand,storyText);
				SayAnimation.SecondaryAnimTypes sayAnimType = (SayAnimation.SecondaryAnimTypes) Enum.Parse(typeof(SayAnimation.SecondaryAnimTypes), extTxt[1]); 
			}
			

		}
		
	}
	private static void setTalkingLength(SayAnimation sayCommand,string storyText){
		if(storyText==null||storyText.Length<=0){
		sayCommand.primaryAnimation = SayAnimation.PrimaryAnimTypes.MouthNone;

		}else{
			if(storyText.Split(" ".ToCharArray(0,1)).Length>8){
				sayCommand.primaryAnimation = SayAnimation.PrimaryAnimTypes.MouthSpeaking;
			}else if(storyText.Split(" ".ToCharArray(0,1)).Length>4){
				sayCommand.primaryAnimation = SayAnimation.PrimaryAnimTypes.MouthSpeakingShort;
			}else{
				sayCommand.primaryAnimation = SayAnimation.PrimaryAnimTypes.MouthExtraShortSpeaking;
			}
		}
		
	}
	private static void addExpressionsToChars(string expString, Block block){
		expString = expString.Replace (" ", "");
		expString = expString.Replace ("--", "-");
		expString = expString.Replace((char)173, '\0');
		string[] expressions = expString.Split (Environment.NewLine.ToCharArray());
		for (int a = 0; a < expressions.Length; a++) {
			SetAnimationCommand setAnimCommand=block.gameObject.AddComponent<SetAnimationCommand> () as SetAnimationCommand;
			setAnimCommand.ParentBlock = block;
			setAnimCommand.ItemId = currentFlowChart.NextItemId();
			setAnimCommand.OnCommandAdded(block);
			block.CommandList.Add(setAnimCommand);
			setAnimCommand._duration = new FloatData(0.5f);

			string[] expression = expressions [a].Split ("-".ToCharArray(0,1));
			//Debug.Log(">"+expression.ToString());
			if(expression[0]=="You"){
				setAnimCommand.character = GameObject.Find ("You_S"+storyNumber+"_proxy" ).GetComponent<ProxyCharacter>();
			}else{
				setAnimCommand.character = GameObject.Find (expression[0]+"_proxy" ).GetComponent<ProxyCharacter>();
			}
			if(expression.Length>1){
				if (expression [1].IndexOf ("+") >= 0) {
					string[] multiExpression = expression [1].Split ("+".ToCharArray (0, 1));

					setAnimCommand.useMultipleAnimation = false;
					SetAnimationCommand.SecondaryAnimTypes setAnimType = (SetAnimationCommand.SecondaryAnimTypes) Enum.Parse(typeof(SetAnimationCommand.SecondaryAnimTypes), multiExpression[0]);  
					setAnimCommand.secondaryAnimation = setAnimType;

					/*SetAnimationCommand.PrimaryAnimTypes multiSetAnimType_1 = (SetAnimationCommand.PrimaryAnimTypes) Enum.Parse(typeof(SetAnimationCommand.PrimaryAnimTypes), multiExpression[0]);  
					setAnimCommand.primaryAnimation = multiSetAnimType_1;
					SetAnimationCommand.SecondaryAnimTypes multiSetAnimType_2 = (SetAnimationCommand.SecondaryAnimTypes) Enum.Parse(typeof(SetAnimationCommand.SecondaryAnimTypes), multiExpression[1]);  
					setAnimCommand.secondaryAnimation = multiSetAnimType_2;*/

				} else {

					setAnimCommand.useMultipleAnimation = false;
					SetAnimationCommand.SecondaryAnimTypes setAnimType = (SetAnimationCommand.SecondaryAnimTypes) Enum.Parse(typeof(SetAnimationCommand.SecondaryAnimTypes), expression[1]);  
					setAnimCommand.secondaryAnimation = setAnimType;
				}

			}
			
		}
	}
}
