﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G2WMusicManager : MonoBehaviour {

    private AudioSource musicManager;

    public AudioClip[] audioclips;
    // Use this for initialization
    void Start () {

        musicManager = this.GetComponent<AudioSource>();

        if (GameConstants.currentChapterNumber % 2 == 0)
        {
            musicManager.clip = audioclips[0];
        }
		else
		{
			 musicManager.clip = audioclips[1];
		}

		musicManager.Play();

    }
	

}
