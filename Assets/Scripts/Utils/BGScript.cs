﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Reflection;

public class BGScript : MonoBehaviour
{

	public string bgName = "";



    Sprite GetSpriteFromResources (string name)
	{
		//Texture2D tempTex = Resources.Load(name) as Texture2D;

		TextAsset tmp = Resources.Load ("House", typeof(TextAsset)) as TextAsset;

		Texture2D tempTex = new Texture2D (1, 1, TextureFormat.ARGB32, false);

		tempTex.LoadImage (tmp.bytes);

		Sprite tempAvatar = Sprite.Create (tempTex, new Rect (0, 0, tempTex.width, tempTex.height), new Vector2 (tempTex.width / 2, tempTex.height / 2), 100f);

		Debug.Log ("SPRITE" + tempAvatar.name);

		return tempAvatar;
	}

	Texture2D GetTextureFromResources (string name)
	{
		//Texture2D tempTex = Resources.Load(name) as Texture2D;

		TextAsset tmp = Resources.Load (name, typeof(TextAsset)) as TextAsset;

		Texture2D tempTex = new Texture2D (1, 1, TextureFormat.ARGB32, false);

		tempTex.LoadImage (tmp.bytes);


		return tempTex;
	}

	Sprite GetSpriteFromResourcesNew (string name)
	{
		//Texture2D tempTex = Resources.Load(name) as Texture2D;

		TextAsset tmp = Resources.Load (name, typeof(TextAsset)) as TextAsset;

		if (tmp == null) {
			return Resources.Load<Sprite> (name);
			
		} else {
			string imageStr = Convert.ToBase64String (tmp.bytes); 
			//PlayerPrefs.SetString("SecondaryImage_ByteString", imageStr);

			Texture2D tempTex = new Texture2D (1, 1, TextureFormat.ARGB32, false);

			tempTex.LoadImage (Convert.FromBase64String (imageStr));

			Sprite tempAvatar = Sprite.Create (tempTex, new Rect (0, 0, tempTex.width, tempTex.height), new Vector2 (.5f, .5f));

			//tempTex.LoadImage(tmp.bytes);

			//Sprite tempAvatar = Sprite.Create( tempTex, new Rect(0, 0, tempTex.width, tempTex.height), new Vector2(tempTex.width/2, tempTex.height/2), 100f);


			return tempAvatar;
		}
	}

	void OnEnable ()
	{
		//this.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(bgName);

		if (bgName != "") {
            // this.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(bgName);
            // this.GetComponent<SpriteRenderer>().sprite = GetSpriteFromResources(bgName);
            //this.GetComponent<SpriteRenderer>().material.mainTexture = GetTextureFromResources(bgName);
           // bgName = this.GetComponent<SpriteRenderer>().sprite.name;
             this.GetComponent<SpriteRenderer> ().sprite = GetSpriteFromResourcesNew (bgName);


        }
	}

	public void copyName()
	{
		bgName = this.GetComponent<SpriteRenderer>().sprite.name;
	}

	public void loadBG()
	{
		 this.GetComponent<SpriteRenderer> ().sprite = GetSpriteFromResourcesNew (bgName);
	}

	
}
