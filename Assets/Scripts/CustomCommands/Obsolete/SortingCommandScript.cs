﻿using UnityEngine;
using System.Collections;
using Fungus;
using Unity.Linq;

[CommandInfo("Obsolete",
			 "Sort Layer",
	"Sorts the image layers")]

public class SortingCommandScript : Command {


	public 	GameObject itemObject;
	public string layername;
	public override void OnEnter ()
	{
		//base.OnEnter ();
		foreach (var item in itemObject.Descendants())
		{
			if (item.GetComponent<SpriteRenderer> () != null) {
				item.GetComponent<SpriteRenderer> ().sortingLayerName = layername;
			}
		}
		Continue ();
	}

	public override string GetSummary()
	{
		return itemObject.name + "(" + layername + ")";
	}
}
