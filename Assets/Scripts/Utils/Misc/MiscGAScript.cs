﻿using UnityEngine;
using System.Collections;

public class MiscGAScript : MonoBehaviour
{


    // Sends the total number of gameplay
    public static void GAforGameplay()
    {
        if (GameConstants.totalGameplays == 10)
        {
            G2WGAHelper.LogEvent("Gameplays", "10", G2WAdHelper.AD_ID, 1);
            G2WGAHelper.LogRichEvent("Gameplays 10");
        }
        else if (GameConstants.totalGameplays == 20)
        {
            G2WGAHelper.LogEvent("Gameplays", "20", G2WAdHelper.AD_ID, 1);
             G2WGAHelper.LogRichEvent("Gameplays 20");
        }
        else if (GameConstants.totalGameplays == 30)
        {
            G2WGAHelper.LogEvent("Gameplays", "30", G2WAdHelper.AD_ID, 1);
             G2WGAHelper.LogRichEvent("Gameplays 30");
        }
        else if (GameConstants.totalGameplays == 40)
        {
            G2WGAHelper.LogEvent("Gameplays", "40", G2WAdHelper.AD_ID, 1);
             G2WGAHelper.LogRichEvent("Gameplays 40");
        }
        else if (GameConstants.totalGameplays == 50)
        {
            G2WGAHelper.LogEvent("Gameplays", "50", G2WAdHelper.AD_ID, 1);
             G2WGAHelper.LogRichEvent("Gameplays 50");
        }
        else if (GameConstants.totalGameplays == 60)
        {
            G2WGAHelper.LogEvent("Gameplays", "60", G2WAdHelper.AD_ID, 1);
             G2WGAHelper.LogRichEvent("Gameplays 60");
        }
        else if (GameConstants.totalGameplays > 60)
        {
            if (GameConstants.totalGameplays % 10 == 0)
            {
                G2WGAHelper.LogEvent("Gameplays", "61+", G2WAdHelper.AD_ID, 1);
                 G2WGAHelper.LogRichEvent("Gameplays 61+");
            }
        }


    }

    public static void chapterEndEvents()
    {
            G2WGAHelper.LogEvent("End Screen", "Seen");
            G2WGAHelper.LogRichEvent("End Screen");
            G2WGAHelper.LogEvent("End Screen - " + GameConstants.getStoryName(), "Seen");
            G2WGAHelper.LogEvent(GameConstants.getStoryName() + ": " + "End Chapter " + GameConstants.currentChapterNumber.ToString(), "Coins Balance", StoreConstants.totalCoins.ToString(), 1);
            G2WGAHelper.LogEvent(GameConstants.getStoryName() + ": " + "End Chapter " + GameConstants.currentChapterNumber.ToString(), "Moves Balance", StoreConstants.totalMoves.ToString(), 1);
            if(GameConstants.currentChapterNumber == 1 || (GameConstants.currentChapterNumber % 5 == 0))
				{
                    G2WGAHelper.LogRichEvent("Story "+GameConstants.currentStoryNumber+" - End Chapter "+GameConstants.currentChapterNumber.ToString());
                }
                StoryScript.storyScript.levelOverTime = System.DateTime.Now;
        long levelTimeMilliseconds = (long)(StoryScript.storyScript.levelOverTime - StoryScript.storyScript.levelStartTime).TotalMilliseconds;
G2WGAHelper.LogTiming (GameConstants.getStoryName() + ": End Chapter " + GameConstants.currentChapterNumber, levelTimeMilliseconds, "Time", "1");
    }

    public static void hiddenObjectEndEvents()
    {
                G2WGAHelper.LogEvent(GameConstants.getStoryName() + ": " + "End Chapter " + GameConstants.currentChapterNumber.ToString(), "Hints Used", GameConstants.hintsUsedInLevel.ToString(), 1);
                G2WGAHelper.LogEvent(GameConstants.getStoryName() + ": " + "End Chapter " + GameConstants.currentChapterNumber.ToString(), "Hints Cost", GameConstants.coinsUsedForHints.ToString(), 1);
                G2WGAHelper.LogEvent(GameConstants.getStoryName() + ": " + "End Chapter " + GameConstants.currentChapterNumber.ToString(), "Time Increased", GameConstants.timerUsedInLevel.ToString(), 1);
                G2WGAHelper.LogEvent(GameConstants.getStoryName() + ": " + "End Chapter " + GameConstants.currentChapterNumber.ToString(), "Time Cost", GameConstants.coinsUsedForTimer.ToString(), 1);
    }
}
