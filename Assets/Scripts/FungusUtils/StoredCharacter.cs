﻿using UnityEngine;
using System.Collections;
using Fungus;
using System.Linq;

public class StoredCharacter : Fungus.Character {

	public string Identifier;
	protected override void OnEnable()
	{
		base.OnEnable();
		if (string.IsNullOrEmpty(Identifier))
		{
			return;
		}
		foreach (ProxyCharacter result in activeCharacters.OfType<ProxyCharacter>())
		{
			if (result.ProxiedIdentifier == Identifier)
			{
				result.Original = this;
			}
		}
	}
}
