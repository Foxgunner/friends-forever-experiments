﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Text;
using Prime31;

public class FeedbackScript : MonoBehaviour
{

	public static FeedbackScript feedbackScript;

	private string uploadurl = "https://s02.appucino.com/feedback/submit";
	
	public InputField feedbackText, emailText;

	public Button submitButton;

	private bool hasOneKey = false;

	public Dropdown age;
	public Dropdown gender;

	public GameObject feedbackPanel;

	public GameObject feedbackPopup;

    public GameObject feedbackParticleEffect;

    void OnEnable ()
	{
		Messenger.AddListener<string> (MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY, HideFeedBackPanel);
		Messenger.MarkAsPermanent (MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY);
	}

	void OnDisable ()
	{
		Messenger.RemoveListener<string> (MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY, HideFeedBackPanel);
	}

	void Awake ()
	{
		feedbackScript = this;
		print ("good here");
	}

	void Start ()
	{
		submitButton.interactable = false;
		//FlurryHelper.LogEvent( "Open Feedback" );
		
		print ("age " + age.value);
		
		gender.ClearOptions ();

		//submitButton.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (96, -211);

		//transform.FindChild ("Later Button").GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-105, -211);
		
		List<Dropdown.OptionData> tempGender = new List<Dropdown.OptionData> ();
		
		Dropdown.OptionData boy = new Dropdown.OptionData ();
		boy.text = "Boy";
		
		Dropdown.OptionData girl = new Dropdown.OptionData ();
		girl.text = "Girl";
		
		Dropdown.OptionData choose = new Dropdown.OptionData ();
		choose.text = "CHOOSE";
		
		gender.transform.GetChild (0).GetComponent<Text> ().text = "I am a";
		
		tempGender.Add (choose);
		tempGender.Add (girl);
		tempGender.Add (boy);
		
		gender.value = 0;
		gender.AddOptions (tempGender);
		//gender.Select(0);
		gender.RefreshShownValue ();
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		age.ClearOptions ();
		
		List<Dropdown.OptionData> tempAge = new List<Dropdown.OptionData> ();
		
		Dropdown.OptionData below13 = new Dropdown.OptionData ();
		below13.text = "Below 13";
		
		Dropdown.OptionData b13to16 = new Dropdown.OptionData ();
		b13to16.text = "13 - 16";
		
		Dropdown.OptionData b17to20 = new Dropdown.OptionData ();
		b17to20.text = "17 - 20";
		
		Dropdown.OptionData above21 = new Dropdown.OptionData ();
		above21.text = "Above 21";
		
		age.transform.GetChild (0).GetComponent<Text> ().text = "My age group is";
		
		tempAge.Add (choose);
		tempAge.Add (below13);
		tempAge.Add (b13to16);
		tempAge.Add (b17to20);
		tempAge.Add (above21);
		
		age.value = 0;
		age.AddOptions (tempAge);
		//gender.Select(0);
		age.RefreshShownValue ();
	}



	public void feedback ()
	{
		//FlurryHelper.LogEvent ("Feedback");
		if (Application.internetReachability != NetworkReachability.NotReachable) {
			#if UNITY_IOS //pragma
			sendfeedback (feedbackText.text, "friends_forever_ios");
			#elif UNITY_ANDROID
			sendfeedback (feedbackText.text, "friends_forever_android");
			#endif
			
		} //else
		//GameConstants.NoNetworkAvailable ();
		//FlurryHelper.LogEvent ("Feedback - Submit");
		G2WBackButtonHelper.isFeedBackOn = false;
		feedbackPanel.SetActive (false);
		//UIManager.ui.HideFeedbackPanel ();

		age.value = 0;
		gender.value = 0;
		feedbackPopup.SetActive (true);
		//var buttons = new string[] { "OK" };

		#if UNITY_IOS //pragma
		//EtceteraBinding.showAlertWithTitleMessageAndButtons ("SUCCESS!!", "YOUR FEEDBACK IS SUCCESSFULLY SUBMITTED.", buttons);
		#elif UNITY_ANDROID
		//EtceteraAndroid.showAlert("SUCCESS!!", "YOUR FEEDBACK IS SUCCESSFULLY SUBMITTED.", "OK");
		#endif
	}

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.A))
			print (GetAge ());

		if (Input.GetKeyDown (KeyCode.F))
			print (GetGender ());

		hasOneKey = false;
		char[] array = feedbackText.text.ToCharArray ();
		if (feedbackText.text != null) {
			submitButton.interactable = true;
			
			// Loop through array.
			for (int i = 0; i < array.Length; i++) {
				// Get character from array.
				char letter = array [i];

				if (letter != ' ') {
					//submitButton.interactable = false;
					hasOneKey = true;
				}
			}
		} else {
			submitButton.interactable = true;
		}

		if (hasOneKey)
			submitButton.interactable = true;
		else
			submitButton.interactable = false;
	
	}

	string GetAge ()
	{
		if (age.value == 1) {
			#if UNITY_IOS //pragma
			FlurryAnalytics.setAge (13);
			#endif
			return "Below13";
		} else if (age.value == 2) {
			#if UNITY_IOS //pragma
			FlurryAnalytics.setAge (16);
			#endif
			return "13-16";
		} else if (age.value == 3) {
			#if UNITY_IOS //pragma
			FlurryAnalytics.setAge (20);
			#endif
			return "17-20";
		} else if (age.value == 4) {
			#if UNITY_IOS //pragma
			FlurryAnalytics.setAge (21);
			#endif
			return "Above21";
		} else
			return "NONE";
	}

	string GetGender ()
	{
		if (gender.value == 1) {
			#if UNITY_IOS //pragma
			FlurryAnalytics.setGender ("F");
			#endif
			return "F";
		} else if (gender.value == 2) {
			#if UNITY_IOS //pragma
			FlurryAnalytics.setGender ("M");
			#endif
			return "M";
			
		} else {
			return("X");
		}
	}

	
	public void sendfeedback (/*string emailid ,*/  string feedback, string gameid)
	{
		WWWForm _form = new WWWForm ();
		_form.AddField ("game_id", gameid);
		_form.AddField ("feedback", feedback);
		_form.AddField ("age_group", GetAge ());
		_form.AddField ("gender", GetGender ());
		_form.AddField ("v", "1.6");
		_form.AddField ("device", SystemInfo.deviceModel);
		//_form.AddField ("email_id",emailid);
		WWW w = new WWW (uploadurl, _form);
		StartCoroutine (startupdating (w));
	}

	
	IEnumerator startupdating (WWW www)
	{
		yield return www;
		
		// check for errors
		if (www.error == null) {
			Debug.Log ("WWW Ok!: " + www.data);
		} else {
			Debug.Log ("WWW Error: " + www.error);
		}    
		G2WBackButtonHelper.isFeedBackOn = false;
		feedbackPanel.SetActive (false);
		feedbackText.text = "";
	}

	public void HideFeedBackPanel (string s = "")
	{
		//FlurryHelper.LogEvent( "Feedback - Later" );
		//if (AdsCustom.ads != null)
		//	AdsCustom.ads.ShowBanner2 ();
		if (s == "Feedback") {
			G2WBackButtonHelper.isFeedBackOn = false;
			feedbackText.text = "";
			age.value = 0;
			gender.value = 0;
			feedbackPanel.SetActive (false);
            feedbackParticleEffect.SetActive(true);
        }
	}
}
