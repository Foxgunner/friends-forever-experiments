﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class TempScript : MonoBehaviour {

	public void loadaskedLevel(int level)
	{
		GameConstants.currentStoryNumber = 5;
		  StoreConstants.totalCoins = 1500;
		 GameConstants.currentChapterNumber = level;
		 Debug.Log("CURRENT SCENE NAME" + GameConstants.getSceneName() + "  " +  GameConstants.currentStoryNumber);
		SceneManager.LoadScene(GameConstants.getSceneName());
	}

    public void loadhiddenObjectLevel(int level)
    {
        StoreConstants.totalCoins = 500;
        GameConstants.currentGameState = GameStates.HIDDEN_OBJECT_MODE;
        GameConstants.currentHiddenObjectLevelNumber = level;
        SceneManager.LoadScene("HiddenObjectScene");
	}

}
