﻿using UnityEngine;
using System.Collections;
using Prime31;

public class G2WDynamic : MonoBehaviour
{
	
	private static G2WDynamic instance;


	public static WWW wwwUrl;
	public static bool bUrlLoad;

	public static WWW promoURL1;
	public static WWW promoURL2;
	public static WWW promoURL3;

	public static string promoU1;
	public static string promoU2;
	public static string promoU3;

	public static string clickURL1;
	public static string clickURL2;
	public static string clickURL3;

	public static string promoCampaign1;
	public static string promoCampaign2;
	public static string promoCampaign3;

	public static Texture promoTex1;
	public static Texture promoTex2;
	public static Texture promoTex3;



	void Awake ()
	{
		if (instance == null) {
			DontDestroyOnLoad (gameObject);
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}
	}

	// Use this for initialization
	void Start ()
	{
		StartCoroutine (LoadDynamicData ());
	}

	IEnumerator LoadDynamicData ()
	{
		while (!Caching.ready)
			yield return null;
		
		if (Application.internetReachability != NetworkReachability.NotReachable) {   // online
			var req = new WWW (GameConstants.dynamicDataLink);
			yield return req;

			if (req.error == null) {
				var reqData = (Hashtable)MiniJSON.jsonDecode (req.text);
				var result = reqData ["result"] as Hashtable;
				var promo = result ["ingame_promo"] as Hashtable;

				var promos = result ["more_games"] as Hashtable;
				var one = promos ["one"] as Hashtable;
				var two = promos ["two"] as Hashtable;
				var three = promos ["three"] as Hashtable;

				if (result != null) {
					var newFreq = GameConstants.InterstitialTimer;
					var newCoinPack_Free = StoreConstants.CoinPack_Free;
					var newCoinPack_1 = StoreConstants.CoinPack_1;
					var newCoinPack_2 = StoreConstants.CoinPack_2;
					var newCoinPack_3 = StoreConstants.CoinPack_3;
					var newMovePack_Free = StoreConstants.MovePack_Free;
					var newMovePack_1 = StoreConstants.MovePack_1;
					var newMovePack_2 = StoreConstants.MovePack_2;
					var newMovePack_3 = StoreConstants.MovePack_3;
					var newRewardedMoves = StoreConstants.Rewarded_Moves;
					var newRewardedCoins = StoreConstants.Rewarded_Coins;
					var newfirstTimeHintPrice = DynamicConstants.firstTimeHintPrice;
					var newhintAlreadyUsedPrice = DynamicConstants.hintAlreadyUsedPrice;
					var newlevelEndCoinsAwarded = DynamicConstants.levelEndCoinsAwarded;
					var newcoinsAwardedAtStart = DynamicConstants.CoinsAwardedAtStart;
					var newmovesAwardedAtStart = DynamicConstants.MovesAwardedAtStart;
					var newcoinsrequiredfordialogue = DynamicConstants.CoinsRequiredForDialgoue;
					var newcoinsrequiredforaccessory = DynamicConstants.CoinsRequiredForAccessory;
					var newcoinsforunlockingstory2 = DynamicConstants.CoinsForUnlockingStory2;
					var newmovesgivenforunlockingstory2 = DynamicConstants.MovesGivenForUnlockingStory2;
					var newcoinsforunlockingstory3 = DynamicConstants.CoinsForUnlockingStory3;
					var newmovesgivenforunlockingstory3 = DynamicConstants.MovesGivenForUnlockingStory3;
					var newcoinsforunlockingstory4 = DynamicConstants.CoinsForUnlockingStory4;
					var newmovesgivenforunlockingstory4 = DynamicConstants.MovesGivenForUnlockingStory4;
					var newcoinsforunlockingstory5 = DynamicConstants.CoinsForUnlockingStory5;
					var newmovesgivenforunlockingstory5 = DynamicConstants.MovesGivenForUnlockingStory5;
					var newcoinsforunlockingstory1 = DynamicConstants.CoinsForUnlockingStory1;
					var newmovesgivenforunlockingstory1 = DynamicConstants.MovesGivenForUnlockingStory1;
					var newcoinsforunlockingstory6 = DynamicConstants.CoinsForUnlockingStory6;
					var newmovesgivenforunlockingstory6 = DynamicConstants.MovesGivenForUnlockingStory6;
					var newfreemovestimer = DynamicConstants.freeMovesTime;
					var newtimermovesaward = DynamicConstants.timerMovesAward;
    

                    if (result ["interstitial_timer"] != null) {
						var isIntTimer = int.TryParse (result ["interstitial_timer"].ToString ().Trim (), out newFreq);

						if (isIntTimer) {
							GameConstants.InterstitialTimer = newFreq;
						}
					}

					var extras = result ["extras"] as Hashtable;
					if (extras != null) {

						var coinPack_free = int.TryParse (extras ["coinpack_free"].ToString ().Trim (), out newCoinPack_Free);

						if (coinPack_free) {
							StoreConstants.CoinPack_Free = newCoinPack_Free;
						}

						var coinPack_1 = int.TryParse (extras ["coinpack_1"].ToString ().Trim (), out newCoinPack_1);

						if (coinPack_1) {
							StoreConstants.CoinPack_1 = newCoinPack_1;
						}

						var coinPack_2 = int.TryParse (extras ["coinpack_2"].ToString ().Trim (), out newCoinPack_2);

						if (coinPack_2) {
							StoreConstants.CoinPack_2 = newCoinPack_2;
						}

						var coinPack3 = int.TryParse (extras ["coinpack_3"].ToString ().Trim (), out newCoinPack_3);

						if (coinPack3) {
							StoreConstants.CoinPack_3 = newCoinPack_3;
						}

						var move_Pack_free = int.TryParse (extras ["movepack_free"].ToString ().Trim (), out newMovePack_Free);

						if (move_Pack_free) {
							StoreConstants.MovePack_Free = newMovePack_Free;
						}

						var movePack_1 = int.TryParse (extras ["movepack_1"].ToString ().Trim (), out newMovePack_1);

						if (movePack_1) {
							StoreConstants.MovePack_1 = newMovePack_1;
						}

						var movePack_2 = int.TryParse (extras ["movepack_2"].ToString ().Trim (), out newMovePack_2);

						if (movePack_2) {
							StoreConstants.MovePack_2 = newMovePack_2;
						}

						var movePack3 = int.TryParse (extras ["movepack_3"].ToString ().Trim (), out newMovePack_3);

						if (movePack3) {
							StoreConstants.MovePack_3 = newMovePack_3;
						}

						var rewardedMoves = int.TryParse (extras ["rewarded_moves"].ToString ().Trim (), out newRewardedMoves);

						if (rewardedMoves) {
							StoreConstants.Rewarded_Moves = newRewardedMoves;
						}

						var rewardedCoins = int.TryParse (extras ["rewarded_coins"].ToString ().Trim (), out newRewardedCoins);

						if (rewardedCoins) {
							StoreConstants.Rewarded_Coins = newRewardedCoins;
						}

						var first_time_hint_price = int.TryParse (extras ["first_time_hint_price"].ToString ().Trim (), out newfirstTimeHintPrice);

						if (first_time_hint_price) {
							DynamicConstants.firstTimeHintPrice = newfirstTimeHintPrice;
						}

						var hint_already_used_price = int.TryParse (extras ["hint_already_used_price"].ToString ().Trim (), out newhintAlreadyUsedPrice);

						if (hint_already_used_price) {
							DynamicConstants.hintAlreadyUsedPrice = newhintAlreadyUsedPrice;
						}

						var level_end_coins_awarded = int.TryParse (extras ["level_end_coins_awarded"].ToString ().Trim (), out newlevelEndCoinsAwarded);

						if (level_end_coins_awarded) {
							DynamicConstants.levelEndCoinsAwarded = newlevelEndCoinsAwarded;
						}

						var coins_awarded_at_start = int.TryParse (extras ["coins_awarded_at_start"].ToString ().Trim (), out newcoinsAwardedAtStart);

						if (coins_awarded_at_start) {
							DynamicConstants.CoinsAwardedAtStart = newcoinsAwardedAtStart;
						}

						var moves_awarded_at_start = int.TryParse (extras ["moves_awarded_at_start"].ToString ().Trim (), out newmovesAwardedAtStart);

						if (moves_awarded_at_start) {
							DynamicConstants.MovesAwardedAtStart = newmovesAwardedAtStart;
						}

						var coins_required_for_dialogue = int.TryParse (extras ["coins_required_for_dialogue"].ToString ().Trim (), out newcoinsrequiredfordialogue);

						if (coins_required_for_dialogue) {
							DynamicConstants.CoinsRequiredForDialgoue = newcoinsrequiredfordialogue;
						}

							var coins_required_for_accessory = int.TryParse (extras ["coins_required_for_accessory"].ToString ().Trim (), out newcoinsrequiredforaccessory);

						if (coins_required_for_accessory) {
							DynamicConstants.CoinsRequiredForAccessory = newcoinsrequiredforaccessory;
						}

						// Make this part dynamic for all the stories.
						var coins_for_unlocking_story2 = int.TryParse (extras ["coins_for_unlocking_story2"].ToString ().Trim (), out newcoinsforunlockingstory2);

						if (coins_for_unlocking_story2) {
							DynamicConstants.CoinsForUnlockingStory2 = newcoinsforunlockingstory2;
						}

						var moves_given_for_unlocking_story2 = int.TryParse (extras ["moves_given_for_unlocking_story2"].ToString ().Trim (), out newmovesgivenforunlockingstory2);

						if (moves_given_for_unlocking_story2) {
							DynamicConstants.MovesGivenForUnlockingStory2 = newmovesgivenforunlockingstory2;
						}

						var coins_for_unlocking_story3 = int.TryParse (extras ["coins_for_unlocking_story3"].ToString ().Trim (), out newcoinsforunlockingstory3);

						if (coins_for_unlocking_story3) {
							DynamicConstants.CoinsForUnlockingStory3 = newcoinsforunlockingstory3;
						}

						var moves_given_for_unlocking_story3 = int.TryParse (extras ["moves_given_for_unlocking_story3"].ToString ().Trim (), out newmovesgivenforunlockingstory3);

						if (moves_given_for_unlocking_story3) {
							DynamicConstants.MovesGivenForUnlockingStory3 = newmovesgivenforunlockingstory3;
						}

						var coins_for_unlocking_story4 = int.TryParse (extras ["coins_for_unlocking_story4"].ToString ().Trim (), out newcoinsforunlockingstory4);

						if (coins_for_unlocking_story4) {
							DynamicConstants.CoinsForUnlockingStory4 = newcoinsforunlockingstory4;
						}

						var moves_given_for_unlocking_story4 = int.TryParse (extras ["moves_given_for_unlocking_story4"].ToString ().Trim (), out newmovesgivenforunlockingstory4);

						if (moves_given_for_unlocking_story4) {
							DynamicConstants.MovesGivenForUnlockingStory4 = newmovesgivenforunlockingstory4;
						}

						var coins_for_unlocking_story5 = int.TryParse (extras ["coins_for_unlocking_story5"].ToString ().Trim (), out newcoinsforunlockingstory5);

						if (coins_for_unlocking_story5) {
							DynamicConstants.CoinsForUnlockingStory5 = newcoinsforunlockingstory5;
						}

						var moves_given_for_unlocking_story5 = int.TryParse (extras ["moves_given_for_unlocking_story5"].ToString ().Trim (), out newmovesgivenforunlockingstory5);

						if (moves_given_for_unlocking_story5) {
							DynamicConstants.MovesGivenForUnlockingStory5 = newmovesgivenforunlockingstory5;
						}

						var coins_for_unlocking_story1 = int.TryParse (extras ["coins_for_unlocking_story1"].ToString ().Trim (), out newcoinsforunlockingstory1);

						if (coins_for_unlocking_story1) {
							DynamicConstants.CoinsForUnlockingStory1 = newcoinsforunlockingstory1;
						}

						var moves_given_for_unlocking_story1 = int.TryParse (extras ["moves_given_for_unlocking_story1"].ToString ().Trim (), out newmovesgivenforunlockingstory1);

						if (moves_given_for_unlocking_story1) {
							DynamicConstants.MovesGivenForUnlockingStory1 = newmovesgivenforunlockingstory1;
						}

						var coins_for_unlocking_story6 = int.TryParse (extras ["coins_for_unlocking_story6"].ToString ().Trim (), out newcoinsforunlockingstory6);

						if (coins_for_unlocking_story6) {
							DynamicConstants.CoinsForUnlockingStory6 = newcoinsforunlockingstory6;
						}

						var moves_given_for_unlocking_story6 = int.TryParse (extras ["moves_given_for_unlocking_story6"].ToString ().Trim (), out newmovesgivenforunlockingstory6);

						if (moves_given_for_unlocking_story6) {
							DynamicConstants.MovesGivenForUnlockingStory6 = newmovesgivenforunlockingstory6;
						}



						var free_moves_timer = int.TryParse (extras ["free_moves_timer"].ToString ().Trim (), out newfreemovestimer);

						if (free_moves_timer) {
							DynamicConstants.freeMovesTime = newfreemovestimer;
						}

						var timer_moves_awarded = int.TryParse (extras ["timer_moves_awarded"].ToString ().Trim (), out newtimermovesaward);

						if (timer_moves_awarded) {
							DynamicConstants.timerMovesAward = newtimermovesaward;
						}

                        DynamicConstants.timerText = extras["timer_text"].ToString();
                    }
			
				//	wwwUrl = new WWW (promo ["image_url"].ToString ());

				//	yield return wwwUrl;

					promoURL1 = new WWW (one ["image_url"].ToString ());

					clickURL1 = one ["click_url"].ToString ();
					promoCampaign1 = one ["campaign"].ToString ();
					yield return promoURL1;
					promoTex1 = promoURL1.texture;

					promoURL2 = new WWW (two ["image_url"].ToString ());

					clickURL2 = two ["click_url"].ToString ();
					promoCampaign2 = two ["campaign"].ToString ();

					yield return promoURL2;
					promoTex2 = promoURL2.texture;

					//third image
					promoURL3 = new WWW (three ["image_url"].ToString ());

					clickURL3 = three ["click_url"].ToString ();
					promoCampaign3 = three ["campaign"].ToString ();

					yield return promoURL3;
					promoTex3 = promoURL3.texture;

					bUrlLoad = true;

                    Debug.Log("FETCHING COMPLETE");
                    ///Had to create this here so that the starting coins and moves can be fetched online first time he installs the game.

                    PlayerPrefs.Save ();
				}
			} else {
				Debug.Log (req.error);
			}
		}
        else
        {
            DynamicConstants.CoinsForUnlockingStory2 = 0;
			DynamicConstants.CoinsForUnlockingStory3 = 120;
			DynamicConstants.CoinsForUnlockingStory4 = 100;
            DynamicConstants.CoinsForUnlockingStory1 = 140;
            DynamicConstants.CoinsForUnlockingStory5 = 150;
			DynamicConstants.CoinsForUnlockingStory6 = 70;

DynamicConstants.MovesGivenForUnlockingStory2 = 4;
DynamicConstants.MovesGivenForUnlockingStory3 = 4;
DynamicConstants.MovesGivenForUnlockingStory4 = 4;
DynamicConstants.MovesGivenForUnlockingStory1 = 4;
DynamicConstants.MovesGivenForUnlockingStory5 = 4;
DynamicConstants.MovesGivenForUnlockingStory6 = 4;
        }
        int isFirstTime = PlayerPrefs.GetInt ("isFirstTimeCoins", 0);

					if (isFirstTime == 0) {
						Debug.Log ("UPDATE COINS" + StoreConstants.totalCoins);
						PlayerPrefs.SetInt ("isFirstTimeCoins", 1);
						if (StoreConstants.totalCoins == 0) {
							StoreConstants.totalCoins = DynamicConstants.CoinsAwardedAtStart;
						}
						if (StoreConstants.totalMoves == 0) {
							StoreConstants.totalMoves = DynamicConstants.MovesAwardedAtStart;	
						}

					}
	}
	// Update is called once per frame
	void Update ()
	{
	
	}
}
