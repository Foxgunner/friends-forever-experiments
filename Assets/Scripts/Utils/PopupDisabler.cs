﻿using UnityEngine;
using System.Collections;

public class PopupDisabler : MonoBehaviour {

	void OnEnable()
	{
		G2WBackButtonHelper.ispopupOn = true;
	}

	void OnDisable()
	{
		G2WBackButtonHelper.ispopupOn = false;
	}
}
