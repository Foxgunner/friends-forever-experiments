﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PreLoaderScript : MonoBehaviour {

	void Awake()
	{
TextLocalization.Init ();
GameConstants.sessionCount++;
		Debug.Log ("LANGUAGE" + Application.systemLanguage.ToString ());
        if (!GameConstants.hasOverriddenLanguage)
        {
            GameConstants.currentLanguage = Application.systemLanguage.ToString();
        }
	
        if (GameConstants.currentLanguage == "French") {
			TextLocalization.SelectLanguage ("French");
		} 
		else if (GameConstants.currentLanguage == "Italian") {
			TextLocalization.SelectLanguage ("Italian");
		 } 
		 //else if (GameConstants.currentLanguage == "German") {
		 	//TextLocalization.SelectLanguage ("German");
	//	} 
		else if (GameConstants.currentLanguage == "Spanish") {
		 	TextLocalization.SelectLanguage ("Spanish");
		}  
		else if (GameConstants.currentLanguage == "Portuguese") {
			TextLocalization.SelectLanguage ("Portuguese");
		} else {
			TextLocalization.SelectLanguage ("English");
		}
     //  GameConstants.currentLanguage = "Portuguese";
     // 	TextLocalization.SelectLanguage ("Portuguese");	
		GameConstants.currentGameState = GameStates.INTRO;
		SceneManager.LoadScene ("IntroScene");
	
	//	 SceneManager.LoadScene("TempLevelSelection");


	}
}
