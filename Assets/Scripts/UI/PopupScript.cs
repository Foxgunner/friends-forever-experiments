﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PopupScript : MonoBehaviour {

	public enum ValueType
	{
		MovesAwardedStore,
		CoinsAwardedStore,
		MovesAwarded,
		CoinsAwarded,
		None
	}

	public ValueType vtype;

	public Text popupText;

	void OnEnable()
	{
		G2WBackButtonHelper.ispopupOn = true;
		setText ();
	}

	void OnDisable()
	{
		Debug.Log ("POPUP DISABLE");
		G2WBackButtonHelper.ispopupOn = false;
	}

	public void setText()
	{
		if (vtype == ValueType.CoinsAwarded || vtype == ValueType.CoinsAwardedStore) {
			if (GameConstants.currentLanguage == "French") {
			popupText.text = "Félicitations ! Il vous a été attribué " + StoreConstants.Rewarded_Coins + " pièces";
		} else if (GameConstants.currentLanguage == "Italian") {
			popupText.text = "Congratulazioni! Hai ottenuto " + StoreConstants.Rewarded_Coins + " monete";
		} else if (GameConstants.currentLanguage == "German") {
			popupText.text = "Congrats! You've been awarded " + StoreConstants.Rewarded_Coins + " coins";
		} else if (GameConstants.currentLanguage == "Spanish") {
			popupText.text = "Congrats! You've been awarded " + StoreConstants.Rewarded_Coins + " coins";
		}  else if (GameConstants.currentLanguage == "Portuguese") {
		popupText.text = "Parabéns! Recebeste  " + StoreConstants.Rewarded_Coins + " moedas";
		} else {
			popupText.text = "Congrats! You've been awarded " + StoreConstants.Rewarded_Coins + " coins";
		}
			
		}

		else if (vtype == ValueType.MovesAwarded || vtype == ValueType.MovesAwardedStore) {
			if (GameConstants.currentLanguage == "French") {
						popupText.text = "Félicitations ! Il vous a été attribué " + StoreConstants.Rewarded_Moves + " coups";
		} else if (GameConstants.currentLanguage == "Italian") {
						popupText.text = "Congratulazioni! Hai ottenuto " + StoreConstants.Rewarded_Moves + " mosse";
		} else if (GameConstants.currentLanguage == "German") {
						popupText.text = "Congrats! You've been awarded " + StoreConstants.Rewarded_Moves + " moves";
		} else if (GameConstants.currentLanguage == "Spanish") {
						popupText.text = "Congrats! You've been awarded " + StoreConstants.Rewarded_Moves + " moves";
		}  else if (GameConstants.currentLanguage == "Portuguese") {
					popupText.text = "Parabéns! Recebeste " + StoreConstants.Rewarded_Moves + " jogadas";
		} else {
					popupText.text = "Congrats! You've been awarded " + StoreConstants.Rewarded_Moves + " moves";
		}

		}
	}
}
