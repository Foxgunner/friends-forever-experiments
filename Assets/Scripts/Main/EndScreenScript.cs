﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Prime31;

public class EndScreenScript : MonoBehaviour
{

    // Used for blocking the buttons in ChapterSelectionScreen when Store is added additively as a hack because buttons in the ChapterSelectionScreen are getting clicked in Store (Unity bug).
    public GameObject panel;

    //Screen contains the collect reward button and it displays the amount of coins won by the user.
    public GameObject screen1;

    //Screen contains the End Screen buttons.
    public GameObject screen2;

    //Whether the first screen has been shown or not.
    private bool isFirstScreenShown = true;

    //Feedback Panel
    public GameObject feedbackPanel;

    public GameObject rateTranslationPanel;

    public GameObject storyFeedbackPanel;

    public Text levelendCoinText;

    public GameObject coinParticleObject;

    public ParticleSystem coinParticle;



    public bool enter;
#if UNITY_PRE_IOS7_TARGET
	public static string RateUsLink =  "itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1152276761";




#else
    public static string RateUsLink = "itms-apps://itunes.apple.com/en/app/id1152276761";
#endif


    void OnParticleTrigger()
    {
        if (enter)
        {
            Debug.Log("YES");
        }
    }

    void Update()
    {
        var ts = coinParticle.trigger;
        ts.enter = enter ? ParticleSystemOverlapAction.Callback : ParticleSystemOverlapAction.Ignore;
    }

    void OnEnable()
    {
        Messenger.AddListener<string>(MessengerConstants.DISABLE_BACK_PANEL, hidePanel);
        Messenger.MarkAsPermanent(MessengerConstants.DISABLE_BACK_PANEL);
    }

    void OnDisable()
    {
        Messenger.RemoveListener<string>(MessengerConstants.DISABLE_BACK_PANEL, hidePanel);
    }

    void Awake()
    {
        GameConstants.currentGameState = GameStates.GAME_END;
    }

    void Start()
    {
         levelendCoinText.text = DynamicConstants.levelEndCoinsAwarded.ToString();
        //ACHIEVEMENTS
        if (GameConstants.currentStoryNumber == 1 && GameConstants.currentChapterNumber == 1)
        {
            GameServicesHelper.UnlockAchievements(AchievementConstants.THE_EARLY_READER);
        }

        //

        if (isFirstScreenShown && P31Prefs.getInt("Story" + GameConstants.currentStoryNumber + "Chapter" + GameConstants.currentChapterNumber) != 2)
        {
            screen1.SetActive(true);
            screen2.SetActive(false);
   
        }
        else
        {
 
            screen1.SetActive(false);
            screen2.SetActive(true);

            runGAandAchievements();
            levelendCoinText.text = DynamicConstants.levelEndCoinsAwarded.ToString();

            MiscGAScript.GAforGameplay();
        }
    }

    public void showRateTranslationPanel()
    { 
        if((GameConstants.currentChapterNumber == 5 || GameConstants.currentChapterNumber == 10) && GameConstants.askedRateUsTranslationNumber != 2)
        {
            rateTranslationPanel.SetActive(true);
            GameConstants.askedRateUsTranslationNumber++;
            G2WGAHelper.LogEvent("Rate Language - Shown", GameConstants.currentStoryNumber.ToString(), "Seen", 1);
            RateUsScript.rateUsScript.feedbackParticleEffect.SetActive(false);
        }
    }

    public void showStoryFeedbackPanel()
    {
         if(GameConstants.askedStoryFeedbackRateUsNumber != 1)
        {
            G2WGAHelper.LogEvent("Rate Story - Shown", GameConstants.currentStoryNumber.ToString(), "Seen", 1);
            storyFeedbackPanel.SetActive(true);
            FeedbackRateUsScript.feedbackRateUsScript.feedbackParticleEffect.SetActive(false);
        }
    }


    //Awards the user with coins for completing the level.
    public void collectRewardHandler()
    {
        screen1.SetActive(false);
        screen2.SetActive(true);

        coinParticleObject.SetActive(true);
        isFirstScreenShown = false;
        P31Prefs.setInt("Story" + GameConstants.currentStoryNumber + "Chapter" + GameConstants.currentChapterNumber, 2);
        StoreConstants.totalCoins += DynamicConstants.levelEndCoinsAwarded;

        runGAandAchievements();
StartCoroutine("showPopups");
        
    }

    IEnumerator showPopups(){
        yield return new WaitForSeconds(1.5f);
if (Application.internetReachability != NetworkReachability.NotReachable)
        {
                if (GameConstants.currentLanguage != "English")
                {
                    showRateTranslationPanel();
                }
                if(GameConstants.currentChapterNumber == 30)
            {
                showStoryFeedbackPanel();
            }
        }
    }


    public void loadNextChapter()
    {
        if (GameConstants.currentChapterNumber < 30)
        {
            if (GameConstants.currentStoryNumber == 2)
            {
                if (GameConstants.currentChapterNumber == 5)
                {
                    GameServicesHelper.UnlockAchievements(AchievementConstants.FASTEST_FIVE);
                }
                else if (GameConstants.currentChapterNumber == 15)
                {
                    GameServicesHelper.UnlockAchievements(AchievementConstants.BLOSSOMING_ROMANCE);
                }
            }
            if (GameConstants.currentStoryNumber == 3)
            {
                if (GameConstants.currentChapterNumber == 10)
                {
                    GameServicesHelper.UnlockAchievements(AchievementConstants.TANGO_EXPERT);
                }
                else if (GameConstants.currentChapterNumber == 29)
                {
                    GameServicesHelper.UnlockAchievements(AchievementConstants.HERO_OF_THE_DAY);
                }
               
            }
            if (GameConstants.currentStoryNumber == 4)
            {
                if (GameConstants.currentChapterNumber == 5)
                {
                    GameServicesHelper.UnlockAchievements(AchievementConstants.LAB_ROMANCE);
                }
                else if (GameConstants.currentChapterNumber == 10)
                {
                    GameServicesHelper.UnlockAchievements(AchievementConstants.SWEET_ENCOUNTER  );
                }
                else if (GameConstants.currentChapterNumber == 15)
                {
                    GameServicesHelper.UnlockAchievements(AchievementConstants.LIVING_IT_UP  );
                }
                else if (GameConstants.currentChapterNumber == 21)
                {
                    GameServicesHelper.UnlockAchievements(AchievementConstants.A_FRIEND_IN_NEED  );
                }
               
            }
            if (GameConstants.currentStoryNumber == 5)
            {
                if (GameConstants.currentChapterNumber == 5)
                {
                    GameServicesHelper.UnlockAchievements(AchievementConstants.SIGNS_SPECIALIST);
                }

                 if (GameConstants.currentChapterNumber == 20)
                {
                    GameServicesHelper.UnlockAchievements(AchievementConstants.REAL_ENCOUNTER);
                }
            }
             if (GameConstants.currentStoryNumber == 6)
            {
                if (GameConstants.currentChapterNumber == 5)
                {
                    GameServicesHelper.UnlockAchievements(AchievementConstants.TAKES_TWO_TO_TANGO);
                }

                 if (GameConstants.currentChapterNumber == 10)
                {
                    GameServicesHelper.UnlockAchievements(AchievementConstants.CURIOUS_CHOICES);
                }
                  if (GameConstants.currentChapterNumber == 15)
                {
                    GameServicesHelper.UnlockAchievements(AchievementConstants.ON_THE_FENCE);
                }


            }
            GameConstants.currentChapterNumber++;
            GameConstants.currentGameState = GameStates.LEVEL_SELECT;
               SceneManager.LoadScene("LevelSelectionScreen");
            //SceneManager.LoadScene("TempLevelSelection");
        }
        else
        {
            GameConstants.currentGameState = GameStates.CHAPTER_SELECT;
            GameConstants.isStoryCompleted = true;
            GameConstants.currentChapterNumber = 1;
            GameConstants.isStoryNameGiven = false;
            SceneManager.LoadScene("ChapterSelectionScreen");
           // SceneManager.LoadScene("TempLevelSelection");
            //ACHIEVEMENTS
            if (GameConstants.currentStoryNumber == 1)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.EAGER_READER);
            }
            else if (GameConstants.currentStoryNumber == 2)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.DILIGENT_READER);
            }
             else if (GameConstants.currentStoryNumber == 3)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.LOYAL_READER);
            }
             else if (GameConstants.currentStoryNumber == 4)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.ARDENT_READER);
            }
             else if (GameConstants.currentStoryNumber == 5)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.DEDICATED_READER);
            }
             else if (GameConstants.currentStoryNumber == 5)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.CASE_CRACKED);
            }

            //
        }
        //SceneManager.LoadScene (GameConstants.currentSceneName);
    }


    public void loadStorySelection()
    {
        GameConstants.currentChapterNumber++;
       SceneManager.LoadScene("ChapterSelectionScreen");
       // SceneManager.LoadScene("TempLevelSelection");
    }



    public void shareFunction()
    {
        if (GameConstants.isNetConnected())
        {
            //FlurryHelper.LogEvent ("Share - Whatsapp");
            /*OLD : Application.OpenURL ("whatsapp://send?text=This game has driven me nuts. Its Rockin’ MAN!Download World Cup Cricket Champs-2015 from the Play Store for FREE and am sure you'll also forget everything else in this world! bit.ly/wccc_andr");*/
//             if (GameConstants.currentLanguage == "French")
//             {
// #if UNITY_IOS
// 			Application.OpenURL ("whatsapp://send?text=Live%20a%20crazy%20adventure%20with%20Friends%20Forever%2C%20where%20you%20are%20the%20star!%20Choose%20your%20path%20and%20build%20your%20story.%20Download%20Friends%20Forever%20from%20the%20App%20store%20here%20bit.ly%2Ffforever_ios");
// #else
//                 Application.OpenURL("whatsapp://send?text=Vivez une aventure délirante avec les Friends Forever où vous êtes la Star ! Choisissez votre chemin et construisez votre histoire. Téléchargez les Friends Forever à partir de Play Store ici bit.ly/ffever_andr");
// #endif
//             }
//             else if (GameConstants.currentLanguage == "Italian")
//             {
// #if UNITY_IOS
// 			Application.OpenURL ("whatsapp://send?text=Live%20a%20crazy%20adventure%20with%20Friends%20Forever%2C%20where%20you%20are%20the%20star!%20Choose%20your%20path%20and%20build%20your%20story.%20Download%20Friends%20Forever%20from%20the%20App%20store%20here%20bit.ly%2Ffforever_ios");
// #else
//                 Application.OpenURL("whatsapp://send?text=Vivi un'incredibile avventura con Friends Forever, in cui tu sei la stella! Scegli il tuo percorso e costruisci la tua storia. Scarica Friends Forever dal Play store bit.ly/ffever_andr");
// #endif
//             }
//             else if (GameConstants.currentLanguage == "German")
//             {
// #if UNITY_IOS
// 			Application.OpenURL ("whatsapp://send?text=Live%20a%20crazy%20adventure%20with%20Friends%20Forever%2C%20where%20you%20are%20the%20star!%20Choose%20your%20path%20and%20build%20your%20story.%20Download%20Friends%20Forever%20from%20the%20App%20store%20here%20bit.ly%2Ffforever_ios");
// #else
//                 Application.OpenURL("whatsapp://send?text=Erlebe verrücktes Abenteuer mit Friends Forever, indem du der Star bist! Wähle deinen Weg und erschaffe deine Geschichte. Lade Friends Forever aus dem Play Store hier herunter bit.ly/ffever_andr");
// #endif
//             }
//             else if (GameConstants.currentLanguage == "Spanish")
//             {
// #if UNITY_IOS
// 			Application.OpenURL ("whatsapp://send?text=Live%20a%20crazy%20adventure%20with%20Friends%20Forever%2C%20where%20you%20are%20the%20star!%20Choose%20your%20path%20and%20build%20your%20story.%20Download%20Friends%20Forever%20from%20the%20App%20store%20here%20bit.ly%2Ffforever_ios");
// #else
//                 Application.OpenURL("whatsapp://send?text=¡Vive una loca aventura con Friends Forever, donde tú eres la estrella! Elige tu camino y construye tu historia. Descarga Friends Forever en Play store aquí bit.ly/ffever_andr");
// #endif
//             }
//             else if (GameConstants.currentLanguage == "Portuguese")
//             {
// #if UNITY_IOS
// 			Application.OpenURL ("whatsapp://send?text=Live%20a%20crazy%20adventure%20with%20Friends%20Forever%2C%20where%20you%20are%20the%20star!%20Choose%20your%20path%20and%20build%20your%20story.%20Download%20Friends%20Forever%20from%20the%20App%20store%20here%20bit.ly%2Ffforever_ios");
// #else
//                 Application.OpenURL("whatsapp://send?text=Vive uma aventura excitante com o Friends Forever, onde tu és a estrela! Escolhe o teu caminho e cria a tua história. Transfere o Friends Forever desta loja Play bit.ly/ffever_andr");
// #endif
//             }
//             else
//             {
#if UNITY_IOS
			Application.OpenURL ("whatsapp://send?text=Live%20a%20crazy%20adventure%20with%20Friends%20Forever%2C%20where%20you%20are%20the%20star!%20Choose%20your%20path%20and%20build%20your%20story.%20Download%20Friends%20Forever%20from%20the%20App%20store%20here%20bit.ly%2Ffforever_ios");
#else
                 Application.OpenURL("whatsapp://send?text=Live a crazy adventure with Friends Forever, where you are the star! Choose your path and build your story. Download Friends Forever from the Play store here bit.ly/ffever_andr");
 #endif
//             }



        }
    }

    public void rateUsButton()
    {
        if (GameConstants.isNetConnected())
        {
            //Application.OpenURL ("market://details?id=com.games2win.friendsforever");
#if UNITY_IOS

			Application.OpenURL (RateUsLink);
#else
            Application.OpenURL("market://details?id=com.games2win.friendsforever");
#endif
        }
    }

    //Restart current chapter/level.
    public void restartLevel()
    {
        SceneManager.LoadScene(GameConstants.getSceneName());
    }

    /// <summary>
    /// Opens the feedback panel.
    /// </summary>
    public void openFeedbackPanel()
    {
        if (GameConstants.isNetConnected())
        {
            feedbackPanel.SetActive(true);
            G2WBackButtonHelper.isFeedBackOn = true;
            FeedbackScript.feedbackScript.feedbackParticleEffect.SetActive(false);
        }
    }

    /// <summary>
    /// Loads the store.
    /// </summary>
    /// <param name="type">Coins or Moves</param>
    public void loadStore(string type)
    {
        if (type == "coins")
        {
            StoreConstants.showCoinScreen = true;
        }
        else
        {
            StoreConstants.showCoinScreen = false;
        }
        //panel.SetActive (true);
        SceneManager.LoadScene("Store", LoadSceneMode.Additive);

    }

    public void hidePanel(string s)
    {
        panel.SetActive(false);
    }

    public void runGAandAchievements()
    {
        MiscGAScript.chapterEndEvents();
        if (GameConstants.isHiddenObject())
        {
            MiscGAScript.hiddenObjectEndEvents();

            if (GameConstants.currentChapterNumber == 5)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.KEEN_EYE);
            }
            if (GameConstants.currentStoryNumber == 2 && GameConstants.currentChapterNumber == 23)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.DETECTIVE_EXTRAORDINAIRE);
            }
            if (GameConstants.currentStoryNumber == 3 && GameConstants.currentChapterNumber == 21)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.AGENT_TO_THE_RESCUE);
            }
            if (GameConstants.currentStoryNumber == 4 && GameConstants.currentChapterNumber == 25)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.THE_HEIST_PRO);
            }
            GameConstants.coinsUsedForHints = 0;
            GameConstants.coinsUsedForTimer = 0;
        }
    }

}
