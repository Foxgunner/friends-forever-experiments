﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageSelectionScript : MonoBehaviour
{

    public Toggle[] languageToggles;

    public GameObject popup;

    private string tempLanguage;

    private int tempcounter = 0;

    // Use this for initialization
    void Start()
    {

        tempLanguage = GameConstants.currentLanguage;
        if (GameConstants.currentLanguage == "French")
        {
            languageToggles[3].isOn = true;
        }
        else if (GameConstants.currentLanguage == "Italian")
        {
            languageToggles[4].isOn = true;
        }
        //  else if (GameConstants.currentLanguage == "German") {
        //  	languageToggles[3].isOn = false;
        // } 
        else if (GameConstants.currentLanguage == "Spanish")
        {
            languageToggles[2].isOn = true;
        }
        else if (GameConstants.currentLanguage == "Portuguese")
        {
            languageToggles[1].isOn = true;
        }
        else
        {
            languageToggles[0].isOn = true;
        }
        tempcounter++;
    }

    public void changeLanguage(string language)
    {
        Debug.Log("SET POPUP" + tempcounter);
        if (tempcounter != 0)
        {
            if (language != tempLanguage)
            {
                if (popup != null)
                {
                    popup.SetActive(true);
                    tempLanguage = language;
                }
                else
                {
                    tempLanguage = language;
                    confirmedLanguage();
                }
            }
        }
    }

    public void confirmedLanguage()
    {
        if (popup != null)
        {
            popup.SetActive(false);
        }

        GameConstants.hasOverriddenLanguage = true;
        TextLocalization.Init();
        if (tempLanguage == "French")
        {
             languageToggles[3].isOn = true;
            GameConstants.currentLanguage = "French";
             G2WGAHelper.LogEvent("Change Language", "Language", GameConstants.currentLanguage, 1);
            TextLocalization.SelectLanguage("French");
           
        }
        else if (tempLanguage == "Italian")
        {
             languageToggles[4].isOn = true;
            GameConstants.currentLanguage = "Italian";
             G2WGAHelper.LogEvent("Change Language", "Language", GameConstants.currentLanguage, 1);
            TextLocalization.SelectLanguage("Italian");
           
        }
        // else if (GameConstants.currentLanguage == "German") {
        //	GameConstants.currentLanguage = "French";
        //   TextLocalization.SelectLanguage ("French");
        //	languageToggles[3].isOn = true;
        //	} 
        else if (tempLanguage == "Spanish")
        {
             languageToggles[2].isOn = true;
            GameConstants.currentLanguage = "Spanish";
             G2WGAHelper.LogEvent("Change Language", "Language", GameConstants.currentLanguage, 1);
            TextLocalization.SelectLanguage("Spanish");
           
        }
        else if (tempLanguage == "Portuguese")
        {
              languageToggles[1].isOn = true;
            GameConstants.currentLanguage = "Portuguese";
             G2WGAHelper.LogEvent("Change Language", "Language", GameConstants.currentLanguage, 1);
            TextLocalization.SelectLanguage("Portuguese");
          
        }
        else
        {
            languageToggles[0].isOn = true;
            GameConstants.currentLanguage = "English";
             G2WGAHelper.LogEvent("Change Language", "Language", GameConstants.currentLanguage, 1);
            TextLocalization.SelectLanguage("English");
            
        }

    
    }


}
