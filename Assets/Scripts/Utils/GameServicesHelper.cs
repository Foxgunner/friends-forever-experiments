﻿using UnityEngine;
using System.Collections;
using Prime31;

public class GameServicesHelper : MonoBehaviour
{


	#region GPS

	public static void authenticate ()
	{
		Debug.Log ("authentication started............");
#if UNITY_IOS

		GameCenterManager.playerAuthenticatedEvent += playerAuthenticated;
		GameCenterManager.playerFailedToAuthenticateEvent += playerFailedToAuthenticate;
		GameCenterBinding.authenticateLocalPlayer ();
#elif UNITY_ANDROID
        if (!isAuthenticated())
        {
            PlayGameServices.authenticate();
        }
#endif
    }
	#if UNITY_ANDROID
	private static bool shouldShowAchievement = false;

	public static bool isAuthenticated ()
	{
		return PlayGameServices.isSignedIn ();	
	}

	public static void SignIn (bool achSignal = false, bool trigFlurry = true, string flurryProp = null)
	{
		Debug.Log("GPGS SignIN");

		PlayGameServices.enableDebugLog(true);

		if (Application.internetReachability != NetworkReachability.NotReachable) {
			GPGManager.authenticationSucceededEvent += authenticationSucceededEvent;
			GPGManager.authenticationFailedEvent += authenticationFailedEvent;
			shouldShowAchievement = achSignal;	
			Debug.Log("GPGS Is Signed In ?"+PlayGameServices.isSignedIn());
			if (!PlayGameServices.isSignedIn ()) {
				Debug.Log("GPGS Signing In Now");
				PlayGameServices.authenticate ();
//				if(trigFlurry)
//					FlurryHelper.LogEvent ("Google Sign In:", "Source", flurryProp);
			}
		} else {
			EtceteraAndroidManager.alertButtonClickedEvent += SignInHandler;
			EtceteraAndroid.showAlert ("Google+ sign in", "Unable to Connect to the Internet", "OK!", "Retry");
		}

	}

	private static void SignInHandler (string text)
	{
		EtceteraAndroidManager.alertButtonClickedEvent -= SignInHandler;
		if (text == "OK!") {
		} else if (text == "Retry") {
			if(shouldShowAchievement)
			{
				SignIn (true);
			}
			else
			{
				SignIn();
			}
			
		}else if (text == "Update Now") {
			Application.OpenURL ("market://details?id=com.google.android.gms");
		}
	}

	private static void SignOutHandler (string text)
	{
		shouldShowAchievement = false;
		EtceteraAndroidManager.alertButtonClickedEvent -= SignOutHandler;
		if (text == "OK!") {
		} else if (text == "Retry") {
			SignOut ();
		}
	}

	public static void SignOut ()
	{
		Debug.Log ("SignOut");
		if (Application.internetReachability != NetworkReachability.NotReachable) {
			PlayGameServices.signOut ();
			Debug.Log ("SignOut");
		} else {
			EtceteraAndroidManager.alertButtonClickedEvent += SignOutHandler;
			EtceteraAndroid.showAlert ("Google+ sign in", "Unable to Connect to the Internet", "OK!", "Retry");
			//Game.NoNetworkAvailable ();
		}
	}

	private static void authenticationSucceededEvent (string param)
	{
	Debug.Log ("Auth Succeeds GameServiceHelper Class");
		//googleLogin.transform.localPosition = new Vector3(500,100, 0);
		Debug.Log ("authenticationSucceededEvent: " + param);
		
		if(shouldShowAchievement)
		{
			shouldShowAchievement = false;
			showAchievements();
		}
		else
		{
			//EtceteraAndroid.showAlert ("Google+", "Signed in successfully!", "OK!");
		}
		GPGManager.authenticationSucceededEvent -= authenticationSucceededEvent;
		GPGManager.authenticationFailedEvent -= authenticationFailedEvent;
	}

	private static void authenticationFailedEvent (string error)
	{

		Debug.Log ("Auth Fails GameServiceHelper Class");
		Debug.Log ("authenticationFailedEvent: " + error);

		if(error == "Canceled") {
			Debug.Log ("Canceled by User");
			PlayerPrefs.SetInt("StopAutoSignIn", 1);
		}

		//GPGManager.authenticationSucceededEvent -= authenticationSucceededEvent;
		GPGManager.authenticationFailedEvent -= authenticationFailedEvent;
		EtceteraAndroidManager.alertButtonClickedEvent += SignInHandler;
		if (error.Contains ("SERVICE_VERSION_UPDATE_REQUIRED"))
		{
			EtceteraAndroid.showAlert ("Update Google Play Services", "Unfortunately your Google Play Services App is not updated. Please update it with the latest version and restart the game.", "Update Now");
		}
		else
		{
			//EtceteraAndroid.showAlert ("Google+ sign in", "Unable to Connect to the Internet", "Retry", "Cancel");
		}


	}

	public static void UnlockAchievements (string id)
	{
		if(!PlayerPrefs.HasKey(id))
		{
			if (PlayGameServices.isSignedIn ())
			{
				PlayGameServices.unlockAchievement (id);
				PlayerPrefs.SetInt(id, 1);
				PlayerPrefs.Save();
			}
		}
	}

	public static void showAchievements ()
	{
		if (PlayGameServices.isSignedIn ())
		{
			G2WGAHelper.LogEvent ("Achievements");
			PlayGameServices.showAchievements ();
		}

	}

	public static void showLeaderboard (string id)
	{
		
		if (PlayGameServices.isSignedIn ())
		{
			G2WGAHelper.LogEvent ("Leaderboard");
			PlayGameServices.showLeaderboard(id);
		}
		
	}
	#endif

	#if UNITY_IPHONE
	private static bool shouldShowAchievement = false;

	// Added by Raghavendra
	public static void SignIn (bool achSignal = false, bool trigFlurry = true)
	{
		
	}
	//

	public static bool isAuthenticated ()
	{

		return GameCenterBinding.isPlayerAuthenticated ();

	}

	static void playerAuthenticated ()
	{
		Debug.Log ("playerAuthenticated");
		GameCenterManager.playerAuthenticatedEvent -= playerAuthenticated;
		GameCenterManager.playerFailedToAuthenticateEvent -= playerFailedToAuthenticate;
		if (shouldShowAchievement) {
			showAchievements ();
			shouldShowAchievement = false;
		}
	}


	static void playerFailedToAuthenticate (string error)
	{
		Debug.Log ("playerFailedToAuthenticate: " + error);

		GameCenterManager.playerAuthenticatedEvent -= playerAuthenticated;
		GameCenterManager.playerFailedToAuthenticateEvent -= playerFailedToAuthenticate;
	}

	public static void showAchievements ()
	{
		Debug.Log ("Show Achivemnet called ..............................");
		if (GameCenterBinding.isPlayerAuthenticated ()) {
			Debug.Log ("Show Achivemnet loged in ..............................");
			GameCenterBinding.showAchievements ();
		} else {
			Debug.Log ("Show Achivemnet Show Alert ..............................");
			shouldShowAchievement = true;
			authenticate ();

			//EtceteraBinding.showAlertWithTitleMessageAndButtons ("Game Center", "Please log in to your Game Center account.", new[] { "OK" });
		}
	}

	public static void UnlockAchievements (string id)
	{

		if (GameCenterBinding.isPlayerAuthenticated ()) {
			GameCenterBinding.reportAchievement (id, 100f);
		}
	}
	#endif

	#endregion
}

