﻿using UnityEngine;
using System.Collections;

public class HiddenObject : MonoBehaviour {

	public string objName = "";
	public int id = -1;
	public Vector3 hintOffset = Vector3.zero;
	public bool mirrorHint = false;

	void Awake() {
		Debug.Log ("INSIDE HIDDEN AWAKE" + objName);
		Collider2D col2D = GetComponent<Collider2D>();
		if (col2D != null) {
			col2D.enabled = false;
		}
		objName = gameObject.name;
	}

	void OnEnable()
	{
		Messenger.AddListener<string> (MessengerConstants.OBJECT_CLICKED, Clicked);
		Messenger.MarkAsPermanent (MessengerConstants.OBJECT_CLICKED);
	}

	void OnDisable()
	{
		Messenger.RemoveListener<string> (MessengerConstants.OBJECT_CLICKED, Clicked);
	}
	
	void Start() {
#if UNITY_EDITOR
		//Debug.Assert(objName == name, string.Format("GameObject name {0} mismatch with value in script {1} in level {2}", name, objName, Application.loadedLevelName));
		Debug.Assert(transform.position.z < 0, string.Format("GameObject name {0} at z = 0 in  level {1}", name, Application.loadedLevelName));
		Debug.Assert(gameObject.tag == "HiddenObject", string.Format("GameObject name {0} has script but no tag in level {1}", gameObject.name, Application.loadedLevelName));
#endif
	}

	/// <summary>
	/// Activate this object.
	/// </summary>
	/// <param name="id">Identifier.</param>
	void Activate(int id) {
		Debug.Log ("INSIDE ACTIVATE");
		GetComponent<Collider2D>().enabled = true;
		this.id = id;
	}
	
	/// <summary>
	/// Player clicked this object.
	/// </summary>
	void Clicked(string s) {
		Debug.Log ("CLICKED FALSE");
		if (s == objName) {
			Debug.Log ("CLICKED TRUE");
			HOGameplayScript.CurrentLevel.ObjectClicked (id);
		}
	}
}
