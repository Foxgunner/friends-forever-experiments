﻿using UnityEngine;
using System.Collections;

public class AdsConstant
{

	#if UNITY_ANDROID
	public static bool isExitGamePopupActive = false;
	public static string mopubBannerPhoneID = "12d45ab3263f45f6bba2661d1dec5931";
	public static string mopubInterstitialPhoneID = "541b4aa543cc483388d2573a763c2c21";
	public static string mopubExitAdPhoneID = "62fb45c8afdc4383a50d63e7c03a75ea";

	public static string mopubBannerTabID = "c0e9328fef554b6c9abaf8a6345c5f02";
	public static string mopubInterstitialTabID = "270c46f5453a47038d90d98c20084608";
	public static string mopubExitAdTabID = "62fb45c8afdc4383a50d63e7c03a75ea";

	public static string SUPERSONIC_APPLICATION_KEY = "40e1ad2d";
	
	
#elif UNITY_IPHONE
	public static string APPName = "Friends Forever";
	public static string APPVersion = "1.1";
	public static string PNSID = "210";

	public static string dynamicDataLink = "http://s01.appucino.com/games/init?game_id=test_template_ios";

	public static string mopubBannerPhoneID = "9794688886e611e295fa123138070049";
	public static string mopubInterstitialPhoneID = "d9b9214a86e611e295fa123138070049";

	public static string mopubBannerTabID = "agltb3B1Yi1pbmNyDQsSBFNpdGUYmv-BFgw";
	public static string mopubInterstitialTabID = "agltb3B1Yi1pbmNyDQsSBFNpdGUYpLb1FQw";

	#endif

	public static int InterstitialTimer {
		get {
			return PlayerPrefs.GetInt ("InterstitialTimer", 60);
		}
		set {
			PlayerPrefs.SetInt ("InterstitialTimer", value);
			PlayerPrefs.Save ();
		}
	}
	
}
