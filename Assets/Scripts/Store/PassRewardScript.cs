﻿using UnityEngine;
using System.Collections;
using Prime31;
using UnityEngine.UI;

public class PassRewardScript : MonoBehaviour {

	public GameObject Btn_Collect, TimeKeeper;
    // Use this for initialization
    public Text movesTimerText;

    void OnEnable()
    {
		Messenger.AddListener<string>(MessengerConstants.START_TIMER, resetValues);
        Messenger.MarkAsPermanent(MessengerConstants.START_TIMER);
		Messenger.AddListener<string>(MessengerConstants.AWARD_TIMER, FreePasses);
        Messenger.MarkAsPermanent(MessengerConstants.AWARD_TIMER);

    }
    void OnDisable()
	{
		Messenger.RemoveListener<string>(MessengerConstants.START_TIMER, resetValues);
		Messenger.RemoveListener<string>(MessengerConstants.AWARD_TIMER, FreePasses);

	}
    void Start () {
		if(movesTimerText.gameObject!=null)
		{
			movesTimerText.text = "GET " +DynamicConstants.timerMovesAward.ToString() + " MOVES IN";
		}
        if (GameConstants.canCollectMove)
        {
            CheckTime();
        }
        else
        { 
			TimeKeeper.gameObject.SetActive (false);
		}
    }
	
	void CheckTime ()
	{
		var temp = System.DateTime.Now.Hour - PlayerPrefs.GetInt ("CurrentHour");
		Debug.Log ("Current Hour: " + PlayerPrefs.GetInt ("CurrentHour") + " TemP: " + temp + " NowHour: " + System.DateTime.Now.Hour);
		if (temp >= DynamicConstants.freeMovesTime) {
            FreePasses();
            //Btn_Collect.gameObject.SetActive (true);
            TimeKeeper.gameObject.SetActive (false);
			PlayerPrefsX.SetBool ("VideoTimer", false);
			Debug.Log ("Entering Temp>=4");
		} else if (System.DateTime.Now.Day  > PlayerPrefs.GetInt ("CurrentDay") + 1) {
			FreePasses();
			//Btn_Collect.gameObject.SetActive (true);
			TimeKeeper.gameObject.SetActive (false);
			PlayerPrefsX.SetBool ("VideoTimer", false);
			Debug.Log ("Entering Day");
		} else if (System.DateTime.Now.Month > PlayerPrefs.GetInt ("CurrentMonth")) {
			FreePasses();
			//Btn_Collect.gameObject.SetActive (true);
			TimeKeeper.gameObject.SetActive (false);
			PlayerPrefsX.SetBool ("VideoTimer", false);
			Debug.Log ("Entering Month");
		} else if (System.DateTime.Now.Year > PlayerPrefs.GetInt ("CurrentYear")) {
			FreePasses();
			//Btn_Collect.gameObject.SetActive (true);
			TimeKeeper.gameObject.SetActive (false);
			PlayerPrefsX.SetBool ("VideoTimer", false);
			Debug.Log ("Entering Year");
		} else {
			//							Btn_Video.enabled = false;
			//Btn_Collect.gameObject.SetActive (false);
			TimeKeeper.gameObject.SetActive (true);
			PlayerPrefsX.SetBool ("VideoTimer", true);
			Debug.Log ("Entering Disabled");
		}
	}

	public void FreePasses (string s = null)
	{
		StoreConstants.totalMoves += DynamicConstants.timerMovesAward;
        GameConstants.canCollectMove = false;
		G2WGAHelper.LogEvent("Free Move - Timer");
        //CoinScript.CallUpdate (StoreHelper.totalUserCoins);

PlayerPrefsX.SetBool ("VideoTimer", false);

        //Btn_Collect.gameObject.SetActive (false);
        TimeKeeper.gameObject.SetActive (false);


    }

    public void resetValues(string s = null)
    {
		
        GameConstants.canCollectMove = true;
     
        if (!PlayerPrefsX.GetBool("VideoTimer"))
        {
			#if UNITY_ANDROID
            EtceteraAndroid.scheduleNotification((DynamicConstants.freeMovesTime * 3600), "Friends Forever", DynamicConstants.timerText, DynamicConstants.timerText, "No extra data");
		   //EtceteraAndroid.scheduleNotification(200, "Friends Forever", "Your free move is added!", "Your free move is added!", "No extra data");
		   #endif
		   G2WGAHelper.LogEvent("PNS Sent");
            PlayerPrefs.SetInt("CurrentHour", System.DateTime.Now.Hour);
            PlayerPrefs.SetInt("CurrentMinute", System.DateTime.Now.Minute);
            PlayerPrefs.SetInt("CurrentSeconds", System.DateTime.Now.Second);
            PlayerPrefs.SetInt("CurrentDay", System.DateTime.Now.Day);
            PlayerPrefs.SetInt("CurrentMonth", System.DateTime.Now.Month);
            PlayerPrefs.SetInt("CurrentYear", System.DateTime.Now.Year);
            Debug.Log("Free Coins successful");
            PlayerPrefsX.SetBool("VideoTimer", true);
        }
		   Start();
    }
}
