﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;

public class ChapterTextScript : MonoBehaviour {

	public Text chapterNumberText;

	public Text chapterNameText;

	public Text loadingText;

    public Text storyNameText;

    private string[] loadingTextArray;

    private string[] storyNameArray;

    private int randomLoadingInt;

    private string json_chapters_string;

    private JSONNode dynamicchapters;

    void OnEnable()
	{
		   if (GameConstants.currentChapterNumber > 30)
            {
                GameConstants.currentChapterNumber = 1;
            }
        loadJSONValues();
        populateArray ();

         
        
        chapterNumberText.text = "CHAPTER "+GameConstants.currentChapterNumber.ToString();
		storyNameText.text = storyNameArray[GameConstants.currentStoryNumber - 1].ToUpper();
        //chapterNameText.text = story1ChapterNameArray [GameConstants.currentChapterNumber - 1].ToUpper();
        //loadingText.text = loadingTextArray [randomLoadingInt];
        loadingText.gameObject.SetActive(false);
        loadingText.GetComponent<TextLocalize>().key = "loading" + randomLoadingInt.ToString() + "story" + GameConstants.currentStoryNumber.ToString();
        loadingText.gameObject.SetActive(true);
	}

	void populateArray()
	{
        if (GameConstants.currentStoryNumber == 1)
        {
            loadingTextArray = new string[] {"Crazy Teen Life only gets better...hang on!",
            "BFFs make it worth the wait, isn't it?",
            "The secrets are going to reveal in no time!",
            "Who will catch your fancy in high school?",
            "A couple more seconds of wait, and your story will begin... :)"
        };
        }
		else if(GameConstants.currentStoryNumber == 2)
		{
			loadingTextArray = new string[] {"A lot is going to unfold in Latte.Love.Life...Hang on!",
			"Just a couple of seconds, and your story will begin!",
			"Read on to find out the perfect end to your story.",
			"Don't miss out on the secrets and interesting tidbits!",
			"Your latte is almost ready, give it a few more seconds!"
		};
		}
        	else if (GameConstants.currentStoryNumber == 3)
        {
            loadingTextArray = new string[] {"Are you one of the gifted too? Read and find out!",
            "A few more seconds and we’ll be at Mc Grants!",
            "Can Mira be trusted? Beware of who you befriend!",
            "We’re almost there… to uncover Valerie’s secrets!",
            "Hang on! The truth will be unveiled soon."
        };
        }
        else if (GameConstants.currentStoryNumber == 4)
        {
            loadingTextArray = new string[] {"Liam or Federico? Who is the guy for you?",
            "Hang on… We’re preparing to get back at Lizzie!",
            "Who do you think deserves a second chance?",
            "Hold on… We’re loading up on the jello :p",
            "Almost there… for more gossip at Daleside High!"
        };
        }

if(GameConstants.currentStoryNumber == 6)
{
 randomLoadingInt = Random.Range(1, 7);
}
else
        {
            randomLoadingInt = Random.Range(1, 6);
        }

        storyNameArray = new string[] {"Crazy Teen Life",
                                        "Latte. Love. Life." ,
                                        "The Gifted",
                                        "Strangers & Second Chances",
                                        "They're Watching...",
                                        "Oh Hack No!"
										};


    }

    void loadJSONValues()
    { 
		TextAsset txt = (TextAsset)Resources.Load ("ChapterNames", typeof(TextAsset));

		json_chapters_string = txt.text;

		populateValues ();
	}

    void populateValues()
    {
        dynamicchapters = JSON.Parse(json_chapters_string);

		chapterNameText.text = dynamicchapters["Story" + GameConstants.currentStoryNumber][GameConstants.currentChapterNumber - 1].ToString().ToUpper();
    }
}
