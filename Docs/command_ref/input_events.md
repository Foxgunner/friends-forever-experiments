# Input event handlers # {#input_events}

[TOC]
# Key Pressed # {#KeyPressed}
The block will execute when a key press event occurs.

Defined in Fungus.KeyPressed

Property | Type | Description
 --- | --- | ---
Key Press Type | Fungus.KeyPressType | 
Key Code | UnityEngine.KeyCode | 

